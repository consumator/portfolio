package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Picture;
import com.alekseevp.portfolio.yasn.entities.Picture_;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

@Repository
public class FileDAOImpl implements FileDAO {
    private static final Logger logger = LogManager.getLogger(FileDAOImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    protected FileDAOImpl() {
    }

    @Transactional
    @Override
    public Picture save(Picture picture) {
        if (picture.getId() == 0) {
            entityManager.persist(picture);
        } else {
            entityManager.merge(picture);
        }
        entityManager.flush();
        return picture;
    }

    @Override
    public Picture read(int id) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Picture> cqry = cb.createQuery(Picture.class);
        Root<Picture> root = cqry.from(Picture.class);
        cqry.where(cb.equal(root.get(Picture_.id), id));
        TypedQuery<Picture> q = entityManager.createQuery(cqry);

        try {
            Picture avatar = q.getSingleResult();
            return avatar;
        } catch (NoResultException e) {
            return null;
        }
    }


    @Transactional
    @Override
    public void delete(Picture picture) {
        entityManager.remove(picture);
        entityManager.flush();
    }

    @Override
    public Picture getUserAvatar(int id) {
        final Query query = entityManager.createQuery("select p from Picture p join User u WHERE u.id=:id", Picture.class);
        try {
            return (Picture) query.setParameter("id", id).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
