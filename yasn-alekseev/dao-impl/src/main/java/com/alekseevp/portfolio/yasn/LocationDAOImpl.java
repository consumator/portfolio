package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.City;
import com.alekseevp.portfolio.yasn.entities.Country;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;

import static com.alekseevp.portfolio.yasn.Utility.isBlank;

@Repository
public class LocationDAOImpl implements LocationDAO {
<<<<<<< HEAD
    private static final Logger logger = LogManager.getLogger(PictureDAOImpl.class);
=======
    private static final Logger logger = LogManager.getLogger(FileDAOImpl.class);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    @PersistenceContext
    private EntityManager entityManager;

    protected LocationDAOImpl() {
    }

    @Override
    public List<Country> getCountryList() {
        final Query query = entityManager.createQuery("select cn from Country cn", Country.class);
        try {
<<<<<<< HEAD
            return (List<Country>) query.getResultList();
=======
            return (List<Country>)query.getResultList();
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        } catch (NoResultException e) {
            return null;
        }
    }


    @Override
    public List<City> getAllCitiesInCountry(String countryName) {
        if (countryName == null) {
            return null;
        }
        final TypedQuery query = entityManager.createQuery("select ct from City ct " +
                "where ct.country.name=:countryName", City.class);
        query.setParameter("countryName", countryName);
        try {
<<<<<<< HEAD
            return (List<City>) query.getResultList();
=======
            return (List<City>)query.getResultList();
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<City> getAllCitiesInCountry(Integer countryId) {
        final TypedQuery query = entityManager.createQuery("select ct from City ct " +
                "where ct.country.id=:countryId", City.class);
        query.setParameter("countryId", countryId);
        try {
<<<<<<< HEAD
            return (List<City>) query.getResultList();
=======
            return (List<City>)query.getResultList();
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public boolean saveCity(Country country, String cityName) {
        if (getCity(cityName, country.getId()) != null) {
            return false;
        }
        entityManager.persist(new City(cityName, country));
        entityManager.flush();
        return true;
    }


    @Override
    public boolean saveCity(Integer countryId, String cityName) {
        return !((countryId == null) || countryId == 0) && saveCity(getCountry(countryId), cityName);


    }


    @Override
    public Country getCountry(int id) {
        final Query query = entityManager.createQuery("select c from Country c WHERE c.id=:id", Country.class);
        try {
            return (Country) query.setParameter("id", id).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Country getCountry(String countryName) {
        final Query query = entityManager.createQuery("select c from Country c WHERE c.name=:name", Country.class);
        try {
            return (Country) query.setParameter("name", countryName).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


    @Override
    public boolean saveCountry(Country country) {
        if (getCountry(country.getName()) != null) {
            return false;
        }
        entityManager.persist(country);
        entityManager.flush();
        return true;
    }

    @Override
    public boolean saveCountry(String name) {
        return !isBlank(name) && saveCountry(new Country(name));
    }

    @Override
    public City getCity(int id) {
        final Query query = entityManager.createQuery("select c from City c WHERE c.id=:id", City.class);
        try {
            return (City) query.setParameter("id", id).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


    @Override
    public City getCity(String cityName, Integer countryId) {
        final Query query = entityManager.createQuery("select c from City c WHERE c.name=:name and c.country.id=:countryId", City.class);
        try {
            return (City) query.setParameter("name", cityName).setParameter("countryId", countryId).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
