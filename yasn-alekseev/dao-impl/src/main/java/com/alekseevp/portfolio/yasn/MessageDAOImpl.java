package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MessageDAOImpl implements MessageDAO {
<<<<<<< HEAD
    private static final Logger logger = LogManager.getLogger(PictureDAOImpl.class);
=======
    private static final Logger logger = LogManager.getLogger(FileDAOImpl.class);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Message> getUserMessages(int user) {
        final Query query = entityManager.createQuery("select m from Message m WHERE m.author.id=:user or m.receiver.id=:user", Message.class);
        query.setParameter("user", user);
        try {
            return (List<Message>) query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Message> getFromToMessages(int user, boolean isAuthor) {
        final Query query;
        if (isAuthor) {
            query = entityManager.createQuery("select m from Message m WHERE m.author.id=:author", Message.class);
            query.setParameter("author", user);
        } else {
            query = entityManager.createQuery("select m from Message m WHERE m.receiver.id=:receiver", Message.class);
            query.setParameter("receiver", user);
        }
        try {
            return (List<Message>) query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Message> getMutualMessages(int user1, int user2) {
        final Query query = entityManager.createQuery("select m from Message m WHERE (m.author.id=:user1 and m.receiver.id=:user2) " +
                "OR (m.author.id=:user2 and m.receiver.id=:user1)", Message.class);
        query.setParameter("user1", user1);
        query.setParameter("user2", user2);
        try {
            return (List<Message>) query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

    @Override
<<<<<<< HEAD
    public Message get(int id) {
=======
    public Message read(int id) {
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        final Query query = entityManager.createQuery("select m from Message m WHERE m.id=:id", Message.class);
        query.setParameter("id", id);
        try {
            return (Message) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
<<<<<<< HEAD
    public Message saveOrUpdate(Message message) {
        if (message.getId() != 0 && get(message.getId()) != null) {
            entityManager.merge(message);
        } else {
            entityManager.persist(message);
        }
        entityManager.flush();
        return message;
    }

    @Override
    public void remove(Message message) {
        entityManager.remove(message);
        entityManager.flush();
    }

    @Override
    public void remove(int id) {
        final Query query = entityManager.createQuery("delete from Message m where m.id=:id");
        query.setParameter("id", id).executeUpdate();
        entityManager.flush();

    }
=======
    public Message create(Message message) {
        entityManager.persist(message);
        entityManager.flush();
        return message;
    }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
}
