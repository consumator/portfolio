package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Picture;
import com.alekseevp.portfolio.yasn.entities.Picture_;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

@Repository
public class PictureDAOImpl implements PictureDAO {
    private static final Logger logger = LogManager.getLogger(PictureDAOImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    protected PictureDAOImpl() {
    }

    @Transactional
    @Override
    public Picture saveOrUpdate(Picture picture) {
        if (picture.getId() == 0) {
            entityManager.persist(picture);
        } else {
            entityManager.merge(picture);
        }
        entityManager.flush();
        return picture;
    }

    @Override
    public Picture get(int id) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Picture> cqry = cb.createQuery(Picture.class);
        Root<Picture> root = cqry.from(Picture.class);
        cqry.where(cb.equal(root.get(Picture_.id), id));
        TypedQuery<Picture> q = entityManager.createQuery(cqry);

        try {
            Picture avatar = q.getSingleResult();
            return avatar;
        } catch (NoResultException e) {
            return null;
        }
    }


    @Transactional
    @Override
    public void remove(Picture picture) {
        entityManager.remove(picture);
        entityManager.flush();
    }

    @Override
    public void remove(int id) {
        final Query query = entityManager.createQuery("delete from Picture p where p.id=:id");
        query.setParameter("id", id).executeUpdate();
        entityManager.flush();
    }

    @Override
    public Picture getUserAvatar(int userId) {
        final Query query = entityManager.createQuery("select p from Picture p join User u WHERE u.id=:id", Picture.class);
        try {
            return (Picture) query.setParameter("id", userId).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
