package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Setting;
import com.alekseevp.portfolio.yasn.entities.Setting_;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

<<<<<<< HEAD
import javax.persistence.*;
=======
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashSet;
import java.util.Set;

@Repository
public class SettingsDAOImpl implements SettingsDAO {
<<<<<<< HEAD
    private static final Logger logger = LogManager.getLogger(PictureDAOImpl.class);
=======
    private static final Logger logger = LogManager.getLogger(FileDAOImpl.class);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    @PersistenceContext
    private EntityManager entityManager;

    protected SettingsDAOImpl() {
    }

    @Override
<<<<<<< HEAD
    public Setting get(int id) {
        final Query query = entityManager.createQuery("select s from Setting s WHERE s.id=:id", Setting.class);
        try {
            return (Setting) query.setParameter("id", id).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Setting saveOrUpdate(Setting setting) {
        if (setting.getId() != 0 && get(setting.getId()) != null) {
            entityManager.merge(setting);
        } else {
            entityManager.persist(setting);
        }
        populateSettingAuxValue(setting);
        entityManager.flush();
        return setting;
    }

    @Override
    public void remove(Setting setting) {
        entityManager.remove(setting);
        entityManager.flush();

    }

    @Override
    public void remove(int id) {
        final Query query = entityManager.createQuery("delete from Setting s where s.id=:id");
        query.setParameter("id", id).executeUpdate();
        entityManager.flush();
    }


=======
    public boolean createSetting(Setting setting) {
        entityManager.persist(setting);
        entityManager.flush();
        return true;
    }

    @Override
    public boolean modifySetting(Setting setting) {
        entityManager.merge(setting);
        entityManager.flush();
        populateSettingAuxValue(setting);
        return true;
    }

>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    @Override
    public Setting getSettingByUserAndType(User userId, PrivacySettingType settingType) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Setting> cqry = cb.createQuery(Setting.class);
        Root<Setting> root = cqry.from(Setting.class);
        cqry.where(cb.and(cb.equal(root.get(Setting_.user), userId)),
                cb.equal(root.get(Setting_.settingType), settingType.name())
        );
        TypedQuery<Setting> q = entityManager.createQuery(cqry);
        try {
            Setting setting = q.getSingleResult();
            if (setting != null) {
                populateSettingAuxValue(setting);
            }
            return setting;
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Set<Setting> getUserSettings(User user) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Setting> cqry = cb.createQuery(Setting.class);
        Root<Setting> root = cqry.from(Setting.class);
        cqry.where(cb.equal(root.get(Setting_.user), user));
        TypedQuery<Setting> q = entityManager.createQuery(cqry);
        Set<Setting> settings = new HashSet<>();
        settings.addAll(q.getResultList());
        try {
            if (settings.isEmpty()) {
                return null;
            } else
                for (Setting setting : settings) {
                    populateSettingAuxValue(setting);
                }
            return settings;
        } catch (NoResultException e) {
            return null;
        }
    }

    private void populateSettingAuxValue(Setting setting) {
        PrivacySettingType settingType = setting.getSettingType();
        if (settingType != null) {
            setting.setSettingTypeViewName(settingType.toString());
            setting.setPossibleValues(settingType.getPossibleValues());
        }
    }
}
