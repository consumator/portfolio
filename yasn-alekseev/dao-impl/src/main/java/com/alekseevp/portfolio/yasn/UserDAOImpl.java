package com.alekseevp.portfolio.yasn;


import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.User_;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
<<<<<<< HEAD
=======
import java.text.ParseException;
import java.text.SimpleDateFormat;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import java.util.*;

import static com.alekseevp.portfolio.yasn.Utility.isBlank;

@Repository
public class UserDAOImpl implements UserDAO {
<<<<<<< HEAD
    private static final Logger logger = LogManager.getLogger(PictureDAOImpl.class);
=======
    private static final Logger logger = LogManager.getLogger(FileDAOImpl.class);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private LocationDAO locationDAO;

    protected UserDAOImpl() {
    }


    @Override
<<<<<<< HEAD
    public User saveOrUpdate(User user) {
        if (user.getId() != 0 && get(user.getId()) != null) {
=======
    public User saveUser(User user) {
        System.out.println(user.getEmail());
        if (getUser(user.getEmail()) != null) {
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
            entityManager.merge(user);
        } else {
            entityManager.persist(user);
        }
        entityManager.flush();
        return user;
    }

    @Override
<<<<<<< HEAD
    public void remove(User user) {
        entityManager.remove(user);
        entityManager.flush();

    }

    @Override
    public void remove(int id) {
        final Query query = entityManager.createQuery("delete from User u where u.id=:id");
        query.setParameter("id", id).executeUpdate();
        entityManager.flush();
    }

    @Override
=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    public Long countEntries() {
        final Query query = entityManager.createQuery("select count(u) from User u ");
        try {
            return (Long) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


    @Override
<<<<<<< HEAD
    public User getByEmail(String email) {
=======
    public User getUser(String email) {
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        final Query query = entityManager.createQuery("select u from User u WHERE u.email=:email", User.class);
        try {
            return (User) query.setParameter("email", email).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
<<<<<<< HEAD
    public User get(int id) {
=======
    public User getUser(int id) {
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        final Query query = entityManager.createQuery("select u from User u WHERE u.id=:id", User.class);
        try {
            return (User) query.setParameter("id", id).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


<<<<<<< HEAD
    private Predicate getFullSearchPredicate(User.UserSearchQueryObject queryObject, CriteriaBuilder cb, Root<User> root) {
        List<Predicate> predicates = new ArrayList<>();
        if (queryObject.getBirthDate() != null) {
            if (queryObject.isBirthDateGreaterThan()) {
                predicates.add(cb.greaterThanOrEqualTo(root.get(User_.birthDate), queryObject.getBirthDate()));
            } else {
                predicates.add(cb.lessThanOrEqualTo(root.get(User_.birthDate), queryObject.getBirthDate()));
            }
        }
        if (!isBlank(queryObject.getEmail())) {
            predicates.add(cb.like(cb.lower(root.get(User_.email)), "%" + queryObject.getEmail() + "%"));
        }
        if (!isBlank(queryObject.getName())) {
            predicates.add(cb.equal(cb.lower(root.get(User_.name)), queryObject.getName()));
        }
        if (!isBlank(queryObject.getSurname())) {
            predicates.add(cb.equal(cb.lower(root.get(User_.surname)), queryObject.getSurname()));
        }
        if (!isBlank(queryObject.getPatronymic())) {
            predicates.add(cb.equal(cb.lower(root.get(User_.patronymic)), queryObject.getPatronymic()));
        }
        if (queryObject.getSex() != null) {
            predicates.add(cb.equal(root.get(User_.sex), queryObject.getSex()));
        }
        if (queryObject.getBirthCountry() != 0) {
            predicates.add(cb.equal(root.get(User_.birthCountry), locationDAO.getCountry(queryObject.getBirthCountry())));
            if (queryObject.getBirthCity() != 0) {
                predicates.add(cb.equal(root.get(User_.birthCity), locationDAO.getCity(queryObject.getBirthCity())));
            }
        }
        if (queryObject.getCurrentCountry() != 0) {
            predicates.add(cb.equal(root.get(User_.currentCountry), locationDAO.getCountry(queryObject.getCurrentCountry())));
            if (queryObject.getCurrentCity() != 0) {
                predicates.add(cb.equal(root.get(User_.currentCity), locationDAO.getCity(queryObject.getCurrentCity())));
            }
        }
        if (predicates.size()==0){
            return cb.conjunction();
        }
        Predicate result = predicates.get(0);
        for (int i = 1; i < predicates.size(); i++) {
            result = cb.and(result, predicates.get(i));
        }
        return result;

    }

    private Predicate getQuickSearchPredicate(User.UserSearchQueryObject queryObject, CriteriaBuilder cb, Root<User> root) {
        String[] nameSurnameString = queryObject.getQuickSearchString().split(" ");
        if (nameSurnameString.length < 1) {
            return null;
        }
        Predicate nameSurname = null;
        Predicate surnameName = null;
        if (nameSurnameString.length >= 1) {
            String firstArg = nameSurnameString[0].toLowerCase();
            nameSurname = cb.equal(cb.lower(root.get(User_.name)), firstArg);
            surnameName = cb.equal(cb.lower(root.get(User_.surname)), firstArg);
            if (nameSurnameString.length >= 2) {
                String secondArg = nameSurnameString[1].toLowerCase();
                nameSurname = cb.and(nameSurname, cb.equal(cb.lower(root.get(User_.surname)), secondArg));
                surnameName = cb.and(surnameName, cb.equal(cb.lower(root.get(User_.name)), secondArg));
            }
        }
        return cb.or(nameSurname, surnameName);
    }


=======
    @Override
    public List<User> criteriaSearch(Map<String, String> searchParams) {
        if (searchParams.size() == 1) {
            return new ArrayList<>();
        }
        boolean birthDateSign = searchParams.get("bdSign").trim().equals("1");
        String searchEmail = searchParams.get("email").trim().toLowerCase();
        String searchName = searchParams.get("name").trim().toLowerCase();
        String searchSurname = searchParams.get("surname").trim().toLowerCase();
        String searchPatronymic = searchParams.get("patronymic").trim().toLowerCase();
        String searchSex = searchParams.get("sex").trim();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
        String birthDateString = searchParams.get("birthDate").trim();
        Date birthDate = null;
        if (!isBlank(birthDateString)) {
            try {
                birthDate = formatter.parse(birthDateString);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        Boolean sex;
        switch (Integer.valueOf(searchSex)) {
            case 2:
                sex = true;
                break;
            case 1:
                sex = false;
                break;
            default:
                sex = null;
        }
        Integer searchBirthCountry = Integer.valueOf(searchParams.get("birthCountry"));
        String birthCity = searchParams.get("birthCity");
        Integer searchBirthCity = birthCity == null ? 0 : Integer.valueOf(birthCity);
        Integer searchCurrentCountry = Integer.valueOf(searchParams.get("currentCountry"));
        String currentCity = searchParams.get("birthCity");
        Integer searchCurrentCity = currentCity == null ? 0 : Integer.valueOf(currentCity);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> cqry = cb.createQuery(User.class);
        Root<User> root = cqry.from(User.class);
        List<Predicate> predicates = new ArrayList<>();
        if (birthDate != null) {
            if (birthDateSign) {
                predicates.add(cb.greaterThanOrEqualTo(root.get(User_.birthDate), birthDate));
            } else {
                predicates.add(cb.lessThanOrEqualTo(root.get(User_.birthDate), birthDate));
            }
        }
        if (!isBlank(searchEmail)) {
            predicates.add(cb.like(cb.lower(root.get(User_.email)), "%" + searchEmail + "%"));
        }
        if (!isBlank(searchName)) {
            predicates.add(cb.equal(cb.lower(root.get(User_.name)), searchName));
        }
        if (!isBlank(searchSurname)) {
            predicates.add(cb.equal(cb.lower(root.get(User_.surname)), searchSurname));
        }
        if (!isBlank(searchPatronymic)) {
            predicates.add(cb.equal(cb.lower(root.get(User_.patronymic)), searchPatronymic));
        }
        if (sex != null) {
            predicates.add(cb.equal(root.get(User_.sex), sex));
        }
        if (searchBirthCountry != 0) {
            predicates.add(cb.equal(root.get(User_.birthCountry), locationDAO.getCountry(searchBirthCountry)));
            if (searchBirthCity != 0) {
                predicates.add(cb.equal(root.get(User_.birthCity), locationDAO.getCity(searchBirthCity)));
            }
        }
        if (searchCurrentCountry != 0) {
            predicates.add(cb.equal(root.get(User_.currentCountry), locationDAO.getCountry(searchCurrentCountry)));
            if (searchCurrentCity != 0) {
                predicates.add(cb.equal(root.get(User_.currentCity), locationDAO.getCity(searchCurrentCity)));
            }
        }
        cqry.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
        TypedQuery<User> q = entityManager.createQuery(cqry);
        List<User> users = q.getResultList();
        try {
            return users;
        } catch (NoResultException e) {
            return new LinkedList<>();
        }
    }

>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    @Override
    public List<User> quickSearch(String searchParams) {
        String[] params = searchParams.split(" ");
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> cqry = cb.createQuery(User.class);
        Root<User> root = cqry.from(User.class);
        Predicate nameSurname;
        Predicate surnameName;
        if (params.length >= 1) {
            String firstArg = params[0].toLowerCase();
            nameSurname = cb.equal(cb.lower(root.get(User_.name)), firstArg);
            surnameName = cb.equal(cb.lower(root.get(User_.surname)), firstArg);
            if (params.length >= 2) {
                String secondArg = params[1].toLowerCase();
                nameSurname = cb.and(nameSurname, cb.equal(cb.lower(root.get(User_.surname)), secondArg));
                surnameName = cb.and(surnameName, cb.equal(cb.lower(root.get(User_.name)), secondArg));
            }
            cqry.where(cb.or(nameSurname, surnameName));
        }
        TypedQuery<User> q = entityManager.createQuery(cqry);
        List<User> users = q.getResultList();
        try {
            return users;
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

<<<<<<< HEAD
    @Override
    public List<User> search(User.UserSearchQueryObject queryObject) {
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<User> cqry = cb.createQuery(User.class);
            Root<User> root = cqry.from(User.class);
            Predicate predicate = queryObject.isQuickSearch() ? getQuickSearchPredicate(queryObject, cb, root) :
                    getFullSearchPredicate(queryObject, cb, root);

            CriteriaQuery<Long> countTotalResultsQuery = cb.createQuery(Long.class);
            countTotalResultsQuery.select(cb.count(countTotalResultsQuery.from(User.class)));
            entityManager.createQuery(countTotalResultsQuery);
                countTotalResultsQuery.where(predicate);
            TypedQuery<Long> tq = entityManager.createQuery(countTotalResultsQuery);
            int totalResultsCount = tq.getSingleResult().intValue();
            queryObject.setResultsPageCount(
                    totalResultsCount % queryObject.getPageSize() == 0 ? totalResultsCount / queryObject.getPageSize() :
                            totalResultsCount / queryObject.getPageSize() + 1);
                cqry.select(root).where(predicate);
            TypedQuery<User> q = entityManager.createQuery(cqry);
            q.setFirstResult(queryObject.getPageNumber() * queryObject.getPageSize());
            q.setMaxResults(queryObject.getPageSize());
            return q.getResultList();
        } catch (NoResultException e) {
            return new LinkedList<>();
        }
    }
=======

>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
}

