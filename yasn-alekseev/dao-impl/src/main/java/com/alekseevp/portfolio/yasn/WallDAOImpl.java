package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.WallPost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Repository
public class WallDAOImpl implements WallDAO {
<<<<<<< HEAD
    private static final Logger logger = LogManager.getLogger(PictureDAOImpl.class);
=======
    private static final Logger logger = LogManager.getLogger(FileDAOImpl.class);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

    @PersistenceContext
    private EntityManager entityManager;

    protected WallDAOImpl() {
    }

    @Override
<<<<<<< HEAD
    public WallPost get(int postId) {
=======
    public List<WallPost> getWall(int owner) {
        final Query query = entityManager.createQuery("select w from WallPost w WHERE w.owner.id=:owner", WallPost.class);
        query.setParameter("owner", owner);
        try {
            return (List<WallPost>) query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public WallPost read(int postId) {
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        final Query query = entityManager.createQuery("select w from WallPost w WHERE w.id=:id", WallPost.class);
        query.setParameter("id", postId);
        try {
            return (WallPost) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

<<<<<<< HEAD
    @Override
    public WallPost saveOrUpdate(WallPost wallPost) {
        if (wallPost.getId()!=0|| get(wallPost.getId())!=null) {
            entityManager.merge(wallPost);
        } else {
            entityManager.persist(wallPost);
        }
        entityManager.flush();
        return wallPost;
    }

    @Override
    public void remove(WallPost wallPost) {
        entityManager.remove(wallPost);
        entityManager.flush();
    }

    @Override
    public void remove(int id) {
        final Query query =entityManager.createQuery("delete from WallPost w where w.id=:id");
        query.setParameter("id", id).executeUpdate();
        entityManager.flush();

=======

    @Override
    public int remove(int postId) {
        final Query query = entityManager.createQuery("delete from WallPost w WHERE w.id=:id");
        return query.setParameter("id", postId).executeUpdate();
    }

    @Override
    public WallPost update(WallPost wallPost) {
        entityManager.merge(wallPost);
        entityManager.flush();
        return wallPost;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }


    @Override
<<<<<<< HEAD
    public List<WallPost> getWall(int owner) {
        final Query query = entityManager.createQuery("select w from WallPost w WHERE w.owner.id=:owner", WallPost.class);
        query.setParameter("owner", owner);
        try {
            return (List<WallPost>) query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }





=======
    public WallPost create(WallPost wallPost) {
        entityManager.persist(wallPost);
        entityManager.flush();
        return wallPost;
    }


>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
}
