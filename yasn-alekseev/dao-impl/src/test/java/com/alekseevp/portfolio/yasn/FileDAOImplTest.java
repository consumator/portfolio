package com.alekseevp.portfolio.yasn;


import com.alekseevp.portfolio.yasn.entities.Picture;
<<<<<<< HEAD
import org.junit.BeforeClass;
=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.util.Arrays;

import static org.junit.Assert.*;

@ContextConfiguration(locations = "classpath:/repository-test-context.xml")
public class FileDAOImplTest extends AbstractTransactionalJUnit4SpringContextTests {
    @Autowired
<<<<<<< HEAD
    PictureDAO fileRepository;

    @BeforeClass
    public static void setEnv(){
        System.setProperty("textdb.allow_full_path","true");
    }
=======
    FileDAO fileRepository;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

    @Test
    public void read_isCorrect() {
        int testId = 5000;
        assertTrue("id is already occupied", countRowsInTableWhere("Picture", "id=" + testId) == 0);
        byte[] rndBytes = getRandomBytesArray();


        jdbcTemplate.update("INSERT INTO picture (id, name, image) VALUES (?,'testImage',?)", testId, rndBytes);

<<<<<<< HEAD
        Picture savedPic = fileRepository.get(testId);
=======
        Picture savedPic = fileRepository.read(testId);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        assertNotNull(savedPic);
        assertEquals("", savedPic.getName(), "testImage");
        assertEquals("", savedPic.getId(), testId);
        assertTrue("Stored value not equal to test value", Arrays.equals(savedPic.getImage(), rndBytes));

    }

    @Test
    public void save_creates_correctly() {
        int testId = 5000;
        assertTrue("id is already occupied", countRowsInTableWhere("Picture", "id=" + testId) == 0);
        byte[] rndBytes = getRandomBytesArray();
        Picture testPic = new Picture();
        testPic.setName("testImage");
        testPic.setImage(rndBytes);
        testPic.setId(testId);

<<<<<<< HEAD
        Picture savedPic = fileRepository.saveOrUpdate(testPic);
=======
        Picture savedPic = fileRepository.save(testPic);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

        assertNotNull(savedPic);
        assertEquals("", savedPic.getName(), "testImage");
        assertEquals("", savedPic.getId(), testId);
        assertTrue("Stored value not equal to test value", Arrays.equals(savedPic.getImage(), rndBytes));
    }

    @Test
    public void save_updates_correctly() {
        int testId = 5000;
        assertTrue("id is already occupied", countRowsInTableWhere("Picture", "id=" + testId) == 0);
        byte[] rndBytes = getRandomBytesArray();

        jdbcTemplate.update("INSERT INTO picture (id, name, image) VALUES (?,'testImage',?)", testId, rndBytes);
<<<<<<< HEAD
        Picture savedPic = fileRepository.get(testId);
        savedPic.setName("newName");
        savedPic = fileRepository.saveOrUpdate(savedPic);
=======
        Picture savedPic = fileRepository.read(testId);
        savedPic.setName("newName");
        savedPic = fileRepository.save(savedPic);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

        assertNotNull(savedPic);
        assertEquals("Update operation was unsuccessful", savedPic.getName(), "newName");
    }

    @Test
    public void delete_removes_correctly() {
        int testId = 5000;
        assertTrue("id is already occupied", countRowsInTableWhere("Picture", "id=" + testId) == 0);
        byte[] rndBytes = getRandomBytesArray();

        jdbcTemplate.update("INSERT INTO picture (id, name, image) VALUES (?,'testImage',?)", testId, rndBytes);
<<<<<<< HEAD
        Picture savedPic = fileRepository.get(testId);
        assertNotNull(savedPic);
        fileRepository.remove(savedPic);
        savedPic = fileRepository.get(testId);
=======
        Picture savedPic = fileRepository.read(testId);
        assertNotNull(savedPic);
        fileRepository.delete(savedPic);
        savedPic = fileRepository.read(testId);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

        assertNull("Delete operation was unsuccessful", savedPic);
    }

    private byte[] getRandomBytesArray() {
        byte[] rndBytes = new byte[128];
        for (int i = 0; i < rndBytes.length; i++) {
            rndBytes[i] = (byte) (7 * Math.random());
        }
        return rndBytes;
    }
}
