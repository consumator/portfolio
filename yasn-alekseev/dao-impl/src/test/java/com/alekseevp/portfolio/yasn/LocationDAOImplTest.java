package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.City;
import com.alekseevp.portfolio.yasn.entities.Country;
<<<<<<< HEAD
import org.junit.BeforeClass;
=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.util.List;

import static org.junit.Assert.*;

@ContextConfiguration(locations = "classpath:/repository-test-context.xml")
public class LocationDAOImplTest extends AbstractTransactionalJUnit4SpringContextTests {
    @Autowired
    LocationDAO locationRepository;

<<<<<<< HEAD
    @BeforeClass
    public static void setEnv(){
        System.setProperty("textdb.allow_full_path","true");
    }

=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    @Test
    public void saveCity_byCountryAndCityName_isSaved() throws Exception {
        final Integer countryId = 1;
        final Country russia = locationRepository.getCountry(countryId);

        locationRepository.saveCity(russia, "Нижневартовск");

        final City nijnevartovsk = locationRepository.getCity("Нижневартовск", countryId);
        assertNotNull(nijnevartovsk);
        assertEquals("", nijnevartovsk.getCountry().getName(), "Россия");
        assertEquals("", nijnevartovsk.getName(), "Нижневартовск");
    }


    @Test
    public void saveCity_byCountryIdAndCityName_isSaved() throws Exception {
        final String cityName = "Нижневартовск";
        final Integer countryId = 1;
        assertNull(locationRepository.getCity(cityName, countryId));

        locationRepository.saveCity(countryId, cityName);

        final City nijnevartovsk = locationRepository.getCity(cityName, countryId);
        assertNotNull(nijnevartovsk);
        assertEquals("", nijnevartovsk.getCountry().getId(), (long) countryId);
        assertEquals("", nijnevartovsk.getName(), cityName);
    }


    @Test
    public void getCountryList_returns_all_countries() throws Exception {
        final int numberOfCountries = countRowsInTable("country");
        assertTrue("there are no countries", numberOfCountries > 0);

        final List<Country> countries = locationRepository.getCountryList();

        assertEquals("number of expected countries is not equal to number of fetched counties", numberOfCountries, countries.size());
    }

    @Test
    public void getAllCitiesInCountry_by_country_id_returns_correct_list() throws Exception {
        final int numberOfCities = countRowsInTableWhere("city", "country_id=1");
        assertTrue("there are no cities", numberOfCities > 0);

        final List<City> cities = locationRepository.getAllCitiesInCountry(1);

        assertEquals("number of expected cities is not equal to number of fetched counties", numberOfCities, cities.size());
    }

    @Test
    public void getAllCitiesInCountry_by_country_name_returns_correct_list() throws Exception {
        final int numberOfCities = countRowsInTableWhere("city", "country_id=1");
        assertTrue("there are no cities", numberOfCities > 0);

        final List<City> cities = locationRepository.getAllCitiesInCountry("Россия");

        assertEquals("number of expected cities is not equal to number of fetched counties", numberOfCities, cities.size());
    }

    @Test
    public void getCity_byId_returns_correct_city() throws Exception {

        jdbcTemplate.update("INSERT INTO city (city_id, city_name, country_id) VALUES (100, 'Mahachkala', 1)");
        final City mahachkala = locationRepository.getCity(100);
        assertNotNull(mahachkala);
        assertEquals("", mahachkala.getCountry().getName(), "Россия");
        assertEquals("", mahachkala.getName(), "Mahachkala");
    }


    @Test
    public void getCity_by_name_and_country_id_returns_correct_city() throws Exception {
        jdbcTemplate.update("INSERT INTO city (city_id, city_name, country_id) VALUES (100, 'Mahachkala', 1)");

        final City mahachkala = locationRepository.getCity("Mahachkala", 1);

        assertNotNull(mahachkala);
        assertEquals("", mahachkala.getCountry().getName(), "Россия");
        assertEquals("", mahachkala.getName(), "Mahachkala");
    }

    @Test
    public void saveCountry_new_country_saves_correctly() throws Exception {
        Country georgia = new Country("Грузия");
        assertNull(locationRepository.getCountry("Грузия"));


        locationRepository.saveCountry(georgia);
        georgia = locationRepository.getCountry("Грузия");

        assertNotNull(georgia);
        assertEquals("", georgia.getName(), "Грузия");
        assertNotNull(georgia.getId());
    }

    @Test
    public void saveCountry_by_name_saves_correctly() throws Exception {
        Country georgia = locationRepository.getCountry("Грузия");
        assertNull(georgia);

        jdbcTemplate.update("INSERT INTO COUNTRY (country_id, country_name) VALUES (500,'Грузия')");
        georgia = locationRepository.getCountry("Грузия");

        assertNotNull(georgia);
        assertEquals("", georgia.getName(), "Грузия");
        assertEquals("", georgia.getId(), 500);
    }

    @Test
    public void getCountry_by_name_returns_correct_country() throws Exception {
        jdbcTemplate.update("INSERT INTO COUNTRY (country_id, country_name) VALUES (500,'Грузия')");

        final Country georgia = locationRepository.getCountry("Грузия");

        assertNotNull(georgia);
        assertEquals("", georgia.getName(), "Грузия");
        assertEquals("", georgia.getId(), 500);
    }

    @Test
    public void getCountry_byId_returns_correct_country() throws Exception {
        jdbcTemplate.update("INSERT INTO COUNTRY (country_id, country_name) VALUES (500,'Грузия')");

        final Country georgia = locationRepository.getCountry(500);

        assertNotNull(georgia);
        assertEquals("", georgia.getName(), "Грузия");
    }
}
