package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Message;
<<<<<<< HEAD
import org.junit.BeforeClass;
=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@ContextConfiguration(locations = "classpath:/repository-test-context.xml")
public class MessageDAOImplTest extends AbstractTransactionalJUnit4SpringContextTests {
    @Autowired
    MessageDAO messageRepository;
    @Autowired
    UserDAO userRepository;

<<<<<<< HEAD
    @BeforeClass
    public static void setEnv(){
        System.setProperty("textdb.allow_full_path","true");
    }

=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    @Test
         public void read_is_correct() {
        int testId = 5000;
        assertTrue("id is already occupied", countRowsInTableWhere("msg", "id=" + testId) == 0);

        jdbcTemplate.update("INSERT INTO MSG (id,author, receiver, time_stamp, text, is_read) " +
                "VALUES (?,1, 2, TIMESTAMP '2016-03-01 12:00:00', 'Happy Spring', FALSE);", testId);

<<<<<<< HEAD
        Message message = messageRepository.get(testId);
=======
        Message message = messageRepository.read(testId);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        assertNotNull(message);
        assertEquals("", message.getText(), "Happy Spring");
        assertTrue("", message.getReceiver().getId()==2);
    }

    @Test
    public void create_is_correct() {
        assertTrue("Entry is in the base before test", countRowsInTableWhere("msg", "id=5000 and text='Happy Spring'") == 0);

        GregorianCalendar timestamp=new GregorianCalendar(2016, 3, 1, 12, 0, 0);
<<<<<<< HEAD
        Message message=new Message(userRepository.get(1),userRepository.get(2),"Happy Spring");
        message.setTimestamp(timestamp);
        messageRepository.saveOrUpdate(message);
        Message savedMessage=messageRepository.get(message.getId());
=======
        Message message=new Message(userRepository.getUser(1),userRepository.getUser(2),"Happy Spring");
        message.setTimestamp(timestamp);
        messageRepository.create(message);
        Message savedMessage=messageRepository.read(message.getId());
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

        assertNotNull(savedMessage);
        assertTrue("Author is incorrect", savedMessage.getAuthor().getId() == 1);
        assertTrue("Receiver is incorrect", savedMessage.getReceiver().getId() == 2);
        assertEquals("Timestamp is incorrect", timestamp, savedMessage.getTimestamp());
        assertEquals("Text is incorrect", savedMessage.getText(), "Happy Spring");
    }


    @Test
    public void getUserMessages_returns_all_messages_correctly() throws Exception {
        final int numberOfMessages= countRowsInTableWhere("msg", "author=1 or receiver=1");
        assertTrue("there are no messages", numberOfMessages > 0);

        final List<Message> messages = messageRepository.getUserMessages(1);

        assertEquals("number of expected messages is not equal to number of fetched messages", numberOfMessages, messages.size());
    }

    @Test
    public void getMutualMessages_returns_all_messages_correctly() throws Exception {
        final int numberOfMessages= countRowsInTableWhere("msg", "(author=1 and receiver=2) or (author=2 and receiver=1)");
        assertTrue("there are no messages", numberOfMessages > 0);

        final List<Message> messages = messageRepository.getMutualMessages(1, 2);

        assertEquals("number of expected messages is not equal to number of fetched messages", numberOfMessages, messages.size());
    }


    @Test
    public void getFromToMessages_author_true_returns_all_messages_correctly() throws Exception {
        final int numberOfMessages= countRowsInTableWhere("msg", "author=1");
        assertTrue("there are no messages", numberOfMessages > 0);

        final List<Message> messages = messageRepository.getFromToMessages(1, true);

        assertEquals("number of expected messages is not equal to number of fetched messages", numberOfMessages, messages.size());
    }

    @Test
    public void getFromToMessages_author_false_returns_all_messages_correctly() throws Exception {
        final int numberOfMessages= countRowsInTableWhere("msg", "receiver=2");
        assertTrue("there are no messages", numberOfMessages > 0);

        final List<Message> messages = messageRepository.getFromToMessages(2, false);

        assertEquals("number of expected messages is not equal to number of fetched messages", numberOfMessages, messages.size());
    }
}
