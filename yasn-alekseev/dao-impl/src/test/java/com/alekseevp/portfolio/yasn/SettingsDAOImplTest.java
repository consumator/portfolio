package com.alekseevp.portfolio.yasn;


import com.alekseevp.portfolio.yasn.entities.Setting;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingType;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingsValue;
import org.junit.Assert;
<<<<<<< HEAD
import org.junit.BeforeClass;
=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotNull;

@ContextConfiguration(locations = "classpath:/repository-test-context.xml")
public class SettingsDAOImplTest extends AbstractTransactionalJUnit4SpringContextTests {
    @Autowired
    SettingsDAO settingsDAO;
    @Autowired
    UserDAO userRepository;
<<<<<<< HEAD

    @BeforeClass
    public static void setEnv() {
        System.setProperty("textdb.allow_full_path", "true");
    }

    @Test
    public void getSettingByUserAndType_returns_correct_result() {
=======
    @Test
    public void getSettingByUserAndType_returns_correct_result(){
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        assertTrue("setting is already present in the base", countRowsInTableWhere("user_settings", "user_id=5 and setting_type='PROFILE_VIEW'") == 0);
        jdbcTemplate.update("INSERT INTO user_settings (id, user_id, setting_type,setting_value) " +
                "VALUES (1000,5,?,?)", PrivacySettingType.PROFILE_VIEW.name(), PrivacySettingsValue.ALL.name());

<<<<<<< HEAD
        Setting savedSetting = settingsDAO.getSettingByUserAndType(userRepository.get(5), PrivacySettingType.PROFILE_VIEW);

        assertNotNull(savedSetting);
        Assert.assertTrue("Value is set incorrect", savedSetting.getValue() == PrivacySettingsValue.ALL);
        Assert.assertTrue("Id is incorrect", savedSetting.getId() == 1000);
=======
        Setting savedSetting=settingsDAO.getSettingByUserAndType(userRepository.getUser(5),PrivacySettingType.PROFILE_VIEW);

        assertNotNull(savedSetting);
        Assert.assertTrue("Value is set incorrect", savedSetting.getValue() == PrivacySettingsValue.ALL);
        Assert.assertTrue("Id is incorrect", savedSetting.getId()==1000);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

    }

    @Test
<<<<<<< HEAD
    public void createSetting_saves_correctly() {
        assertTrue("setting is already present in the base", countRowsInTableWhere("user_settings", "user_id=5 and setting_type='PROFILE_VIEW'") == 0);
        Setting setting = new Setting();
        setting.setUser(userRepository.get(5));
        setting.setSettingType(PrivacySettingType.PROFILE_VIEW);
        setting.setValue(PrivacySettingsValue.ALL);

        settingsDAO.saveOrUpdate(setting);
        Setting savedSetting = settingsDAO.getSettingByUserAndType(userRepository.get(5), PrivacySettingType.PROFILE_VIEW);
=======
    public void createSetting_saves_correctly(){
        assertTrue("setting is already present in the base", countRowsInTableWhere("user_settings", "user_id=5 and setting_type='PROFILE_VIEW'") == 0);
        Setting setting=new Setting();
        setting.setUser(userRepository.getUser(5));
        setting.setSettingType(PrivacySettingType.PROFILE_VIEW);
        setting.setValue(PrivacySettingsValue.ALL);

        settingsDAO.createSetting(setting);
        Setting savedSetting=settingsDAO.getSettingByUserAndType(userRepository.getUser(5),PrivacySettingType.PROFILE_VIEW);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

        assertNotNull(savedSetting);
        Assert.assertTrue("Value is set incorrect", savedSetting.getValue() == PrivacySettingsValue.ALL);
    }

    @Test
<<<<<<< HEAD
    public void saveOrUpdate_updates_correctly() {
        assertTrue("setting is already present in the base", countRowsInTableWhere("user_settings", "user_id=5 and setting_type='PROFILE_VIEW'") == 0);
        jdbcTemplate.update("INSERT INTO user_settings (id, user_id, setting_type,setting_value) " +
                "VALUES (1000,5,?,?)", PrivacySettingType.PROFILE_VIEW.name(), PrivacySettingsValue.ALL.name());
        Setting savedSetting = settingsDAO.getSettingByUserAndType(userRepository.get(5), PrivacySettingType.PROFILE_VIEW);
        savedSetting.setValue(PrivacySettingsValue.FRIENDS);

        settingsDAO.saveOrUpdate(savedSetting);

        Setting updatedSetting = jdbcTemplate.queryForObject("select * from user_settings where id=1000", new RowMapper<Setting>() {
=======
    public void modifySetting_updates_correctly(){
        assertTrue("setting is already present in the base", countRowsInTableWhere("user_settings", "user_id=5 and setting_type='PROFILE_VIEW'") == 0);
        jdbcTemplate.update("INSERT INTO user_settings (id, user_id, setting_type,setting_value) " +
                "VALUES (1000,5,?,?)", PrivacySettingType.PROFILE_VIEW.name(), PrivacySettingsValue.ALL.name());
        Setting savedSetting=settingsDAO.getSettingByUserAndType(userRepository.getUser(5),PrivacySettingType.PROFILE_VIEW);
        savedSetting.setValue(PrivacySettingsValue.FRIENDS);

        settingsDAO.modifySetting(savedSetting);

        Setting updatedSetting=jdbcTemplate.queryForObject("select * from user_settings where id=1000", new RowMapper<Setting>() {
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
            @Override
            public Setting mapRow(ResultSet rs, int rowNum) throws SQLException {
                Setting setting = new Setting();
                setting.setId(rs.getInt(1));
<<<<<<< HEAD
                setting.setUser(userRepository.get(rs.getInt(2)));
=======
                setting.setUser(userRepository.getUser(rs.getInt(2)));
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
                setting.setSettingType(PrivacySettingType.valueOf(rs.getString(3)));
                setting.setValue(PrivacySettingsValue.valueOf(rs.getString(4)));
                return setting;
            }
        });

        assertNotNull(updatedSetting);
        Assert.assertTrue("Value is set incorrect", updatedSetting.getValue() == PrivacySettingsValue.FRIENDS);
    }

    @Test
<<<<<<< HEAD
    public void getUserSettings_return_correct_set() {
        assertTrue("setting are already present in the base", countRowsInTableWhere("user_settings", "user_id=5") == 0);
        User testUser = userRepository.get(5);
        Setting profile = new Setting();
        profile.setUser(testUser);
        profile.populateWithDefault(PrivacySettingType.PROFILE_VIEW);
        settingsDAO.saveOrUpdate(profile);
        Setting messages = new Setting();
        messages.setUser(testUser);
        messages.populateWithDefault(PrivacySettingType.MESSAGES_WRITE);
        settingsDAO.saveOrUpdate(messages);
        Setting wall = new Setting();
        wall.setUser(testUser);
        wall.populateWithDefault(PrivacySettingType.WALL_WRITE);
        settingsDAO.saveOrUpdate(wall);

        Set<Setting> settings = settingsDAO.getUserSettings(testUser);

        int settingsCount = countRowsInTableWhere("user_settings", "user_id=5");
        assertTrue("Not all setttings were retreived", settingsCount == settings.size());
=======
    public void getUserSettings_return_correct_set(){
        assertTrue("setting are already present in the base", countRowsInTableWhere("user_settings", "user_id=5") == 0);
        User testUser=userRepository.getUser(5);
        Setting profile=new Setting();
        profile.setUser(testUser);
        profile.populateWithDefault(PrivacySettingType.PROFILE_VIEW);
        settingsDAO.createSetting(profile);
        Setting messages=new Setting();
        messages.setUser(testUser);
        messages.populateWithDefault(PrivacySettingType.MESSAGES_WRITE);
        settingsDAO.createSetting(messages);
        Setting wall=new Setting();
        wall.setUser(testUser);
        wall.populateWithDefault(PrivacySettingType.WALL_WRITE);
        settingsDAO.createSetting(wall);

        Set<Setting> settings=settingsDAO.getUserSettings(testUser);

        int settingsCount = countRowsInTableWhere("user_settings", "user_id=5");
        assertTrue("Not all setttings were retreived",settingsCount==settings.size());
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }


}
