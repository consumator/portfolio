package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.User;
<<<<<<< HEAD
import org.junit.BeforeClass;
=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

<<<<<<< HEAD
import java.util.List;

=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@ContextConfiguration(locations = "classpath:/repository-test-context.xml")
public class UserDAOImplTest extends AbstractTransactionalJUnit4SpringContextTests {
    @Autowired
    UserDAO userRepository;

    public static final int TEST_ID = 5000;
    public static final String TEST_EMAIL = "sobaka@ya.ru";
    public static final String TEST_NAME = "Ataman";
    public static final String TEST_SURNAME = "Mahno";
    public static final String TEST_PASSWORD = "QWERTY123";

    public static final String SEARCH_NAME = "Ataman";
    public static final String SEARCH_SURNAME = "Ataman";


<<<<<<< HEAD
    @BeforeClass
    public static void setEnv() {
        System.setProperty("textdb.allow_full_path", "true");
    }

    @Test
    public void getUser_returns_correct_user() {
        assertNull("Test user already in base", userRepository.get(100));
        insertTestUser();

        User readUser = userRepository.get(TEST_ID);
=======
    @Test
    public void getUser_returns_correct_user() {
        assertNull("Test user already in base", userRepository.getUser(100));
        insertTestUser();

        User readUser = userRepository.getUser(TEST_ID);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

        assertNotNull(readUser);
        checkAgainstTestValues(readUser);
    }

    @Test
    public void getUser_by_email_returns_correct_user() {
<<<<<<< HEAD
        assertNull("Test user already in base", userRepository.get(100));
        insertTestUser();

        User readUser = userRepository.getByEmail(TEST_EMAIL);
=======
        assertNull("Test user already in base", userRepository.getUser(100));
        insertTestUser();

        User readUser = userRepository.getUser(TEST_EMAIL);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

        assertNotNull(readUser);
        checkAgainstTestValues(readUser);
    }

    @Test
    public void saveUser_saves_correctly() {
<<<<<<< HEAD
        assertNull("Test user already in base", userRepository.get(100));
        User user = new User(TEST_EMAIL, TEST_PASSWORD, TEST_NAME, TEST_SURNAME);

        int savedId = userRepository.saveOrUpdate(user).getId();

        checkAgainstTestValues(userRepository.get(savedId));
=======
        assertNull("Test user already in base", userRepository.getUser(100));
        User user = new User(TEST_EMAIL,TEST_PASSWORD,TEST_NAME,TEST_SURNAME);

        int savedId =userRepository.saveUser(user).getId();

        checkAgainstTestValues(userRepository.getUser(savedId));
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Test
    public void saveUser_updates_correctly() {
<<<<<<< HEAD
        assertNotNull("Test user already in base", userRepository.get(1));
        User user = userRepository.get(1);
=======
        assertNotNull("Test user already in base", userRepository.getUser(1));
        User user = userRepository.getUser(1);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        user.setEmail(TEST_EMAIL);
        user.setPassword(TEST_PASSWORD);
        user.setName(TEST_NAME);
        user.setSurname(TEST_SURNAME);


<<<<<<< HEAD
        userRepository.saveOrUpdate(user);

        checkAgainstTestValues(userRepository.get(1));
=======
        userRepository.saveUser(user);

        checkAgainstTestValues(userRepository.getUser(1));
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }


    @Test
<<<<<<< HEAD
    public void countEntries_returns_correctNumber() {
=======
    public void countEntries_returns_correctNumber(){
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        assertTrue("Wrong result", userRepository.countEntries() == countRowsInTable("user_list"));
    }

    private void checkAgainstTestValues(User user) {
        assertEquals("Email is stored incorrectly", user.getEmail(), TEST_EMAIL);
        assertEquals("Name is stored incorrectly", user.getName(), TEST_NAME);
        assertEquals("Surname is stored incorrectly", user.getSurname(), TEST_SURNAME);
        assertEquals("Password is stored incorrectly", user.getPassword(), TEST_PASSWORD);
    }

    private void insertTestUser() {
        jdbcTemplate.update("INSERT INTO user_list (id,email,password,name,surname) " +
                "VALUES (?,?,?,?,?)", TEST_ID, TEST_EMAIL, TEST_PASSWORD, TEST_NAME, TEST_SURNAME);
    }


    @Test
<<<<<<< HEAD
    public void quickSearch_returns_correct_entries() {
        insertTestUser();

        assertTrue("List size is correct", userRepository.quickSearch(TEST_NAME).size() == 1);
        assertTrue("Search results are correct", userRepository.quickSearch(TEST_NAME).contains(userRepository.getByEmail(TEST_EMAIL)));
        assertTrue("List size is correct", userRepository.quickSearch(TEST_SURNAME).size() == 1);
        assertTrue("Search results are correct", userRepository.quickSearch(TEST_SURNAME).contains(userRepository.getByEmail(TEST_EMAIL)));
        assertTrue("List size is correct", userRepository.quickSearch(TEST_NAME + " " + TEST_SURNAME).size() == 1);
        assertTrue("Search results are correct", userRepository.quickSearch(TEST_NAME + " " + TEST_SURNAME).contains(userRepository.getByEmail(TEST_EMAIL)));
        assertTrue("List size is correct", userRepository.quickSearch(TEST_SURNAME + " " + TEST_NAME).size() == 1);
        assertTrue("Search results are correct", userRepository.quickSearch(TEST_SURNAME + " " + TEST_NAME).contains(userRepository.getByEmail(TEST_EMAIL)));
    }

    @Test
    public void search_returns_correct_enries() {
        User.UserSearchQueryObject queryObject = new User.UserSearchQueryObject();
        queryObject.setBirthDate(Utility.parseDateFromString("1998-06-08"));
        queryObject.setBirthDateGreaterThan(true);
        List<User> searchResults = userRepository.search(queryObject);
        System.out.println();
        for (int i = 0; i < 4; i++) {
            assertEquals("Returned list is incorrect", userRepository.get(i + 1), searchResults.get(i));
        }
        assertEquals("Returned list is incorrect", 4, searchResults.size());
        int nazarovId = 25;
        queryObject = new User.UserSearchQueryObject();
        queryObject.setSurname("назаров");
        searchResults = userRepository.search(queryObject);
        assertEquals("Returned list is incorrect", userRepository.get(nazarovId), searchResults.get(0));
        assertEquals("Returned list is incorrect", 1, searchResults.size());

        queryObject = new User.UserSearchQueryObject();
        int izotId = 29;
        queryObject.setName("изот");
        searchResults = userRepository.search(queryObject);
        assertEquals("Returned list is incorrect", userRepository.get(izotId), searchResults.get(0));
        assertEquals("Returned list is incorrect", 1, searchResults.size());

        int petrovichId = 1;
        queryObject = new User.UserSearchQueryObject();
        queryObject.setPatronymic("петрович");
        searchResults = userRepository.search(queryObject);
        assertEquals("Returned list is incorrect", userRepository.get(petrovichId), searchResults.get(0));
        assertEquals("Returned list is incorrect", 1, searchResults.size());

        queryObject = new User.UserSearchQueryObject();
        queryObject.setSex(true);
        searchResults = userRepository.search(queryObject);
        for (int i = 0; i < 4; i++) {
            assertEquals("Returned list is incorrect", userRepository.get(i + 1), searchResults.get(i));
        }
        assertEquals("Returned list is incorrect", 4, searchResults.size());

        queryObject = new User.UserSearchQueryObject();
        queryObject.setBirthCountry(5);
        searchResults = userRepository.search(queryObject);
        for (int i = 6; i < 9; i++) {
            assertEquals("Returned list is incorrect", userRepository.get(i), searchResults.get(i - 6));
        }
        assertEquals("Returned list is incorrect", 3, searchResults.size());
        queryObject.setBirthCountry(5);
        queryObject.setBirthCity(15);
        int bruceId = 8;
        searchResults = userRepository.search(queryObject);
        assertEquals("Returned list is incorrect", userRepository.get(bruceId), searchResults.get(0));
        assertEquals("Returned list is incorrect", 1, searchResults.size());

        queryObject=new User.UserSearchQueryObject();
        queryObject.setCurrentCountry(3);
        queryObject.setCurrentCity(7);
        int genadiyId = 3;
        searchResults = userRepository.search(queryObject);
        assertEquals("Returned list is incorrect", userRepository.get(genadiyId), searchResults.get(0));
        assertEquals("Returned list is incorrect", 1, searchResults.size());
        queryObject=new User.UserSearchQueryObject();
        queryObject.setEmail("ghj@xyz.com");
        int johnSnowId=6;
        searchResults = userRepository.search(queryObject);
        assertEquals("Returned list is incorrect", userRepository.get(johnSnowId), searchResults.get(0));
        assertEquals("Returned list is incorrect", 1, searchResults.size());
=======
    public void quickSearch_returns_correct_entries(){
        insertTestUser();

        assertTrue("List size is correct", userRepository.quickSearch(TEST_NAME).size() == 1);
        assertTrue("Search results are correct", userRepository.quickSearch(TEST_NAME).contains(userRepository.getUser(TEST_EMAIL)));
        assertTrue("List size is correct", userRepository.quickSearch(TEST_SURNAME).size()==1);
        assertTrue("Search results are correct", userRepository.quickSearch(TEST_SURNAME).contains(userRepository.getUser(TEST_EMAIL)));
        assertTrue("List size is correct", userRepository.quickSearch(TEST_NAME+" "+TEST_SURNAME).size()==1);
        assertTrue("Search results are correct", userRepository.quickSearch(TEST_NAME+" "+TEST_SURNAME).contains(userRepository.getUser(TEST_EMAIL)));
        assertTrue("List size is correct", userRepository.quickSearch(TEST_SURNAME+" "+TEST_NAME).size()==1);
        assertTrue("Search results are correct", userRepository.quickSearch(TEST_SURNAME + " " + TEST_NAME).contains(userRepository.getUser(TEST_EMAIL)));
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }
}
