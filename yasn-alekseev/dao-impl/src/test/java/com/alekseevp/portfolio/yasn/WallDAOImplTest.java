package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.WallPost;
<<<<<<< HEAD
import org.junit.BeforeClass;
=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.*;


@ContextConfiguration(locations = "classpath:/repository-test-context.xml")
public class WallDAOImplTest extends AbstractTransactionalJUnit4SpringContextTests {
    @Autowired
    WallDAO wallRepository;

    @Autowired
    UserDAO userRepository;

<<<<<<< HEAD
    @BeforeClass
    public static void setEnv() {
        System.setProperty("textdb.allow_full_path", "true");
    }

=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    @Test
    public void read_returns_correct_post() {
        int postId = 5000;
        int authorId = 3;
        int ownerId = 5;
        String testString = "Nice day, isn`t it?";
        Calendar calendar = new GregorianCalendar(2016, 1, 15);
<<<<<<< HEAD
        assertNull("base already have test row", wallRepository.get(postId));
        jdbcTemplate.update("INSERT INTO wall (id,from_id,owner_id,time_stamp,text,is_read) " +
                "VALUES (?,?,?,?,?,FALSE)", postId, 3, 5, calendar, testString);

        WallPost post = wallRepository.get(postId);
=======
        assertNull("base already have test row", wallRepository.read(postId));
        jdbcTemplate.update("INSERT INTO wall (id,from_id,owner_id,time_stamp,text,is_read) " +
                "VALUES (?,?,?,?,?,FALSE)", postId, 3, 5, calendar, testString);

        WallPost post = wallRepository.read(postId);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

        assertNotNull(post);
        assertTrue("Author is incorrect", post.getFrom().getId() == authorId);
        assertTrue("Owner is incorrect", post.getOwner().getId() == ownerId);
        assertEquals("Text is incorrect", post.getText(), testString);
        assertEquals("Timestamp is incorrect", post.getTimestamp(), calendar);
    }

    @Test
    public void create_saves_to_base_correctly() {
        String postText = "You are cool man";
        int from = 3;
        int owner = 4;
        assertTrue("There is already such post", countRowsInTableWhere("wall", "from_id=3 and owner_id=4 and text='You are cool man'") == 0);
<<<<<<< HEAD
        WallPost newPost = new WallPost("You are cool man", userRepository.get(4), userRepository.get(3));
        wallRepository.saveOrUpdate(newPost);
=======
        WallPost newPost = new WallPost("You are cool man", userRepository.getUser(4), userRepository.getUser(3));
        wallRepository.create(newPost);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b


        WallPost retreivedPost = jdbcTemplate.queryForObject("SELECT * FROM wall WHERE text=?", new WallPostRowMapper(), postText);

        assertNotNull(retreivedPost);
        assertEquals("Text is saved incorrectly", retreivedPost.getText(), postText);
        assertTrue("From is saved incorrectly", retreivedPost.getFrom().getId() == from);
        assertTrue("Owner is saved incorrectly", retreivedPost.getOwner().getId() == owner);
    }


    @Test
    public void update_saves_to_base_correctly() {
        String postText = "You are cool man";
        int postId = 5000;
        int from = 3;
        int owner = 4;
        String updatedText = "Whooping new TEXT";
        jdbcTemplate.update("INSERT INTO wall (id,from_id,owner_id,text,time_stamp,is_read) " +
                "VALUES (?,?,?,?,?,FALSE)", postId, from, owner, postText, Calendar.getInstance());
<<<<<<< HEAD
        WallPost updatedPost = wallRepository.get(postId);
        updatedPost.setText(updatedText);
        wallRepository.saveOrUpdate(updatedPost);
=======
        WallPost updatedPost = wallRepository.read(postId);
        updatedPost.setText(updatedText);
        wallRepository.update(updatedPost);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b


        WallPost retreivedPost = jdbcTemplate.queryForObject("SELECT * FROM wall WHERE id=?", new WallPostRowMapper(), postId);

        assertNotNull(retreivedPost);
        assertEquals("Text is saved incorrectly", retreivedPost.getText(), updatedText);
    }

    @Test
    public void getWall_returns_all_entries() {
        int wallPostCount = countRowsInTableWhere("wall", "owner_id=2");

        List<WallPost> posts = wallRepository.getWall(2);

        assertTrue("getWall returns incorrect list of posts", wallPostCount == posts.size());
    }

    @Test
    public void remove_deletes_properly() {
        String postText = "You are cool man";
        int postId = 5000;
        int from = 3;
        int owner = 4;
        jdbcTemplate.update("INSERT INTO wall (id,from_id,owner_id,text,time_stamp,is_read) " +
                "VALUES (?,?,?,?,?,FALSE)", postId, from, owner, postText, Calendar.getInstance());
        assertTrue("Row for test was not created", countRowsInTableWhere("wall", "id=" + postId) == 1);
<<<<<<< HEAD
        WallPost post = wallRepository.get(postId);
=======
        WallPost post = wallRepository.read(postId);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

        wallRepository.remove(postId);

        assertTrue("Row was not deleted", countRowsInTableWhere("wall", "id=" + postId) == 0);
    }


    class WallPostRowMapper implements RowMapper<WallPost> {
        @Override
        public WallPost mapRow(ResultSet rs, int rowNum) throws SQLException {
            WallPost result = new WallPost();
            result.setId(rs.getInt(1));
<<<<<<< HEAD
            result.setFrom(userRepository.get(rs.getInt(2)));
            result.setOwner(userRepository.get(rs.getInt(3)));
=======
            result.setFrom(userRepository.getUser(rs.getInt(2)));
            result.setOwner(userRepository.getUser(rs.getInt(3)));
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(rs.getTimestamp(4).getTime());
            result.setTimestamp(calendar);
            result.setText(rs.getString(5));
            result.setIsRead(rs.getBoolean(6));
            return result;
        }
    }
}
