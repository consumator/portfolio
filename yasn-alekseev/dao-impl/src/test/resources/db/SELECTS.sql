﻿SELECT
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '12'
FROM USER_LIST;


SELECT
  id,
  email,
  password     AS password,
  name         AS name,
  surname      AS surname,
  patronymic   AS patronymic,
  birth_city_name,
  country_name AS birth_country_name
FROM
  (SELECT
     id,
     email,
     password,
     name,
     surname,
     patronymic,
     birth_country,
     ct.city_name AS birth_city_name
   FROM USER_LIST AS u JOIN CITY AS ct
       ON u.birth_city = ct.city_id) AS user_with_city JOIN COUNTRY AS cn
    ON user_with_city.birth_country = cn.country_id;

SELECT *
FROM
  USER_LIST AS u JOIN
  (SELECT
     city_id,
     city_name,
     ct.country_id,
     country_name
   FROM
     CITY AS ct JOIN COUNTRY AS cn
       ON ct.country_id = cn.country_id) AS ctcn;

SELECT *
FROM
  USER_LIST u JOIN
  (SELECT
     city_id,
     city_name,
     COUNTRY.country_id,
     country_name
   FROM
     CITY
     JOIN COUNTRY ON
                    CITY.country_id = COUNTRY.country_ID
  ) AS ctcn;

SELECT
  email,
  password AS PASSWORD
FROM
  USER_LIST
WHERE email = 'abc@xyz.com';

SELECT
  email,
  password AS password
FROM
  USER_LIST
WHERE email = 'abc@xyz.com';


SELECT
  id,
  email,
  password           AS password,
  name               AS name,
  surname            AS surname,
  patronymic         AS patronymic,
  sex                AS sex,
  birth_date         AS birthDate,
  ct.city_name       AS birthCity,
  cn.country_name    AS birthCountry,
  curct.city_name    AS currentCity,
  curcn.country_name AS currentCountry,
  birth_date
FROM USER_LIST u
  LEFT JOIN CITY ct ON u.birth_city = ct.city_id
  LEFT JOIN COUNTRY AS cn ON u.birth_country = cn.country_id
  LEFT JOIN CITY curct ON u.current_city = curct.city_id
  LEFT JOIN COUNTRY curcn ON u.birth_country = curcn.country_id

WHERE email = 'nana@xyz.com';

SELECT
  cn.country_id,
  country_name,
  city_id,
  city_name
FROM Country cn JOIN CITY ct ON cn.country_id = ct.country_id
WHERE cn.country_name = 'Украина';

SELECT *
FROM
  CITY ct JOIN COUNTRY cn ON ct.country_id = cn.country_id;

SELECT
  city_id,
  city_name,
  country_name
FROM CITY ct JOIN COUNTRY cn ON ct.country_id = cn.country_id
WHERE city_name = 'Москва' AND country_name = 'Россия';

DO
$do$
BEGIN
  IF NOT exists(
      SELECT *
      FROM CITY
        JOIN COUNTRY ON CITY.country_id = COUNTRY.country_id
      GROUP BY 1, 2, 3, 4, 5
      HAVING city_name = 'Душанбе' AND country_name = 'Таджикистан')
  THEN
    BEGIN
      INSERT INTO COUNTRY VALUES (nextval('country_ids'), 'Таджикистан');
      INSERT INTO CITY VALUES (nextval('city_ids'), 'Душанбе', currval('country_ids'));
    END;
  END IF;
END
$do$

IF (COUNT(SELECT *
FROM CITY JOIN COUNTRY ON CITY.country_id=COUNTRY.country_id
GROUP BY 1, 2,3, 4, 5
HAVING city_name='Киев' AND country_name='Украина');
>0);
