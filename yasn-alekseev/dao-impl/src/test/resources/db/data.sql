﻿INSERT INTO COUNTRY (country_name) VALUES ('Россия');
INSERT INTO COUNTRY (country_name) VALUES ('Беларусь');
INSERT INTO COUNTRY (country_name) VALUES ('Казахстан');
INSERT INTO COUNTRY (country_name) VALUES ('Украина');
INSERT INTO COUNTRY (country_name) VALUES ('США');


INSERT INTO CITY (city_name, country_id) VALUES ('Москва', 1);
INSERT INTO CITY (city_name, country_id) VALUES ('Санкт-петербург', 1);
INSERT INTO CITY (city_name, country_id) VALUES ('Владивосток', 1);
INSERT INTO CITY (city_name, country_id) VALUES ('Минск', 2);
INSERT INTO CITY (city_name, country_id) VALUES ('Гомель', 2);
INSERT INTO CITY (city_name, country_id) VALUES ('Бобруйск', 2);
INSERT INTO CITY (city_name, country_id) VALUES ('Алма-Аты', 3);
INSERT INTO CITY (city_name, country_id) VALUES ('Жезказган', 3);
INSERT INTO CITY (city_name, country_id) VALUES ('Караганда', 3);
INSERT INTO CITY (city_name, country_id) VALUES ('Очаков', 4);
INSERT INTO CITY (city_name, country_id) VALUES ('Николаев', 4);
INSERT INTO CITY (city_name, country_id) VALUES ('Вашингтон', 5);
INSERT INTO CITY (city_name, country_id) VALUES ('Нью-Йорк', 5);
INSERT INTO CITY (city_name, country_id) VALUES ('Филадельфия', 5);
INSERT INTO CITY (city_name, country_id) VALUES ('Готэм', 5);

INSERT INTO USER_LIST (email, password, name, surname, patronymic, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('abc@xyz.com', '12345', 'Иван', 'Данилов', 'Петрович', TRUE, 1, 1, 1, 1, '1999-01-08');
INSERT INTO USER_LIST (email, password, name, surname, patronymic, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('new@xyz.com', '12345', 'Алексей', 'Моль', 'Игнатьевич', TRUE, 2, 4, 2, 4, '1998-06-08');
INSERT INTO USER_LIST (email, password, name, surname, patronymic, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('xyz@xyz.com', '12345', 'Генадий', 'Моль', 'Игнатьевич', TRUE, 3, 7, 3, 7, '1998-06-08');
INSERT INTO USER_LIST (email, password, name, surname, patronymic, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('fgh@xyz.com', '12345', 'Петр', 'Моль', 'Игнатьевич', TRUE, 4, 10, 4, 11, '1998-06-08');
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('bcd@xyz.com', '12345', 'Иван', 'Данилов');
INSERT INTO USER_LIST (email, password, name, surname, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('ghj@xyz.com', '12345', 'John', 'Snow', NULL, 5, 12, 5, 12, '1989-06-08');
INSERT INTO USER_LIST (email, password, name, surname, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('hjk@xyz.com', '12345', 'Bill', 'Clinton', NULL, 5, 12, 5, 13, '1955-06-08');
INSERT INTO USER_LIST (email, password, name, surname, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('jkl@xyz.com', '12345', 'George', 'Bush', NULL, NULL, NULL, 5, 13, '1952-06-08');
INSERT INTO USER_LIST (email, password, name, surname, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('klm@xyz.com', '12345', 'Bruce', 'Wayne', NULL, 5, 15, 5, 15, '1984-06-08');


INSERT INTO RELATIONS (user_id1, user_id2) VALUES (1, 2);
INSERT INTO RELATIONS (user_id1, user_id2) VALUES (2, 1);
INSERT INTO RELATIONS (user_id1, user_id2) VALUES (1, 4);
INSERT INTO RELATIONS (user_id1, user_id2) VALUES (4, 1);

INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (1, 'PROFILE_VIEW', 'ALL');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (1, 'MESSAGES_VIEW', 'FRIENDS');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (1, 'WALL_WRITE', 'ME');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (2, 'PROFILE_VIEW', 'ME');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (2, 'MESSAGES_VIEW', 'FRIENDS');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (2, 'WALL_WRITE', 'ME');

INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
VALUES (1, 2, TIMESTAMP '2016-01-07 12:00:00', 'Happy Christmas', FALSE);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
VALUES (2, 1, TIMESTAMP '2016-01-07 12:50:00', 'Happy Christmas', FALSE);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
VALUES (1, 2, TIMESTAMP '2016-02-23 12:00:00', 'Happy man`s day', FALSE);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
VALUES (2, 1, TIMESTAMP '2016-02-14 12:00:00', 'Happy Christmas', FALSE);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
VALUES (2, 1, TIMESTAMP '2016-02-14 12:00:01', 'Happy Valentine', FALSE);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
VALUES (1, 3, TIMESTAMP '2016-01-01 12:00:01', 'Happy New Year', FALSE);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
VALUES (5, 1, TIMESTAMP '2016-02-14 12:00:01', 'Happy Valentine', FALSE);

INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
VALUES (1, 2, TIMESTAMP '2016-01-07 12:00:00', 'Happy Christmas', FALSE);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
VALUES (2, 1, TIMESTAMP '2016-01-07 12:50:00', 'Happy Christmas', FALSE);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
VALUES (1, 2, TIMESTAMP '2016-02-23 12:00:00', 'Happy man`s day', FALSE);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
VALUES (2, 1, TIMESTAMP '2016-02-14 12:00:00', 'Happy Christmas', FALSE);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
VALUES (2, 1, TIMESTAMP '2016-02-14 12:00:01', 'Happy Valentine', FALSE);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
VALUES (1, 3, TIMESTAMP '2016-01-01 12:00:01', 'Happy New Year', FALSE);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
VALUES (5, 1, TIMESTAMP '2016-02-14 12:00:01', 'Happy Valentine', FALSE);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
VALUES (5, 1, TIMESTAMP '2016-04-01 12:00:01', 'Happy Fool`s Day!', FALSE);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
VALUES (5, 1, TIMESTAMP '2016-01-01 12:00:01', 'Happy First DAY of THE REST OF YOUR LIFE!', FALSE);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
VALUES (1, 1, TIMESTAMP '2016-04-01 12:00:01', 'Just do it', FALSE);

INSERT INTO favourites (user_id, wall_id) VALUES (1, 1);
INSERT INTO favourites (user_id, wall_id) VALUES (1, 3);
INSERT INTO favourites (user_id, wall_id) VALUES (1, 5);
INSERT INTO favourites (user_id, wall_id) VALUES (2, 2);
INSERT INTO favourites (user_id, wall_id) VALUES (2, 6);
