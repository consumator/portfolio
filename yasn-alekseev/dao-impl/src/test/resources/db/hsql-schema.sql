CREATE SCHEMA public;

CREATE TABLE COUNTRY (
  country_id   SERIAL PRIMARY KEY,
  country_name VARCHAR NOT NULL UNIQUE
);


CREATE TABLE PICTURE (
  id    SERIAL PRIMARY KEY,
  name  VARCHAR NOT NULL,
  image BYTEA   NOT NULL
);

CREATE TABLE CITY (
  city_id    SERIAL PRIMARY KEY,
  city_name  VARCHAR NOT NULL UNIQUE,
  country_id INT REFERENCES COUNTRY (country_id)
);

CREATE TABLE HOBBY (
  hobby_id   SERIAL PRIMARY KEY,
  hobby_name VARCHAR NOT NULL UNIQUE
);

CREATE TABLE USER_LIST (
  id              SERIAL PRIMARY KEY,
  email           VARCHAR NOT NULL UNIQUE,
  password        VARCHAR NOT NULL,
  name            VARCHAR NOT NULL,
  surname         VARCHAR NOT NULL,
  patronymic      VARCHAR,
  sex             BOOLEAN,
  birth_country   INT REFERENCES COUNTRY (country_id),
  birth_city      INT REFERENCES CITY (city_id),
  current_country INT REFERENCES COUNTRY (country_id),
  current_city    INT REFERENCES CITY (city_id),
  birth_date      DATE,
  avatar          INT REFERENCES PICTURE (id)
);

CREATE TABLE RELATIONS (
  user_id1 INT NOT NULL REFERENCES USER_LIST (id),
  user_id2 INT NOT NULL REFERENCES USER_LIST (id),
  CONSTRAINT friend_cnstrn PRIMARY KEY (user_id1, user_id2),
  CHECK (user_id1 != user_id2)
);

CREATE TABLE USER_HOBBY (
  user_id  INT NOT NULL REFERENCES USER_LIST (id),
  hobby_id INT NOT NULL REFERENCES HOBBY (hobby_id),
  CONSTRAINT hby_cnstr PRIMARY KEY (user_id, hobby_id)
);

CREATE TABLE USER_SETTINGS (
  id            SERIAL PRIMARY KEY,
  user_id       INT     NOT NULL REFERENCES USER_LIST (id),
  setting_type  VARCHAR NOT NULL,
  setting_value VARCHAR NOT NULL,
  CONSTRAINT stng_cnstr UNIQUE (user_id, setting_type)
);

CREATE TABLE MSG (
  id         SERIAL PRIMARY KEY,
  author     INT       NOT NULL REFERENCES USER_LIST (id),
  receiver   INT       NOT NULL REFERENCES USER_LIST (id),
  time_stamp TIMESTAMP NOT NULL,
  text       VARCHAR   NOT NULL,
  is_read    BOOLEAN   NOT NULL,
  image      INT REFERENCES PICTURE (id),
  CONSTRAINT msg_cnstr UNIQUE (author, receiver, time_stamp),
  CONSTRAINT msg_cnstr1 CHECK (author != receiver)
);

CREATE TABLE WALL (
  id         SERIAL PRIMARY KEY,
  from_id    INT       NOT NULL REFERENCES USER_LIST (id),
  owner_id   INT       NOT NULL REFERENCES USER_LIST (id),
  time_stamp TIMESTAMP NOT NULL,
  text       VARCHAR   NOT NULL,
  is_read    BOOLEAN,
  image      INT REFERENCES PICTURE (id),
  CONSTRAINT wall_cnstr UNIQUE (from_id, owner_id, time_stamp)
);

CREATE TABLE FAVOURITES (
  user_id INT REFERENCES USER_LIST (id),
  wall_id INT REFERENCES wall (id),
  CONSTRAINT fav_cnstr UNIQUE (user_id, wall_id)
);
