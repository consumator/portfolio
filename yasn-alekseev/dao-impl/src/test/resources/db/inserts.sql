
INSERT INTO PROPS (name,value) VALUES ('path','f:\обучение\inmemory\java-web\yasn-alekseev\yasn-war\src\main\webapp\resources\images\');
INSERT INTO COUNTRY (country_name) VALUES ('Россия');
INSERT INTO COUNTRY (country_name) VALUES ('Беларусь');
INSERT INTO COUNTRY (country_name) VALUES ('Казахстан');
INSERT INTO COUNTRY (country_name) VALUES ('Украина');
INSERT INTO COUNTRY (country_name) VALUES ('США');



INSERT INTO CITY (city_name, country_id) VALUES ('Москва', 1);
INSERT INTO CITY (city_name, country_id) VALUES ('Санкт-петербург', 1);
INSERT INTO CITY (city_name, country_id) VALUES ('Владивосток', 1);
INSERT INTO CITY (city_name, country_id) VALUES ('Минск', 2);
INSERT INTO CITY (city_name, country_id) VALUES ('Гомель', 2);
INSERT INTO CITY (city_name, country_id) VALUES ('Бобруйск', 2);
INSERT INTO CITY (city_name, country_id) VALUES ('Алма-Аты', 3);
INSERT INTO CITY (city_name, country_id) VALUES ('Жезказган', 3);
INSERT INTO CITY (city_name, country_id) VALUES ('Караганда', 3);
INSERT INTO CITY (city_name, country_id) VALUES ('Очаков', 4);
INSERT INTO CITY (city_name, country_id) VALUES ('Николаев', 4);
INSERT INTO CITY (city_name, country_id) VALUES ('Вашингтон', 5);
INSERT INTO CITY (city_name, country_id) VALUES ('Нью-Йорк', 5);
INSERT INTO CITY (city_name, country_id) VALUES ('Филадельфия', 5);
INSERT INTO CITY (city_name, country_id) VALUES ('Готэм', 5);


INSERT INTO USER_LIST (email, password, name, surname, patronymic, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('abc@xyz.com', '12345', 'Иван', 'Данилов', 'Петрович', TRUE, 1, 1, 1, 1, '1999-01-08');
INSERT INTO USER_LIST (email, password, name, surname, patronymic, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('new@xyz.com', '12345', 'Алексей', 'Моль', 'Игнатьевич', TRUE, 2, 4, 2, 4, '1998-06-08');
INSERT INTO USER_LIST (email, password, name, surname, patronymic, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('xyz@xyz.com', '12345', 'Генадий', 'Моль', 'Игнатьевич', TRUE, 3, 7, 3, 7, '1998-06-08');
INSERT INTO USER_LIST (email, password, name, surname, patronymic, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('fgh@xyz.com', '12345', 'Петр', 'Моль', 'Игнатьевич', TRUE, 4, 10, 4, 11, '1998-06-08');
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('bcd@xyz.com', '12345', 'Иван', 'Данилов');
INSERT INTO USER_LIST (email, password, name, surname, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('ghj@xyz.com', '12345', 'John', 'Snow', NULL, 5, 12, 5, 12, '1989-06-08');
INSERT INTO USER_LIST (email, password, name, surname, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('hjk@xyz.com', '12345', 'Bill', 'Clinton', NULL, 5, 12, 5, 13, '1955-06-08');
INSERT INTO USER_LIST (email, password, name, surname, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('klm@xyz.com', '12345', 'Bruce', 'Wayne', NULL, 5, 15, 5, 15, '1984-06-08');
INSERT INTO USER_LIST (email, password, name, surname, sex, birth_country, birth_city, current_country, current_city, birth_date)
VALUES ('jkl@xyz.com', '12345', 'George', 'Bush', NULL, NULL, NULL, 5, 13, '1952-06-08');



INSERT INTO RELATIONS (user_id1, user_id2) VALUES (1, 2);
INSERT INTO RELATIONS (user_id1, user_id2) VALUES (2, 1);
INSERT INTO RELATIONS (user_id1, user_id2) VALUES (1, 4);
INSERT INTO RELATIONS (user_id1, user_id2) VALUES (4, 1);


INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (1, 'PROFILE_VIEW', 'ALL');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (1, 'MESSAGES_VIEW', 'FRIENDS');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (1, 'WALL_WRITE', 'ME');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (2, 'PROFILE_VIEW', 'ALL');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (2, 'MESSAGES_VIEW', 'FRIENDS');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (2, 'WALL_WRITE', 'ME');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (3, 'PROFILE_VIEW', 'ALL');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (3, 'MESSAGES_VIEW', 'FRIENDS');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (3, 'WALL_WRITE', 'ME');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (4, 'PROFILE_VIEW', 'ALL');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (4, 'MESSAGES_VIEW', 'FRIENDS');
INSERT INTO USER_SETTINGS (user_id, setting_type, setting_value) VALUES (4, 'WALL_WRITE', 'ME');



INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
values (1, 2, TIMESTAMP '2016-01-07 12:00:00', 'Happy Christmas', false);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
values (2, 1, TIMESTAMP '2016-01-07 12:50:00', 'Happy Christmas', false);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
values (1, 2, TIMESTAMP '2016-02-23 12:00:00', 'Happy man`s day', false);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
values (2, 1, TIMESTAMP '2016-02-14 12:00:00', 'Happy Christmas', false);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
values (2, 1, TIMESTAMP '2016-02-14 12:00:01', 'Happy Valentine', false);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
values (1, 3, TIMESTAMP '2016-01-01 12:00:01', 'Happy New Year', false);
INSERT INTO MSG (author, receiver, time_stamp, text, is_read)
values (5, 1, TIMESTAMP '2016-02-14 12:00:01', 'Happy Valentine', false);



INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
values (1, 2, TIMESTAMP '2016-01-07 12:00:00', 'Happy Christmas', false);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
values (2, 1, TIMESTAMP '2016-01-07 12:50:00', 'Happy Christmas', false);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
values (1, 2, TIMESTAMP '2016-02-23 12:00:00', 'Happy man`s day', false);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
values (2, 1, TIMESTAMP '2016-02-14 12:00:00', 'Happy Christmas', false);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
values (2, 1, TIMESTAMP '2016-02-14 12:00:01', 'Happy Valentine', false);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
values (1, 3, TIMESTAMP '2016-01-01 12:00:01', 'Happy New Year', false);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
values (5, 1, TIMESTAMP '2016-02-14 12:00:01', 'Happy Valentine', false);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
values (5, 1, TIMESTAMP '2016-04-01 12:00:01', 'Happy Fool`s Day!', false);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
values (5, 1, TIMESTAMP '2016-01-01 12:00:01', 'Happy First DAY of THE REST OF YOUR LIFE!', false);
INSERT INTO WALL (from_id, owner_id, time_stamp, text, is_read)
values (1, 1, TIMESTAMP '2016-04-01 12:00:01', 'Just do it', false);
INSERT INTO PICTURE (name,image) VALUES ('1.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'1.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('2.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'2.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('3.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'3.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('4.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'4.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('5.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'5.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('6.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'6.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('7.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'7.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('8.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'8.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('9.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'9.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('10.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'10.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('11.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'11.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('12.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'12.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('13.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'13.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('14.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'14.jpg')));
INSERT INTO PICTURE (name,image) VALUES ('15.jpg', load_file(concat((SELECT value FROM PROPS WHERE  name='path'),'15.jpg')));




UPDATE USER_LIST SET avatar=1 WHERE id=1;
UPDATE USER_LIST SET avatar=2 WHERE id=2;
UPDATE USER_LIST SET avatar=3 WHERE id=3;
UPDATE USER_LIST SET avatar=4 WHERE id=4;
UPDATE USER_LIST SET avatar=5 WHERE id=5;
UPDATE USER_LIST SET avatar=6 WHERE id=6;
UPDATE USER_LIST SET avatar=7 WHERE id=7;
UPDATE USER_LIST SET avatar=8 WHERE id=8;
UPDATE USER_LIST SET avatar=9 WHERE id=9;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc0@xyz.com', '12345','Андрон','Виноградов');
UPDATE USER_LIST SET avatar=4 WHERE id=10;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc1@xyz.com', '12345','Викторин','Карпов');
UPDATE USER_LIST SET avatar=4 WHERE id=11;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc2@xyz.com', '12345','Казимир','Белов');
UPDATE USER_LIST SET avatar=14 WHERE id=12;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc3@xyz.com', '12345','Авдей','Марков');
UPDATE USER_LIST SET avatar=12 WHERE id=13;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc4@xyz.com', '12345','Авксентий','Антонов');
UPDATE USER_LIST SET avatar=2 WHERE id=14;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc5@xyz.com', '12345','Аверьян','Морозов');
UPDATE USER_LIST SET avatar=7 WHERE id=15;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc6@xyz.com', '12345','Митофан','Богданов');
UPDATE USER_LIST SET avatar=4 WHERE id=16;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc7@xyz.com', '12345','Давыд','Константинов');
UPDATE USER_LIST SET avatar=6 WHERE id=17;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc8@xyz.com', '12345','Игорь','Герасимов');
UPDATE USER_LIST SET avatar=11 WHERE id=18;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc9@xyz.com', '12345','Януарий','Савельев');
UPDATE USER_LIST SET avatar=12 WHERE id=19;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc10@xyz.com', '12345','Севастьян','Абрамов');
UPDATE USER_LIST SET avatar=6 WHERE id=20;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc11@xyz.com', '12345','Евдоким','Морозов');
UPDATE USER_LIST SET avatar=1 WHERE id=21;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc12@xyz.com', '12345','Зосима','Гусев');
UPDATE USER_LIST SET avatar=12 WHERE id=22;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc13@xyz.com', '12345','Ульян','Борисов');
UPDATE USER_LIST SET avatar=15 WHERE id=23;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc14@xyz.com', '12345','Пахом','Романов');
UPDATE USER_LIST SET avatar=14 WHERE id=24;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc15@xyz.com', '12345','Давыд','Назаров');
UPDATE USER_LIST SET avatar=3 WHERE id=25;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc16@xyz.com', '12345','Натан','Шмидт');
UPDATE USER_LIST SET avatar=11 WHERE id=26;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc17@xyz.com', '12345','Фотий','Шмидт');
UPDATE USER_LIST SET avatar=10 WHERE id=27;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc18@xyz.com', '12345','Устин','Львов');
UPDATE USER_LIST SET avatar=7 WHERE id=28;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc19@xyz.com', '12345','Изот','Соловьёв');
UPDATE USER_LIST SET avatar=1 WHERE id=29;
INSERT INTO USER_LIST (email, password, name, surname) VALUES ('abc20@xyz.com', '12345','Селиверст','Карпов');
UPDATE USER_LIST SET avatar=8 WHERE id=30;
