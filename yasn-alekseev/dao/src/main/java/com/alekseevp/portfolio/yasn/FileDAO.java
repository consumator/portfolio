package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Picture;


public interface FileDAO {
    Picture save(Picture picture);

    Picture read(int id);

    void delete(Picture picture);

    Picture getUserAvatar(int id);
}
