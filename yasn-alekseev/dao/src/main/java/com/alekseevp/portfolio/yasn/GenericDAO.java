package com.alekseevp.portfolio.yasn;

interface GenericDAO<Entity> {
    Entity get(int id);

    Entity saveOrUpdate(Entity entity);

    void remove(Entity entity);

    void remove(int id);
}