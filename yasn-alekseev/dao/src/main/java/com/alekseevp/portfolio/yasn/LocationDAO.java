package com.alekseevp.portfolio.yasn;


import com.alekseevp.portfolio.yasn.entities.City;
import com.alekseevp.portfolio.yasn.entities.Country;

import java.util.List;


public interface LocationDAO {
<<<<<<< HEAD
    List<Country> getCountryList();

    List<City> getAllCitiesInCountry(String countryName);

    List<City> getAllCitiesInCountry(Integer countryId);

    boolean saveCity(Country country, String cityName);

    boolean saveCity(Integer countryId, String cityName);

    City getCity(int id);

    City getCity(String cityName, Integer countryId);

    Country getCountry(int countryId);

    Country getCountry(String countryName);

    boolean saveCountry(Country country);

    boolean saveCountry(String name);
=======

    List<Country> getCountryList();
    List<City> getAllCitiesInCountry(String countryName);
    List<City> getAllCitiesInCountry(Integer countryId);
    boolean saveCity(Country country, String cityName);
    boolean saveCity(Integer countryId, String cityName);
    City getCity(int id);
    City getCity(String cityName, Integer countryId);
    Country getCountry(int countryId);
    Country getCountry(String countryName);
    boolean saveCountry(Country country);
    boolean saveCountry (String name);

>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
}