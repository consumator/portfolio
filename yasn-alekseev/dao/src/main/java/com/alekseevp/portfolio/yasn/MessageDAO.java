package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Message;

import java.util.List;


<<<<<<< HEAD
public interface MessageDAO extends GenericDAO<Message> {
    List<Message> getUserMessages(int user);

    List<Message> getFromToMessages(int authorReceiver, boolean isAuthor);

    List<Message> getMutualMessages(int author, int receiver);

    Message saveOrUpdate(Message message);
=======
public interface MessageDAO {
    List<Message> getUserMessages(int user);
    List<Message> getFromToMessages(int authorReceiver, boolean isAuthor);
    List<Message> getMutualMessages(int author, int receiver);
    Message read(int id);
    Message create(Message message);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
}
