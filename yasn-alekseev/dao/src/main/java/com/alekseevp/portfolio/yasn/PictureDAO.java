package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Picture;


public interface PictureDAO extends GenericDAO<Picture> {
    Picture getUserAvatar(int userId);
}
