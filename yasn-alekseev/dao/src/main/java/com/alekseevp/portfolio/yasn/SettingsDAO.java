package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Setting;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingType;

import java.util.Set;


<<<<<<< HEAD
public interface SettingsDAO extends GenericDAO<Setting>{
=======
public interface SettingsDAO {
    boolean createSetting(Setting setting);

    boolean modifySetting(Setting setting);

>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    Setting getSettingByUserAndType(User userId, PrivacySettingType settingType);

    Set<Setting> getUserSettings(User user);
}
