package com.alekseevp.portfolio.yasn.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;


@Entity
@Table(name = "msg")
public class Message implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "author")
    @NotNull
    private User author;

    @ManyToOne
    @JoinColumn(name = "receiver")
    @NotNull
    private User receiver;

    @Column(name = "is_read")
    private Boolean isRead;

    @NotNull
    @Column(name = "time_stamp")
    private Calendar timestamp;

    @NotNull
    @Column(name = "text")
    private String text;

    @OneToOne
    @JoinColumn(name = "image", nullable = true)
    private Picture image;


    public Message(User author, User receiver, String text) {
        this.author = author;
        this.receiver = receiver;
        this.text = text;
        this.timestamp = new GregorianCalendar();
        this.isRead = false;
    }

    public Message() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Picture getImage() {
        return image;
    }

    public void setImage(Picture image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;

        Message message = (Message) o;

        if (getId() != message.getId()) return false;
        if (!getAuthor().equals(message.getAuthor())) return false;
        if (!getReceiver().equals(message.getReceiver())) return false;
        if (isRead != null ? !isRead.equals(message.isRead) : message.isRead != null) return false;
        if (!getTimestamp().equals(message.getTimestamp())) return false;
        return getText().equals(message.getText());

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getAuthor().hashCode();
        result = 31 * result + getReceiver().hashCode();
        result = 31 * result + (isRead != null ? isRead.hashCode() : 0);
        result = 31 * result + getTimestamp().hashCode();
        result = 31 * result + getText().hashCode();
        return result;
    }
}
