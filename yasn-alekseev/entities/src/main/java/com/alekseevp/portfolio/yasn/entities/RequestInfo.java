package com.alekseevp.portfolio.yasn.entities;

import java.util.Collection;

public class RequestInfo {
    private Collection<Country> countryList;
    private Collection<City> birthCountryCityList;
    private Collection<City> currentCountryCityList;

    public RequestInfo(Collection<Country> countryList, Collection<City> birthCountryCityList, Collection<City> currentCountryCityList) {
        this.countryList = countryList;
        this.birthCountryCityList = birthCountryCityList;
        this.currentCountryCityList = currentCountryCityList;
    }

    public Collection<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(Collection<Country> countryList) {
        this.countryList = countryList;
    }

    public Collection<City> getBirthCountryCityList() {
        return birthCountryCityList;
    }

    public Collection<City> getCurrentCountryCityList() {
        return currentCountryCityList;
    }
}
