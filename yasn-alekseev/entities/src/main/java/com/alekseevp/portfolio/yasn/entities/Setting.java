package com.alekseevp.portfolio.yasn.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingType;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingsValue;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "USER_SETTINGS")
public class Setting implements Serializable {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    @Column(name = "setting_type")
    private String settingType;

    @Transient
    private String settingTypeViewName;

    @Transient
    private List<PrivacySettingsValue> possibleValues;

    @NotNull
    @Column(name = "setting_value")
    private String value;




    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
<<<<<<< HEAD
    private int id;
=======
    private Integer id;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

    public Setting() {
    }

    public List<PrivacySettingsValue> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(List<PrivacySettingsValue> possibleValues) {
        this.possibleValues = possibleValues;
    }

    public String getSettingTypeViewName() {
        return settingTypeViewName;
    }

    public void setSettingTypeViewName(String settingTypeViewName) {
        this.settingTypeViewName = settingTypeViewName;
    }

    public PrivacySettingsValue getValue() {
        return PrivacySettingsValue.valueOf(value);
    }

    public void setValue(PrivacySettingsValue value) {
        this.value = value.name();
    }

<<<<<<< HEAD
    public int getId() {
        return id;
    }

    public void setId(int id) {
=======
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PrivacySettingType getSettingType() {
        return PrivacySettingType.valueOf(settingType);
    }

    public void setSettingType(PrivacySettingType settingType) {
        this.settingType = settingType.name();
        this.settingTypeViewName = settingType.getPrivacySettingTypeViewName();
        this.possibleValues = settingType.getPossibleValues();
    }

    public void populateWithDefault(PrivacySettingType settingType){
        setSettingType(settingType);
        this.setValue(PrivacySettingsValue.getDefaultValue(settingType));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Setting)) return false;

        Setting setting = (Setting) o;

        if (!getUser().equals(setting.getUser())) return false;
        if (!getSettingType().equals(setting.getSettingType())) return false;
        if (!getValue().equals(setting.getValue())) return false;
<<<<<<< HEAD
        return getId()==setting.getId();
=======
        return getId().equals(setting.getId());
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

    }

    @Override
    public int hashCode() {
<<<<<<< HEAD
        int result = getSettingType().hashCode();
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        result = 31 * result + getId();
=======
        int result = getUser().hashCode();
        result = 31 * result + getSettingType().hashCode();
        result = 31 * result + getValue().hashCode();
        result = 31 * result + getId().hashCode();
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        return result;
    }
}


