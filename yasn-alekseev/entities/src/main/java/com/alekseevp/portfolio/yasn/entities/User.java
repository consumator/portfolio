package com.alekseevp.portfolio.yasn.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
<<<<<<< HEAD
import java.util.Date;
=======
import java.sql.Date;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import java.util.Set;
import java.util.TreeSet;

import static com.google.common.base.MoreObjects.toStringHelper;


@Entity
@Table(name = "user_list")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User implements Serializable, Comparable<User> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull
<<<<<<< HEAD

    @Column(unique = true, name = "email")
=======
    @Column(name = "email")
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    private String email;

    @JsonIgnore
    @NotNull
    @Column(name = "password")
    private String password;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "surname")
    private String surname;


    @Column(name = "patronymic")
    private String patronymic;

    @ManyToOne
    @JoinColumn(name = "birth_country")
    private Country birthCountry;

    @ManyToOne
    @JoinColumn(name = "birth_city")
    private City birthCity;

    @ManyToOne
    @JoinColumn(name = "current_country")
    private Country currentCountry;

    @ManyToOne
    @JoinColumn(name = "current_city")
    private City currentCity;


    @Column(name = "birth_date")
<<<<<<< HEAD
    @Temporal(TemporalType.DATE)
=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    private Date birthDate;

    @Column(name = "sex", nullable = true)
    private Boolean sex;


<<<<<<< HEAD
    @ManyToOne
=======
    @OneToOne
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    @JoinColumn(name = "avatar", nullable = true)
    private Picture avatar;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "relations",
            joinColumns = @JoinColumn(name = "user_id1", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id2", referencedColumnName = "id"))
    @JsonIgnore
    private Set<User> friends = new TreeSet<>();

<<<<<<< HEAD
=======

    @JsonIgnore
    @OneToMany(mappedBy = "owner")
    private Set<WallPost> wallPosts = new TreeSet<>();


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "favouriteOwners")
    @JsonIgnore
    private Set<WallPost> favourites = new TreeSet<>();

>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    protected User() {
    }


    public User(String email, String password, String name, String surname) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
    }


<<<<<<< HEAD
=======
    public Set<WallPost> getFavourites() {
        return favourites;
    }

    public void setFavourites(Set<WallPost> wallPosts) {
        this.favourites = wallPosts;
    }

    public boolean addWallPostToFavourites(WallPost wallPost) {
        return this.favourites.add(wallPost);
    }

    public boolean removeWallPostFromFavourites(WallPost wallPost) {
        return this.favourites.remove(wallPost);
    }

    public Set<WallPost> getWallPosts() {
        return wallPosts;
    }

    public void setWallPosts(Set<WallPost> wallPosts) {
        this.wallPosts = wallPosts;
    }

    public boolean removeWallPost(WallPost wallPost) {
        return wallPosts.remove(wallPost);
    }

>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Country getBirthCountry() {
        return birthCountry;
    }

    public void setBirthCountry(Country birthCountry) {
        this.birthCountry = birthCountry;
    }

    public City getBirthCity() {
        return birthCity;
    }

    public void setBirthCity(City birthCity) {
        this.birthCity = birthCity;
    }

    public Country getCurrentCountry() {
        return currentCountry;
    }

    public void setCurrentCountry(Country currentCountry) {
        this.currentCountry = currentCountry;
    }

    public City getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(City currentCity) {
        this.currentCity = currentCity;
    }


    public Picture getAvatar() {
        return avatar;
    }

    public void setAvatar(Picture avatar) {
        this.avatar = avatar;
    }

    public boolean addFriend(User user) {
        return !this.equals(user) && this.friends.add(user);
    }

    public Set<User> getFriends() {
        return friends;
    }

    public void setFriends(Set<User> friends) {
        this.friends = friends;
    }


    @Override
    public String toString() {
        return toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("surname", surname)
                .add("email", email)
                .add("password", password)
                .toString();
    }

    public boolean equalsForJSON(User user) {
        if (!(getId() == user.getId())) return false;
        if (!getEmail().equals(user.getEmail())) return false;
        if (!getName().equals(user.getName())) return false;
        return getSurname().equals(user.getSurname());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (!getEmail().equals(user.getEmail())) return false;
        if (!getPassword().equals(user.getPassword())) return false;
        if (!getName().equals(user.getName())) return false;
        if (!getSurname().equals(user.getSurname())) return false;
        if (getPatronymic() != null ? !getPatronymic().equals(user.getPatronymic()) : user.getPatronymic() != null)
            return false;

        return true;
    }


    @Override
    public int hashCode() {
<<<<<<< HEAD
        int result = getEmail().hashCode();
=======
        int result =getEmail().hashCode();
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        result = 31 * result + getPassword().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + (getSurname() != null ? getSurname().hashCode() : 0);
        result = 31 * result + (getPatronymic() != null ? getPatronymic().hashCode() : 0);
        return result;
    }


    @Override
    public int compareTo(User o) {
        return ((Integer) this.getId()).compareTo(o.getId());
    }

    public static class UserView {
        private final int id;
        private final String email;
        private final String name;
        private final String surname;
        private final int avatarId;

        public UserView(User user) {
            this.id = user.getId();
            this.email = user.getEmail();
            this.name = user.getName();
            this.surname = user.getSurname();
            this.avatarId = user.getAvatar() == null ? 0 : user.getAvatar().getId();
        }

        public int getId() {
            return id;
        }

        public String getEmail() {
            return email;
        }

        public String getName() {
            return name;
        }

        public String getSurname() {
            return surname;
        }

        public int getAvatarId() {
            return avatarId;
        }
    }
<<<<<<< HEAD

    public static class UserSearchQueryObject {

        public static final int DEFAULT_SEARCH_RESULTS_PAGE_SIZE = 6;

        private String email;
        private String name;
        private String surname;
        private String patronymic;
        private Boolean sex;
        private boolean birthDateGreaterThan;
        private Date birthDate;
        private int birthCountry;
        private int birthCity;
        private int currentCountry;
        private int currentCity;

        private String quickSearchString;
        private boolean quickSearch;
        private int pageSize = DEFAULT_SEARCH_RESULTS_PAGE_SIZE;
        private int pageNumber;

        private int resultsPageCount;

        public UserSearchQueryObject() {
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getPatronymic() {
            return patronymic;
        }

        public void setPatronymic(String patronymic) {
            this.patronymic = patronymic;
        }

        public Boolean getSex() {
            return sex;
        }

        public void setSex(Boolean sex) {
            this.sex = sex;
        }

        public Date getBirthDate() {
            return birthDate;
        }

        public void setBirthDate(Date birthDate) {
            this.birthDate = birthDate;
        }

        public int getBirthCountry() {
            return birthCountry;
        }

        public void setBirthCountry(int birthCountry) {
            this.birthCountry = birthCountry;
        }

        public int getBirthCity() {
            return birthCity;
        }

        public void setBirthCity(int birthCity) {
            this.birthCity = birthCity;
        }

        public int getCurrentCountry() {
            return currentCountry;
        }

        public void setCurrentCountry(int currentCountry) {
            this.currentCountry = currentCountry;
        }

        public int getCurrentCity() {
            return currentCity;
        }

        public void setCurrentCity(int currentCity) {
            this.currentCity = currentCity;
        }

        public String getQuickSearchString() {
            return quickSearchString;
        }

        public void setQuickSearchString(String quickSearchString) {
            this.quickSearchString = quickSearchString;
        }

        public boolean isQuickSearch() {
            return quickSearch;
        }

        public void setQuickSearch(boolean quickSearch) {
            this.quickSearch = quickSearch;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getPageNumber() {
            return pageNumber;
        }

        public void setPageNumber(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        public boolean isBirthDateGreaterThan() {
            return birthDateGreaterThan;
        }

        public void setBirthDateGreaterThan(boolean birthDateGreaterThan) {
            this.birthDateGreaterThan = birthDateGreaterThan;
        }

        public int getResultsPageCount() {
            return resultsPageCount;
        }

        public void setResultsPageCount(int resultsPageCount) {
            this.resultsPageCount = resultsPageCount;
        }
    }
=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
}
