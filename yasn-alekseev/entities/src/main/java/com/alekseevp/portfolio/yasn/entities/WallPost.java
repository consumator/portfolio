package com.alekseevp.portfolio.yasn.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

@Entity
@Table(name = "wall")
public class WallPost implements Serializable, Comparable<WallPost> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "from_id")
    private User from;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id")
    private User owner;

    @NotNull
    @Column(name = "time_stamp")
    private Calendar timestamp;

    @NotNull
    @Column(name = "text")
    private String text;

    @Column(name = "is_read")
    private Boolean isRead;

    @OneToOne
    @JoinColumn(name = "image", nullable = true)
    private Picture image;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "favourites",
            joinColumns = @JoinColumn(name = "wall_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    @JsonIgnore
    private Set<User> favouriteOwners = new TreeSet<>();


    public WallPost(String message, User owner, User author) {
        this.from = author;
        this.owner = owner;
        this.timestamp = new GregorianCalendar();
        this.text = message;
    }

    public WallPost() {
    }

    public boolean addUserToFavouriteOwners(User user) {
        return this.favouriteOwners.add(user);
    }

    public boolean removeUserFromFavouriteOwners(User user) {
        return this.favouriteOwners.remove(user);
    }

    public Set<User> getFavouriteOwners() {
        return favouriteOwners;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public Picture getImage() {
        return image;
    }

    public void setImage(Picture image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

<<<<<<< HEAD
=======
    public void setFavouriteOwners(Set<User> favouriteOwners) {
        this.favouriteOwners = favouriteOwners;
    }

>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WallPost)) return false;

        WallPost post = (WallPost) o;

        if (!getFrom().equals(post.getFrom())) return false;
        if (!getOwner().equals(post.getOwner())) return false;
        if (!getTimestamp().equals(post.getTimestamp())) return false;
        if (!getText().equals(post.getText())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = getFrom().getId();
        result = 31 * result + getOwner().getId();
        result = 31 * result + getTimestamp().hashCode();
        result = 31 * result + getText().hashCode();
        result = 31 * result + (getIsRead() != null ? getIsRead().hashCode() : 0);
        result = 31 * result + (getImage() != null ? getImage().hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(WallPost o) {
        if (this.equals(o)) {
            return 0;
        }
        return o.getTimestamp().getTime().compareTo(this.getTimestamp().getTime());
    }

    public static class WallPostView {
        private final String from;
        private final int from_id;
        private final String text;
        private final int avatarId;
        private final int id;
        private boolean favourite;
        private String time_stamp;


        public WallPostView(WallPost post, boolean isFavourite) {
            this.favourite = isFavourite;
            this.id = post.getId();
            this.from = post.getFrom().getEmail();
            this.from_id = post.getFrom().getId();
            this.text = post.getText();
            SimpleDateFormat format1 = new SimpleDateFormat("YYYY-MM-dd");
            this.time_stamp = format1.format(post.getTimestamp().getTime());
            this.avatarId = post.getFrom().getAvatar() == null ? 0 : post.getFrom().getAvatar().getId();
        }

        public String getFrom() {
            return from;
        }

        public int getFrom_id() {
            return from_id;
        }

        public String getText() {
            return text;
        }

        public int getAvatarId() {
            return avatarId;
        }

        public int getId() {
            return id;
        }

        public boolean isFavourite() {
            return favourite;
        }

        public String getTime_stamp() {
            return time_stamp;
        }
    }
}
