package com.alekseevp.portfolio.yasn.entities.settingEnums;

import java.util.List;


public enum PrivacySettingType {
    PROFILE_VIEW, MESSAGES_WRITE, WALL_WRITE;


    public List<PrivacySettingsValue> getPossibleValues() {
        switch (this) {
            case PROFILE_VIEW:
                return PrivacySettingsValue.getPossibleValues();
            case MESSAGES_WRITE:
                return PrivacySettingsValue.getPossibleValues();
            case WALL_WRITE:
                return PrivacySettingsValue.getPossibleValues();
            default:
                return null;
        }
    }


    public PrivacySettingsValue getDefaultValue() {
        switch (this) {
            case PROFILE_VIEW:
                return PrivacySettingsValue.getDefaultValue(this);
            case MESSAGES_WRITE:
                return PrivacySettingsValue.getDefaultValue(this);
            case WALL_WRITE:
                return PrivacySettingsValue.getDefaultValue(this);
            default:
                return null;
        }
    }

    public String getPrivacySettingTypeViewName() {
        switch (this) {
            case PROFILE_VIEW:
                return "Who can view my profile";
            case MESSAGES_WRITE:
                return "Who can write messages to me";
            case WALL_WRITE:
                return "Who can write at my wall";
            default:
                return null;
        }
    }


    @Override
    public String toString() {
        switch (this) {
            case PROFILE_VIEW:
                return "Who can view my profile";
            case MESSAGES_WRITE:
                return "Who can write messages to me";
            case WALL_WRITE:
                return "Who can write at my wall";
            default:
                return null;
        }
    }
}
