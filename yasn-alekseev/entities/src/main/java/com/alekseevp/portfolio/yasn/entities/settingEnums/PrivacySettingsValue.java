package com.alekseevp.portfolio.yasn.entities.settingEnums;

import java.util.Arrays;
import java.util.List;

public enum PrivacySettingsValue {
    ALL, FRIENDS, ME;

    public static PrivacySettingsValue getDefaultValue(PrivacySettingType type) {
        return ALL;
    }

    public static List<PrivacySettingsValue> getPossibleValues() {
        return Arrays.asList(PrivacySettingsValue.values());
    }


    @Override
    public String toString() {
        switch (this) {
            case ALL:
                return "All";
            case FRIENDS:
                return "Friends";
            case ME:
                return "Me";
            default:
                return null;
        }
    }
}
