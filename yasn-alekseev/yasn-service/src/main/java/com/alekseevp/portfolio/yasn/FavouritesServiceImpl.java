package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.WallPost;
<<<<<<< HEAD
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

=======
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.TreeSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
public class FavouritesServiceImpl implements FavouritesService {
    private static final Logger logger = LogManager.getLogger(FavouritesServiceImpl.class);

    @Autowired
    WallDAO wallDAO;

    @Autowired
    UserDAO userDAO;

    @Override
    @Transactional
    public boolean addFavourite(User user, WallPost wallPost) {
<<<<<<< HEAD
        if (wallPost != null) {
            wallPost.addUserToFavouriteOwners(user);
            wallDAO.saveOrUpdate(wallPost);
=======
        try {
            if (wallPost != null) {
                wallPost.addUserToFavouriteOwners(user);
                user.addWallPostToFavourites(wallPost);
                wallDAO.update(wallPost);
            }
        } catch (Exception e){
           logger.error("Error while adding favourite", e);
            return false;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        }
        return true;
    }

    @Override
    @Transactional
    public boolean removeFavourite(User user, WallPost wallPost) {
<<<<<<< HEAD
        if (wallPost != null) {
            wallPost.removeUserFromFavouriteOwners(user);
            wallDAO.saveOrUpdate(wallPost);
            return true;
        }
        return false;
=======
        try {
            if (wallPost != null) {
                wallPost.removeUserFromFavouriteOwners(user);
                user.removeWallPostFromFavourites(wallPost);
                wallDAO.update(wallPost);
            }
        } catch (Exception e){
            logger.error("Error while removing favourite", e);
            return false;
        }
        return true;
    }

    @Override
    public Set<WallPost> getFavourites(User user) {
        try {
            return user.getFavourites();
        } catch (Exception e){
            logger.error("Error while removing favourite", e);
            return new TreeSet<>();
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    @Transactional
    public boolean addFavourite(User user, Integer wallPostId) {
<<<<<<< HEAD
        WallPost wallPost = wallDAO.get(wallPostId);
=======
        WallPost wallPost = wallDAO.read(wallPostId);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        return addFavourite(user, wallPost);
    }

    @Override
    @Transactional
    public boolean removeFavourite(User user, Integer wallPostId) {
<<<<<<< HEAD
        WallPost wallPost = wallDAO.get(wallPostId);
=======
        WallPost wallPost = wallDAO.read(wallPostId);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        return removeFavourite(user, wallPost);
    }
}
