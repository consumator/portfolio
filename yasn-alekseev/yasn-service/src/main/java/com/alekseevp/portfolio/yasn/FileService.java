package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Picture;
import org.springframework.web.multipart.MultipartFile;


public interface FileService {

    Picture uploadFile(MultipartFile file);

    Picture getPicture(int id);

    Picture getUserAvatar(Integer profileId);
}
