package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Picture;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class FileServiceImpl implements FileService {
    private static final Logger logger = LogManager.getLogger(FileServiceImpl.class);

    @Autowired
<<<<<<< HEAD
    private PictureDAO fileDAO;
=======
    private FileDAO fileDAO;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

    @Override
    @Transactional
    public Picture uploadFile(MultipartFile file) {
        Picture uploadFile = new Picture();
        System.out.println(file.getOriginalFilename());
        uploadFile.setName(file.getOriginalFilename());
        try {
            uploadFile.setImage(file.getBytes());
        } catch (IOException e) {
            logger.error("Error accessing IO", e);
            return null;
        }
<<<<<<< HEAD
        return fileDAO.saveOrUpdate(uploadFile);
=======
        try {
            return fileDAO.save(uploadFile);
        } catch (Exception e){
            logger.error("Error while saving uploaded file", e);
            return null;
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    public Picture getPicture(int id) {
<<<<<<< HEAD
        return fileDAO.get(id);
=======
        try {
            return fileDAO.read(id);
        } catch (Exception e){
            logger.error("Error while reading picture from DB",e);
            return null;
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    public Picture getUserAvatar(Integer profileId) {
<<<<<<< HEAD
        return fileDAO.getUserAvatar(profileId);
=======
        try {
            return fileDAO.getUserAvatar(profileId);
        } catch (Exception e){
            logger.error("Error while retreiving avatar from DB",e);
            return null;
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }
}
