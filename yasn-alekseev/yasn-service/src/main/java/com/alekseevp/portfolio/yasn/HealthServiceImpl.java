package com.alekseevp.portfolio.yasn;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class HealthServiceImpl implements HealthService {
    private static final Logger logger = LogManager.getLogger(HealthServiceImpl.class);

    @Autowired
    private UserDAO userDAO;

    @Override
    public Long getUserCount() {
        return userDAO.countEntries();
    }
}
