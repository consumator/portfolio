package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.City;
import com.alekseevp.portfolio.yasn.entities.Country;

import java.util.List;


public interface LocationService {
    List<Country> getCountries();

    List<City> getAllCitiesInCountry(String countryName);

    List<City> getAllCitiesInCountry(Integer countryId);

    boolean addNewCity(Integer countryId, String cityName);

    City getCity(Integer cityId);

    Country getCountry(Integer id);

    boolean saveCountry(String s);
}
