package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.City;
import com.alekseevp.portfolio.yasn.entities.Country;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service
public class LocationServiceImpl implements LocationService {
    private static final Logger logger = LogManager.getLogger(LocationServiceImpl.class);

    @Autowired
    private LocationDAO locationDAO;

    @Override
    public List<Country> getCountries() {
<<<<<<< HEAD
        return sortCountriesByName(locationDAO.getCountryList());
=======
        try {
           return sortCountriesByName(locationDAO.getCountryList());
        } catch (Exception e){
            logger.error("Error while reading country list from DB", e);
            return new ArrayList<>();
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    public List<City> getAllCitiesInCountry(String countryName) {
<<<<<<< HEAD
        if (countryName == null) {
            return null;
        }
        return sortCitiesByName(locationDAO.getAllCitiesInCountry(countryName));
    }

    public List<Country> sortCountriesByName(List<Country> countries) {
=======
        if (countryName==null) {
            return null;}
        try {
            return sortCitiesByName(locationDAO.getAllCitiesInCountry(countryName));
        } catch (Exception e){
            logger.error("Error while getting city list",e);
            return new ArrayList<>();
        }
    }

    public  List<Country> sortCountriesByName(List<Country> countries){
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        Collections.sort(countries, new Comparator<Country>() {
            @Override
            public int compare(Country o1, Country o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return countries;
    }

<<<<<<< HEAD
    public List<City> sortCitiesByName(List<City> cities) {
=======
    public  List<City> sortCitiesByName(List<City> cities){
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        Collections.sort(cities, new Comparator<City>() {
            @Override
            public int compare(City o1, City o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return cities;
    }

    @Override
    public List<City> getAllCitiesInCountry(Integer countryId) {
<<<<<<< HEAD
        return sortCitiesByName(locationDAO.getAllCitiesInCountry(countryId));
=======
        try {
            return sortCitiesByName(locationDAO.getAllCitiesInCountry(countryId));
        } catch (Exception e){
            logger.error("Error while getting city list",e);
            return new ArrayList<>();
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    @Transactional
    public boolean addNewCity(Integer countryId, String cityName) {
<<<<<<< HEAD
        return countryId != null && countryId != 0 && locationDAO.saveCity(countryId, cityName);
=======
        try {
            return countryId != null && countryId != 0 && locationDAO.saveCity(countryId, cityName);
        } catch (Exception e){
            logger.error("Error while adding new city",e);
            return false;
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }


    @Override
    public City getCity(Integer cityId) {
<<<<<<< HEAD
        return locationDAO.getCity(cityId);
=======
        try {
        return locationDAO.getCity(cityId);
        } catch (Exception e){
            logger.error("Error while getting city ",e);
            return null;
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    public Country getCountry(Integer id) {
<<<<<<< HEAD
        return locationDAO.getCountry(id);
=======
        try {
        return locationDAO.getCountry(id);
        } catch (Exception e){
            logger.error("Error while getting country ",e);
            return null;
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    @Transactional
    public boolean saveCountry(String countryName) {
<<<<<<< HEAD
        locationDAO.saveCountry(countryName);
=======
        try {
            locationDAO.saveCountry(countryName);
        } catch (Exception e){
            logger.error("Error while saving country ",e);
            return false;
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        return true;
    }


}

