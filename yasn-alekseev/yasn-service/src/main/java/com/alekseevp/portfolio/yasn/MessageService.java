package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Message;
import com.alekseevp.portfolio.yasn.entities.User;

import java.util.List;


public interface MessageService {
    List<Message> getMessages(User user1, Integer user2Id);

    List<Message> getMessages(User author, User receiver);

    List<Message> getMessages(User user);

    Message postMessage(Message message);
}
