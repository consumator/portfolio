package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Message;
import com.alekseevp.portfolio.yasn.entities.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

<<<<<<< HEAD
=======
import java.util.ArrayList;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import java.util.List;


@Service
public class MessageServiceImpl implements MessageService {
    private static final Logger logger = LogManager.getLogger(MessageServiceImpl.class);
    @Autowired
    private MessageDAO messageDAO;

    @Override
    public List<Message> getMessages(User user1, Integer user2Id) {
<<<<<<< HEAD
            return messageDAO.getMutualMessages(user1.getId(), user2Id);
=======
        try {
            return messageDAO.getMutualMessages(user1.getId(), user2Id);
        } catch (Exception e) {
            logger.error("Error while getting message ", e);
            return new ArrayList<>();
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    public List<Message> getMessages(User user1, User user2) {
<<<<<<< HEAD
            return messageDAO.getMutualMessages(user1.getId(), user2.getId());
=======
        try {
            return messageDAO.getMutualMessages(user1.getId(), user2.getId());
        } catch (Exception e) {
            logger.error("Error while getting message ", e);
            return null;
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    public List<Message> getMessages(User user) {
<<<<<<< HEAD
        return messageDAO.getUserMessages(user.getId());
=======
        try {
        return messageDAO.getUserMessages(user.getId());
        } catch (Exception e){
            logger.error("Error while getting all messages",e);
            return new ArrayList<>();
        }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    @Transactional
    public Message postMessage(Message message) {
        try {
<<<<<<< HEAD
            return messageDAO.saveOrUpdate(message);
=======
            return messageDAO.create(message);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        } catch (Exception e){
            logger.error("Error while creating message");
            return null;
        }
    }
}
