package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Setting;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingType;

import java.util.List;


public interface SettingsService {

    Setting saveSetting(Setting setting);

    Setting getSetting(User user, PrivacySettingType settingType);

    List<Setting> getSettings(User user);

    boolean isAllowedToViewPage(User visitor, User owner);

    boolean isAllowedToWriteMessages(User visitor, User owner);

    boolean isAllowedToWallPost(User visitor, User owner);

    Setting getProfileViewSetting(User user);

    Setting getMessagesWriteSetting(User user);

    Setting getWallWriteSetting(User user);
}
