package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.Setting;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingType;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingsValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class SettingsServiceImpl implements SettingsService {
    private static final Logger logger = LogManager.getLogger(SettingsServiceImpl.class);
    @Autowired
    private SettingsDAO settingsDAO;


    @Override
    @Transactional
    public Setting saveSetting(Setting setting) {
        if (setting != null) {
            try {
                if (setting.getUser() != null && setting.getSettingType() != null) {
                    if (settingsDAO.getSettingByUserAndType(setting.getUser(), setting.getSettingType()) == null) {
<<<<<<< HEAD
                        settingsDAO.saveOrUpdate(setting);
                    } else settingsDAO.saveOrUpdate(setting);
=======
                        settingsDAO.createSetting(setting);
                    } else settingsDAO.modifySetting(setting);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
                }
                return setting;
            } catch (Exception e){
                logger.error("Error while saving setting",e);
            }
        }
        return null;
    }

    @Override
    @Transactional
    public Setting getSetting(User user, PrivacySettingType settingType) {
        Setting setting=null;
        try {
             setting= settingsDAO.getSettingByUserAndType(user, settingType);
        } catch (Exception e){
            logger.error("Error while getting setting", e);
            return null;
        }
        if (setting == null) {
            return generateDefaultSetting(user, settingType);
        }
        return setting;
    }

    @Transactional
    private Setting generateDefaultSetting(User user, PrivacySettingType settingType) {
        Setting setting = new Setting();
        setting.setUser(user);
        setting.setSettingType(settingType);
        setting.setValue(PrivacySettingsValue.getDefaultValue(settingType));
<<<<<<< HEAD
        settingsDAO.saveOrUpdate(setting);
=======
        settingsDAO.createSetting(setting);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        return setting;
    }

    @Override
    @Transactional
    public List<Setting> getSettings(User user) {
        List<Setting> settings = new ArrayList<>();
        for (PrivacySettingType privacySettingType : PrivacySettingType.values()) {
            settings.add(getSetting(user, privacySettingType));
        }
        return settings;
    }

    @Override
    @Transactional
    public boolean isAllowedToViewPage(User visitor, User owner) {
        if (visitor != null && owner != null) {
            return isActionAllowed(getProfileViewSetting(owner), owner, visitor);
        }
        return false;
    }

    @Override
    @Transactional
    public boolean isAllowedToWriteMessages(User visitor, User owner) {
        if (visitor != null && owner != null) {
            return isActionAllowed(getMessagesWriteSetting(owner), owner, visitor);
        }
        return false;
    }

    @Override
    @Transactional
    public boolean isAllowedToWallPost(User visitor, User owner) {
        if (visitor != null && owner != null) {
            return isActionAllowed(getWallWriteSetting(owner), owner, visitor);
        }
        return false;
    }

    private Boolean isActionAllowed(Setting setting, User owner, User visitor) {
        switch (setting.getValue()) {
            case ALL:
                return true;
            case FRIENDS:
                return owner.getFriends().contains(visitor);
            case ME:
                return false;
        }
        return false;
    }

    @Override
    @Transactional
    public Setting getProfileViewSetting(User user) {
        return getSetting(user, PrivacySettingType.PROFILE_VIEW);
    }

    @Override
    @Transactional
    public Setting getMessagesWriteSetting(User user) {
        return getSetting(user, PrivacySettingType.MESSAGES_WRITE);
    }

    @Override
    @Transactional
    public Setting getWallWriteSetting(User user) {
        return getSetting(user, PrivacySettingType.WALL_WRITE);
    }


}
