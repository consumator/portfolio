package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
<<<<<<< HEAD
=======
import java.util.Map;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

@Service
public class UserServiceImpl implements UserService {
    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);
    @Autowired
    private UserDAO userDAO;

    @Override
    public User getProfile(String email) {
<<<<<<< HEAD
        return userDAO.getByEmail(email);
=======
        return userDAO.getUser(email);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    public User getProfile(String email, String password) {
        if (email != null && !"".equals(email) && password != null && !"".equals(password)) {
<<<<<<< HEAD
            User userInDb = userDAO.getByEmail(email);
=======
            User userInDb = userDAO.getUser(email);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
            if (userInDb != null) {
                return (userInDb.getPassword().equals(password)) ? userInDb : null;
            }
        }
        return null;
    }

    @Override
<<<<<<< HEAD
    public List<User> search(User.UserSearchQueryObject queryObject) {
        return userDAO.search(queryObject);
    }

//    @Override
//    public List<User> quickSearch(String searchParams) {
//        return userDAO.quickSearch(searchParams);
//    }
=======
    public List<User> search(Map<String, String> searchParams) {
        return userDAO.criteriaSearch(searchParams);
    }

    @Override
    public List<User> quickSearch(String searchParams) {
        return userDAO.quickSearch(searchParams);
    }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

    @Override
    public Boolean getViewPermission(Integer owner, Integer visitor) {
        return null;
    }

    @Override
    public User getProfile(int id) {
<<<<<<< HEAD
        return userDAO.get(id);
=======
        return userDAO.getUser(id);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    @Transactional
    public Boolean addFriend(User authorizedUser, Integer profileId) {
<<<<<<< HEAD
        authorizedUser.addFriend(userDAO.get(profileId));
=======
        authorizedUser.addFriend(userDAO.getUser(profileId));
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        return true;
    }

    @Override
    @Transactional
    public void removeFriend(User authorizedUser, Integer profileId) {
<<<<<<< HEAD
        authorizedUser.getFriends().remove(userDAO.get(profileId));
        userDAO.saveOrUpdate(authorizedUser);
=======
        authorizedUser.getFriends().remove(userDAO.getUser(profileId));
        userDAO.saveUser(authorizedUser);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    @Transactional
    public User saveUser(User user) {
<<<<<<< HEAD
        return userDAO.saveOrUpdate(user);
=======
        return userDAO.saveUser(user);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }
}

