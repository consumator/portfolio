package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.WallPost;

import java.util.List;

public interface WallService {
    List<WallPost> getUserWall(User user);

    List<WallPost> getUserWall(int userId);

    int removeWallPost(int postId);

    WallPost saveWallPost(String message, User wallOwner, User Author);

    WallPost getWallPost(Integer postId);
}
