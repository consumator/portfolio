package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.WallPost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class WallServiceImpl implements WallService {
    private static final Logger logger = LogManager.getLogger(WallServiceImpl.class);

    @Autowired
    WallDAO wallDAO;

    @Override
    public List<WallPost> getUserWall(User user) {
        return getUserWall(user.getId());
    }

    @Override
    public List<WallPost> getUserWall(int userId) {
        return wallDAO.getWall(userId);
    }

    @Override
    @Transactional
    public int removeWallPost(int postId) {
<<<<<<< HEAD
        wallDAO.remove(postId);
        return 1;
=======
        return wallDAO.remove(postId);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    public WallPost getWallPost(Integer postId) {
<<<<<<< HEAD
        return wallDAO.get(postId);
=======
        return wallDAO.read(postId);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Override
    @Transactional
    public WallPost saveWallPost(String message, User wallOwner, User author) {
        WallPost wallPost = new WallPost(message, wallOwner, author);
<<<<<<< HEAD
        return wallDAO.saveOrUpdate(wallPost);
=======
        return wallDAO.create(wallPost);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }
}
