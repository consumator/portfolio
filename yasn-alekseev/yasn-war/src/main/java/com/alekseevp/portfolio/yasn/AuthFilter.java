package com.alekseevp.portfolio.yasn;

import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.spring.web.LoggedUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthFilter implements Filter {
    private static final Logger logger = LogManager.getLogger(AuthFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest servletRequest = (HttpServletRequest) request;
        final HttpSession session = servletRequest.getSession();
        final LoggedUser loggedUser = (LoggedUser) session.getAttribute("scopedTarget.loggedUser");
        final User user = loggedUser == null ? null : loggedUser.getAuthorizedUser();
        final HttpServletResponse servletResponse = (HttpServletResponse) response;
        final String contextPath = servletRequest.getContextPath();
        final String pathInfo = servletRequest.getPathInfo();
        final String servletPath = servletRequest.getServletPath();
        final String from = (String) session.getAttribute("from");
        request.setCharacterEncoding("UTF-8");
        String fullPath = servletPath + pathInfo;
        if (fullPath.equals("/")) {
            servletRequest.getSession().setAttribute("from", "/");
            if (user == null) {
                session.invalidate();
                servletResponse.sendRedirect(contextPath + "/login");
            } else {
                servletResponse.sendRedirect(contextPath + "/profile");
            }
            return;
        }
        if (!(fullPath).contains("static/") && (!fullPath.contains("css/"))
                && !"/register".equals(fullPath) && !"/login".equals(fullPath)) {
            if (user == null) {
                servletRequest.getSession().setAttribute("from", contextPath + servletPath);
                session.invalidate();
                servletResponse.sendRedirect(contextPath + "/login");
                return;
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
