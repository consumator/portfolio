﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="static/header.jsp"/>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>
    Profile view
</title>
</head>
<body>
<div id="container">
    <jsp:include page="navigation.jsp"/>
    <div id="content" class="container">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div id="messages-container">
                <div id="messages"></div>
            </div>
        </div>
        <c:if test="${correspondent!=null}">
        <div class="jumbotron message_post_area">
            <div class="col-sm-offset-3 col-sm-6 post_form">
                <c:if test="${isMessageWriteAllowed}">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label for="messagePost">Post:</label>
                                        <textarea class="form-control" cols="80" rows="3" name="post"
                                                  id="messagePost"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a id="postMessageButton" class="btn btn-primary btn-block"
                           onclick="postMessage('${yasnContext}');">
                            Post</a>
                    </div>
                </c:if>
                <c:if test="${!isMessageWriteAllowed}">
                    <div class="text-center">You are not allowed to write to this user</div>
                </c:if>
            </div>
            </c:if>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
<<<<<<< HEAD
        getMessages($yasnEnvironmentVariables.contextPath);
=======
        getMessages("${yasnContext}");
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    });
</script>
</body>

</html>
