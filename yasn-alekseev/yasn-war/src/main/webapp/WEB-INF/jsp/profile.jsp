<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="static/header.jsp"/>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>
    Profile view
</title>
</head>
<body>
<div id="container">
    <jsp:include page="navigation.jsp"/>
    <c:set var="myProfile" scope="request" value="${sessionScope['scopedTarget.loggedUser'].authorizedUser}"/>
    <c:if test="${isForeign}">
        <c:set var="viewProfile" scope="request" value="${foreignProfile}"/>
    </c:if>
    <c:if test="${!isForeign}">
        <c:set var="viewProfile" scope="request" value="${sessionScope['scopedTarget.loggedUser'].authorizedUser}"/>
    </c:if>
    <div id="content" class="container">
        <div class="col-md-4">
            <div class="text-center">
                <a href=
                   <c:if test="${viewProfile.avatar!=null}">
                           "${yasnContext}/${viewProfile.id}/avatar.jpg"
                </c:if>
                        >
                    <img class="avatar" src=
                    <c:if test="${viewProfile.avatar==null}">
                            '${yasnContext}/resources/noavatar.png'
                    </c:if>
                    <c:if test="${viewProfile.avatar!=null}">
                        "${yasnContext}/${viewProfile.id}/avatar.jpg"
                    </c:if>
                    />
                </a>
            </div>
            <P></P>

            <div class="panel panel-default">
                <table class="my-table table table-condensed">
                    <c:if test="${!isForeign}">
                        <tr>
                            <td class="tr_name">E-mail</td>
                            <td><c:out value="${viewProfile.email}"/></td>
                        </tr>
                    </c:if>
                    <tr>
                        <td class="tr_name">Name</td>
                        <td><c:out value="${viewProfile.name}"/></td>
                    </tr>
                    <tr>
                        <td class="tr_name">Patronymic</td>
                        <td><c:out value="${viewProfile.patronymic}"/></td>
                    </tr>
                    <tr>
                        <td class="tr_name">Surname</td>
                        <td><c:out value="${viewProfile.surname}"/></td>
                    </tr>
                    <tr>
                        <td class="tr_name">Sex</td>
                        <c:if test="${viewProfile.sex==true}">
                            <td>М</td>
                        </c:if>
                        <c:if test="${viewProfile.sex==false}">
                            <td>Ж</td>
                        </c:if>
                        <c:if test="${viewProfile.sex==null}">
                            <td></td>
                        </c:if>
                    </tr>
                    <tr>
                        <td class="tr_name">Birth country</td>
                        <td><c:out value="${viewProfile.birthCountry.name}"/></td>
                    </tr>
                    <tr>
                        <td class="tr_name">Birth city</td>
                        <td><c:out value="${viewProfile.birthCity.name}"/></td>
                    </tr>
                    <tr>
                        <td class="tr_name">Country of residence</td>
                        <td><c:out value="${viewProfile.currentCountry.name}"/></td>
                    </tr>
                    <tr>
                        <td class="tr_name">City of residence</td>
                        <td><c:out value="${viewProfile.currentCity.name}"/></td>
                    </tr>
                    <tr>
                        <td class="tr_name">Birth Date</td>
                        <td><c:out value="${viewProfile.birthDate}"/></td>
                    </tr>
                </table>
                <P></P>

                <div class="text-center">
                    <c:if test="${!isForeign}">
                        <A class="btn btn-primary btn-block" href="${yasnContext}/modify">Modify profile</A>
                    </c:if>
                    <c:if test="${isForeign}">
                        <c:if test="${isFriend}">
                            <A class="btn btn-primary btn-block"
                               href="${yasnContext}/messages/${viewProfile.id}" }>View messages</A>
                            <A class="btn btn-primary btn-block"
                               href="${yasnContext}/removeFriend/${viewProfile.id}" }>Remove friend</A>
                        </c:if>
                        <c:if test="${!isFriend}">
                            <A class="btn btn-primary btn-block" href="${yasnContext}/addFriend/${viewProfile.id}"
                               }>Add friend</A>
                        </c:if>
                    </c:if>
                </div>
            </div>
        </div>
        <div class="col-md-8 wall-container">
            <div class="col-sm-9">
                <P class="wall_header">Wall posts</P>
            </div>
            <div id="wall" class="col-sm-12"></div>
        </div>

        <div class="jumbotron wall_post_text">


            <c:if test="${!isForeign||isWallPostAllowed}">
                <div class="row-fluid post_form">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <label for="post">Post:</label>
                                        <textarea class="form-control" cols="80" rows="3" name="post"
                                                  id="post"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a id="postWallButton" class="btn btn-primary btn-block wallPostButtonStyle"
                           onclick="postWallMessage(${viewProfile.id},'${yasnContext}',${!isForeign});">
                            Post</a>
                    </div>
                </div>
            </c:if>
            <c:if test="${isForeign&&!isWallPostAllowed}">
                <div class="text-center">You are not allowed to post here</div>
            </c:if>
        </div>
    </div>
</div>
</body>

<script>
    $(document).ready(function () {
        drawWall(${viewProfile.id}, ${viewProfile.equals(myProfile)})
    });
</script>
</html>
