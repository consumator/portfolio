<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="static/header.jsp"/>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>
    Profile modification
</title>
</head>
<body>
<div id="container">
    <jsp:include page="navigation.jsp"/>
    <c:set var="viewProfile" scope="request" value="${sessionScope['scopedTarget.loggedUser'].authorizedUser}"/>
    <div id="content" class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="text-center">
                    <a href=
                       <c:if test="${viewProfile.avatar!=null}">
                               "${yasnContext}/${viewProfile.id}/avatar.jpg"
                    </c:if>
                            >
                        <img class="avatar_large img-rounded" src=
                        <c:if test="${viewProfile.avatar==null}">
                                '${yasnContext}/resources/noavatar.png'
                        </c:if>
                        <c:if test="${viewProfile.avatar!=null}">
                            "${yasnContext}/${viewProfile.id}/avatar.jpg"
                        </c:if>
                        />
                    </a>
                </div>


                <form class="text-center" action="${yasnContext}/avatarUpload"
                      enctype="multipart/form-data" method="post">
                    <div class="btn-group-vertical text-center">
                    <span class="btn btn-primary btn-file">
                        Browse new avatar<input type="file" name="fileUpload">
                     </span>
                        <button type="submit" class="btn btn-primary btn-file">Upload</button>
                    </div>
                </form>
            </div>


            <form id="profileModificationInputsForm" class="form-horizontal  col-sm-4" ACTION="${yasnContext}/modify" method="POST"
                  accept-charset="UTF-8">


                <div class="form-group form-group-sm">
                    <label class="col-sm-6 control-label" for="email">E-mail</label>

                    <div class="col-sm-6 ">
                        <input class="form-control" disabled type="email" name="email" value="${viewProfile.email}"
                               placeholder="${sessionScope['scopedTarget.loggedUser'].authorizedUser.email}" id="email">
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="n" class="col-sm-6 control-label<c:if
                            test="${requiredFieldsUnfilled}"> bad_input</c:if>">Name</label>

                    <div class="col-sm-6 ">
                        <input class="form-control" type="fname" name="name" value="${viewProfile.name}"
                               placeholder="${user.name}"
                               id="n"/>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label class="col-sm-6 control-label" for="pt">Patronymic</label>

                    <div class="col-sm-6 ">
                        <input class="form-control" type="fname" name="patronymic" value="${viewProfile.patronymic}"
                               placeholder="${user.patronymic}"
                               id="pt"/>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label for="ln" class="col-sm-6 control-label
                           <c:if
                                   test="${requiredFieldsUnfilled}"> bad_input</c:if>"
                            >Surname</label>

                    <div class="col-sm-6 ">
                        <input class="form-control" type="lname" name="surname" value="${viewProfile.surname}"
                               placeholder="${user.surname}" id="ln"/>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label class="col-sm-6 control-label" for="sex">Sex</label>

                    <div class="col-sm-6 ">
                        <select name="sex" id="sex" class="form-control">
                            <option value=2<c:if test="${viewProfile.sex==true}">
                                selected="selected" </c:if>>М
                            </option>
                            <option value=1 <c:if test="${viewProfile.sex==false}">
                                selected="selected" </c:if>>Ж
                            </option>
                            <option value=0 <c:if test="${viewProfile.sex==null}">
                                selected="selected" </c:if>></option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label class="col-sm-6 control-label" for="bCountry">Birth country</label>

                    <div class="col-sm-6 ">
                        <select name="birthCountry" id="bCountry" class="form-control"
                                onchange="onCountryChange('#bCountry')">
                            <c:forEach var="country" items="${requestInfo.countryList}">
                                <option value="${country.id}" <c:if
                                        test="${country.name==viewProfile.birthCountry.name}">
                                    selected="selected" </c:if>>${country.name}
                                </option>
                            </c:forEach>
                            <option value=0<c:if test="${viewProfile.birthCountry==null}">
                                selected="selected"</c:if>></option>
                            <option value="-1">New country</option>

                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label class="col-sm-6 control-label" for="bCity">Birth city</label>

                    <div class="col-sm-6 ">
                        <select name="birthCity" id="bCity" class="form-control
                        <c:if test="${viewProfile.birthCountry==null}">
                                    hidden
                                </c:if>" onchange="onCityChange('#bCity')">
                            <c:forEach var="city" items="${requestInfo.birthCountryCityList}">
                                <option value="${city.id}" <c:if test="${city.name==viewProfile.birthCity.name}">
                                    selected="selected" </c:if>>${city.name}
                                </option>
                            </c:forEach>
                            <option value=0<c:if test="${viewProfile.birthCity==null}">
                                selected="selected"</c:if>></option>
                            <option value="-1">New city</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label class="col-sm-6 control-label" for="cCountry">Country of residence</label>

                    <div class="col-sm-6 ">
                        <select name="currentCountry" id="cCountry" class="form-control"
                                onchange="onCountryChange('#cCountry')">
                            <c:forEach var="country" items="${requestInfo.countryList}">
                                <option value="${country.id}" <c:if
                                        test="${country.name==viewProfile.currentCountry.name}">
                                    selected="selected" </c:if>>${country.name}
                                </option>
                            </c:forEach>
                            <option value=0<c:if test="${viewProfile.currentCountry==null}">
                                selected="selected"</c:if>></option>
                            <option value="-1">New country</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label class="col-sm-6 control-label" for="cCity">City of residence</label>

                    <div class="col-sm-6 ">
                        <select name="currentCity" class="form-control
                        <c:if test="${viewProfile.currentCountry==null}">
                                    hidden
                                </c:if>" id="cCity" onchange="onCityChange('#cCity')">
                            <c:forEach var="city" items="${requestInfo.currentCountryCityList}">
                                <option value="${city.id}" <c:if test="${city.name==viewProfile.currentCity.name}">
                                    selected="selected" </c:if>>${city.name}
                                </option>
                            </c:forEach>
                            <option value=0<c:if test="${viewProfile.currentCity==null}">
                                selected="selected"</c:if>></option>
                            <option value="-1">New city</option>

                        </select>
                    </div>
                </div>
                <div class="form-group form-group">
                    <label class="col-sm-6 control-label" for="bd">Birth date</label>

                    <div class="col-sm-6 ">
                        <input class="form-control" type="date" name="birthDate" value="${viewProfile.birthDate}"
                               id="bd"/>
                    </div>
                </div>
                <div class="text-center">
                    <FORM METHOD="post" ACTION="${yasnContext}/modify">
                        <INPUT align="center" TYPE="submit" class="btn btn-primary" VALUE="Save changes">
                    </FORM>
                </div>
            </form>
            <c:if test="${requiredFieldsUnfilled}"><P class="bad_input">Name and Surname fields are
                required</P></c:if>
            <div class="col-sm-4">
                <div>
                    <form id="new-loc-form">

                        <div class="col-sm-8 form-group"><input class="form-control hidden"
                                                                id="new-country"
                                                                type="text" name="new-country"
                                                                placeholder="Enter new country name"
                                />
                        </div>
                        <div class="col-sm-8 form-group">
                            <select name="new-city-country" class="form-control hidden"
                                    id="new-city-country" onchange="onCityCountryListChange();">
                                <c:forEach var="country" items="${requestInfo.countryList}">
                                    <option value="${country.id}">
                                            ${country.name}
                                    </option>
                                </c:forEach>
                                <option value=0 selected="selected"></option>
                            </select>
                        </div>
                        <div class="col-sm-8 form-group"><input class="form-control hidden" id="new-city"
                                                                type="text" name="new-city"
                                                                placeholder="Enter new city name"
                                />
                        </div>
                        <div class="col-sm-8 text-center">
                            <input type="button" id="save-new-loc" onclick="onNewLocationButtonClick();"
                                   class="btn btn-primary btn-file hidden" value="Save">
                        </div>

                    </form>
                </div>
            </div>
        </div>


    </div>
</div>


<script>

</script>
</body>
</html>
