<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="static/header.jsp"/>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
Profile view
</title>
</head>
<body>
<jsp:include page="navigation.jsp"/>

<div id="container" class="vertical-center">
    <div class="container">
        <div class="col-sm-offset-4 col-sm-4">
            <FORM METHOD="post" ACTION="${yasnContext}/settings" id="settings_form" class="form-horizontal">
                <div>

                    <c:forEach items="${user_settings}" var="setting">
                        <div class="form-group form-group-sm">
                            <label for="${setting.settingType.name()}">${setting.settingTypeViewName}</label>
                            <select name="${setting.settingType.name()}" id="${setting.settingType.name()}"
                                    class="form-control">
                                <c:forEach items="${setting.possibleValues}" var="value">
                                    <option value="${value.name()}"
                                            <c:if test="${setting.value.name()==value.name()}">selected="selected"</c:if>>${value.toString()}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </c:forEach>
                    <div class="text-center">
                        <FORM METHOD="post" ACTION="${yasnContext}/settings">
                            <INPUT align="center" TYPE="submit" class="btn btn-primary" VALUE="Save changes">
                        </FORM>
                    </div>

                </div>
            </FORM>
        </div>
    </div>
</div>
</body>
</html>