<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>
        Profile view
    </title>
    <jsp:include page="header.jsp"/>
</head>
<body>
<div id="container">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">YeehAwSocialNetwork</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li><a href="${yasnContext}/profile"><span
                            class="glyphicon glyphicon-user"></span> Profile</a></li>
                    <li><a href="${yasnContext}/friends"> Friends</a></li>
                    <li><a href="${yasnContext}/settings"><span
                            class="glyphicon glyphicon-cog"></span> Settings</a></li>
                    <li><a href="${yasnContext}/search"><span
                            class="glyphicon glyphicon-search"></span> Search</a></li>
                    <li><a href="${yasnContext}/messages"><span
                            class="glyphicon glyphicon-envelope"></span> Messages</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <form METHOD="get" ACTION="${yasnContext}/search"
                          class="navbar-form navbar-left">
                        <div class="input-group">
                            <input class="form-control" name="searchParams" type="text">
    <span class="input-group-btn">
      <button class="btn btn-default" type="button">
          <span class="glyphicon glyphicon-search"></span>
      </button>
    </span>
                        </div>
                    </form>
                    <li><a href="${yasnContext}/logout"><span
                            class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="content">
        <div class="message-404">
            <P class="text-center">OOPS NO SUCH PAGE</P>

            <div class="text-center">
                <img src="${yasnContext}/resources/images/404.png"/>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <p><strong>(c) Pavel Alekseev</strong></p>
            </div>
        </footer>
    </div>
</div>
</body>
</html>
