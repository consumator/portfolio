<%@ page contentType="text/html;charset=UTF-8" language="java" %>

    <jsp:include page="header.jsp"/>
    <title>
        Login
    </title>
</head>
<body>

<div id="container" class="vertical-center">

    <div class="container">
        <form class="form-horizontal" ACTION="${yasnContext}/login" method="POST">
            <div class="form-group">
                <label for="email" class="col-sm-offset-2 col-sm-2 control-label">Email</label>

                <div class="col-sm-4">
                    <input type="email" class="form-control" id="email" placeholder="Email" name="email">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-offset-2 col-sm-2 control-label">Password</label>

                <div class="col-sm-4">
                    <input type="password" class="form-control" id="password" placeholder="Password"
                           name="password">
                </div>
            </div>
            <div class="col-sm-offset-4 col-sm-4">
                <div>
                    <FORM METHOD="POST" ACTION="${yasnContext}/login">
                        <INPUT TYPE="submit" style="text-align: center" class="btn btn-primary btn-block"
                               VALUE="Log in">
                    </FORM>
                    <A href="${yasnContext}/register"
                       class="btn btn-primary btn-block">Registration</A>
                </div>
            </div>
        </form>
    </div>
    <jsp:include page="../footer.jsp"/>
</div>
</body>
</html>
