<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"/>
<title>
    Registration
</title>
</head>
<body>
<div id="container" class="vertical-center">
    <div class="container">
        <form class="form-horizontal" ACTION="${yasnContext}/register" method="POST">
            <div class="form-group">
                <label for="email" class="col-sm-offset-2 col-sm-2 control-label">Email</label>

                <div class="col-sm-4">
                    <input type="email" class="form-control" id="email" placeholder="Email" name="email">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-offset-2 col-sm-2 control-label">Password</label>

                <div class="col-sm-4">
                    <input type="password" class="form-control" id="password" placeholder="Password"
                           name="password">
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-offset-2 col-sm-2 control-label">Name</label>

                <div class="col-sm-4">
                    <input type="fname" class="form-control" id="name" placeholder="John"
                           name="name">
                </div>
            </div>
            <div class="form-group">
                <label for="surname" class="col-sm-offset-2 col-sm-2 control-label">Surname</label>

                <div class="col-sm-4">
                    <input type="lname" class="form-control" id="surname" placeholder="Smith"
                           name="surname">
                </div>
            </div>
            <div class="col-sm-offset-4 col-sm-4">
                <div>
                    <FORM METHOD="POST" ACTION="${yasnContext}/register">
                        <INPUT TYPE="submit" class="btn btn-primary btn-block text-center"

                               VALUE="Register">
                    </FORM>
                    <A href="${yasnContext}/login"
                       class="btn btn-primary btn-block">Login</A>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
</html>
