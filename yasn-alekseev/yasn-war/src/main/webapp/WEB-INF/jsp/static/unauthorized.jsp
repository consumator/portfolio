<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"/>
<title>
    Profile view
</title>
</head>
<body>
<div id="container">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">YeehAwSocialNetwork</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li><a href="${yasnContext}/profile"><span
                            class="glyphicon glyphicon-user"></span> Profile</a></li>
                    <li><a href="${yasnContext}/friends"> Friends</a></li>
                    <li><a href="${yasnContext}/settings"><span
                            class="glyphicon glyphicon-cog"></span> Settings</a></li>
                    <li><a href="${yasnContext}/search"><span
                            class="glyphicon glyphicon-search"></span> Search</a></li>
                    <li><a href="${yasnContext}/messages"><span
                            class="glyphicon glyphicon-envelope"></span> Messages</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <form METHOD="get" ACTION="${yasnContext}/search"
                          class="navbar-form navbar-left">
                        <div class="input-group">
                            <input class="form-control" name="searchParams" type="text">
    <span class="input-group-btn">
      <button class="btn btn-default" type="button">
          <span class="glyphicon glyphicon-search"></span>
      </button>
    </span>
                        </div>
                    </form>
                    <li><a href="${yasnContext}/logout"><span
                            class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="content">
        <P align="center">You cannot view this page</P>
        <footer class="footer">
            <div class="container">
                <p><strong>(c) Pavel Alekseev</strong></p>
            </div>
        </footer>
    </div>
</div>
</body>
</html>
