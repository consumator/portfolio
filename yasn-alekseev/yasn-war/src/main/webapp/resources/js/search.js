$(document).ready(function () {
    $('.pagination').jqPagination({
        link_string: '/?page={page_number}',
        max_page: 1,
        paged: function (page) {
            console.log($yasnEnvironmentVariables.isChangedExternally);
            if (!$yasnEnvironmentVariables.isChangedExternally) {
                if (!$yasnEnvironmentVariables.isCurrentQuickSearch) {
                    postLastSearchForm(page);
                } else {
                    console
                    quickSearch(page);
                }
            }
            $yasnEnvironmentVariables.isChangedExternally = false;
        }
    });
});