package com.alekseevp.portfolio.yasn.spring.web;

import com.alekseevp.portfolio.yasn.FileService;
import com.alekseevp.portfolio.yasn.UserService;
import com.alekseevp.portfolio.yasn.entities.Picture;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class FileController {
    private static final Logger logger = LogManager.getLogger(FileController.class);
    private final LoggedUser loggedUser;
    private final UserService userService;
    private final FileService fileService;

    @Autowired
    FileController(UserService userService, LoggedUser loggedUser, FileService fileService) {
        this.userService = userService;
        this.loggedUser = loggedUser;
        this.fileService = fileService;
    }

    @RequestMapping(value = "/avatarUpload", method = RequestMethod.POST)
    public String uploadFile(@RequestParam MultipartFile[] fileUpload) {
        if (fileUpload != null && fileUpload.length > 0) {
            for (MultipartFile aFile : fileUpload) {
                Picture avatar = fileService.uploadFile(aFile);
                loggedUser.getAuthorizedUser().setAvatar(avatar);
                userService.saveUser(loggedUser.getAuthorizedUser());
            }
        }
        return "redirect:/modify";
    }

    @RequestMapping(value = "/{profileId}/avatar.jpg", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getAvatar(@PathVariable Integer profileId, HttpServletResponse resp) throws IOException {
        resp.setContentType("image/jpeg;image/png");
        Picture avatar = userService.getProfile(profileId).getAvatar();
        if (avatar != null) {
            return avatar.getImage();
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/avatar/{pictureId}", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getAvatar(HttpServletResponse resp, @PathVariable Integer pictureId) throws IOException {
        resp.setContentType("image/jpeg;image/png");
        Picture avatar = fileService.getPicture(pictureId);
        return avatar == null ? null : avatar.getImage();
    }

    @RequestMapping(value = "/profile/{profileId}/avatar", method = RequestMethod.GET)
    @ResponseBody
    public byte[] getUserAvatar(HttpServletResponse resp, @PathVariable Integer profileId) throws IOException {
        resp.setContentType("image/jpeg;image/png");
        Picture avatar = fileService.getUserAvatar(profileId);
        return avatar == null ? null : avatar.getImage();
    }
}
