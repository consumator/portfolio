package com.alekseevp.portfolio.yasn.spring.web;

import com.alekseevp.portfolio.yasn.HealthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HealthController {
    private final HealthService healthService;

    @Autowired
    public HealthController(HealthService healthService) {
        this.healthService = healthService;
    }


    @RequestMapping(path = "/health", method = RequestMethod.GET)
    public ModelAndView getHealth(ModelAndView modelAndView) {
        modelAndView.addObject("userCount", healthService.getUserCount());
        modelAndView.setViewName("health");
        return modelAndView;
    }
}

