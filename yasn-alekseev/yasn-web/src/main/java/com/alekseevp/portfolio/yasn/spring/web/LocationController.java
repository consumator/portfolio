package com.alekseevp.portfolio.yasn.spring.web;

import com.alekseevp.portfolio.yasn.LocationService;
import com.alekseevp.portfolio.yasn.entities.City;
import com.alekseevp.portfolio.yasn.entities.Country;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class LocationController {
    private static final Logger logger = LogManager.getLogger(LocationController.class);
    private final LocationService locationService;

    @Autowired
    LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @RequestMapping(path = "/countries", headers = "Accept=*/*", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    Collection<Country> getCountries() {
        return locationService.getCountries();
    }

    @RequestMapping(path = "/cities/{countryId}", headers = "Accept=*/*", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    List<City> getCities(@PathVariable Integer countryId) {
        return locationService.getAllCitiesInCountry(countryId);
    }

    @RequestMapping(path = "/new-loc/city/", headers = "Accept=*/*", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public boolean addCity(@RequestParam Map<String, String> locationForm) {
        return locationService.addNewCity(Integer.valueOf(locationForm.get("new-city-country")), locationForm.get("new-city"));
    }

    @RequestMapping(path = "/new-loc/country/", headers = "Accept=*/*", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public boolean addCountry(@RequestParam Map<String, String> locationForm) {
        return locationService.saveCountry(locationForm.get("new-country"));
    }

}
