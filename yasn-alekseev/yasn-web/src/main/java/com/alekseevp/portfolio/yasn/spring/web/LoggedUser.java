package com.alekseevp.portfolio.yasn.spring.web;


import com.alekseevp.portfolio.yasn.entities.User;

import java.io.Serializable;

public class LoggedUser implements Serializable {
    private User authorizedUser;

    public User getAuthorizedUser() {
        return authorizedUser;
    }

    public void setAuthorizedUser(User authorizedUser) {
        this.authorizedUser = authorizedUser;
    }
}


