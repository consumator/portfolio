package com.alekseevp.portfolio.yasn.spring.web;

import com.alekseevp.portfolio.yasn.MessageService;
import com.alekseevp.portfolio.yasn.UserService;
import com.alekseevp.portfolio.yasn.SettingsService;
import com.alekseevp.portfolio.yasn.entities.Message;
import com.alekseevp.portfolio.yasn.entities.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

import static com.alekseevp.portfolio.yasn.Utility.convertCalendarToString;
import static com.alekseevp.portfolio.yasn.Utility.isBlank;

@Controller
public class MessageController {
    private static final Logger logger = LogManager.getLogger(MessageController.class);
    private final MessageService messageService;
    private final SettingsService settingsService;
    private final UserService userService;

    @Autowired
    private LoggedUser loggedUser;

    @Autowired
    public MessageController(MessageService messageService, SettingsService settingsService, UserService userService) {
        this.messageService = messageService;
        this.settingsService = settingsService;
        this.userService = userService;
    }


    @RequestMapping(path = "/messages", method = RequestMethod.GET)
    public ModelAndView getMessages() {
        ModelAndView modelAndView = new ModelAndView("messages");
        List<MessageView> messageViewArrayList = convertMessagesForView(
                messageService.getMessages(loggedUser.getAuthorizedUser()), loggedUser.getAuthorizedUser());
        modelAndView.addObject("messages", messageViewArrayList);
        return modelAndView;
    }

    @RequestMapping(path = "/messages/post/{correspondentId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public MessageView postMessage(@PathVariable Integer correspondentId, @RequestBody String message) {
        User receiver = userService.getProfile(correspondentId);
        if (!isBlank(message)) {
            return new MessageView(
                    messageService.postMessage(new Message(loggedUser.getAuthorizedUser(), receiver, message)),
                    loggedUser.getAuthorizedUser());
        }
        return null;
    }


    @RequestMapping(path = "/messages/{profileId}", method = RequestMethod.GET)
    public ModelAndView getMessagesOnLoad(@PathVariable Integer profileId) {
        User correspondent = userService.getProfile(profileId);
        if (correspondent == null) {
            return new ModelAndView("redirect:/messages");
        }
        ModelAndView modelAndView = new ModelAndView("/messages");
        List<MessageView> messageViews =
                convertMessagesForView(messageService.getMessages(
                        loggedUser.getAuthorizedUser(), profileId), loggedUser.getAuthorizedUser());
        modelAndView.addObject("correspondent", correspondent);
        modelAndView.addObject("messages", messageViews);
        modelAndView.addObject("isMessageWriteAllowed",
                settingsService.isAllowedToWriteMessages(loggedUser.getAuthorizedUser(), correspondent));
        return modelAndView;
    }

    @RequestMapping(path = "/messages/{profileId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<MessageView> getMessages(@PathVariable Integer profileId) {
        User correspondent = userService.getProfile(profileId);
        if (correspondent == null) {
            return new ArrayList<>();
        }
        return convertMessagesForView(messageService.getMessages(
                loggedUser.getAuthorizedUser(), profileId), loggedUser.getAuthorizedUser());
    }

    @RequestMapping(path = "/messages/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<MessageView> getAllMessages() {
        return convertMessagesForView(messageService.getMessages(
                loggedUser.getAuthorizedUser()), loggedUser.getAuthorizedUser());
    }


    public static class MessageView {
        private final int id;
        protected final boolean inbox;
        private final String correspondentEmail;
        private final Integer correspondentId;
        private final String text;
        private final int avatarId;
        private String timeStamp;

        MessageView(Message message, User currentUser) {
            this.id = message.getId();
            this.inbox = !message.getAuthor().getEmail().equals(currentUser.getEmail());
            User correspondent = inbox ? message.getAuthor() : message.getReceiver();
            this.correspondentEmail = correspondent.getEmail();
            this.correspondentId = correspondent.getId();
            this.text = message.getText();
            this.timeStamp = convertCalendarToString(message.getTimestamp());
            this.avatarId = inbox ? message.getAuthor().getAvatar() != null ? message.getAuthor().getAvatar().getId() : 0 :
                    message.getReceiver().getAvatar() != null ? message.getReceiver().getAvatar().getId() : 0;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof MessageView)) return false;

            MessageView that = (MessageView) o;

            if (getId() != that.getId()) return false;
            if (isInbox() != that.isInbox()) return false;
            if (getAvatarId() != that.getAvatarId()) return false;
            if (!getCorrespondentEmail().equals(that.getCorrespondentEmail())) return false;
            if (!getCorrespondentId().equals(that.getCorrespondentId())) return false;
            if (getText() != null ? !getText().equals(that.getText()) : that.getText() != null) return false;
            return getTimeStamp().equals(that.getTimeStamp());

        }

        @Override
        public int hashCode() {
            int result = getId();
            result = 31 * result + (isInbox() ? 1 : 0);
            result = 31 * result + getCorrespondentEmail().hashCode();
            result = 31 * result + getCorrespondentId().hashCode();
            result = 31 * result + (getText() != null ? getText().hashCode() : 0);
            result = 31 * result + getAvatarId();
            result = 31 * result + getTimeStamp().hashCode();
            return result;
        }


        public Integer getCorrespondentId() {
            return correspondentId;
        }


        public int getId() {
            return id;
        }

        public int getAvatarId() {
            return avatarId;
        }

        public String getCorrespondentEmail() {

            return correspondentEmail;
        }

        public boolean isInbox() {
            return inbox;
        }

        public String getText() {
            return text;
        }

        public String getTimeStamp() {
            return timeStamp;
        }
    }

    public static List<MessageView> convertMessagesForView(List<Message> messages, User currentUser) {
        List<MessageView> messageViews = new ArrayList<>();
        for (Message message : messages) {
            messageViews.add(new MessageView(message, currentUser));
        }
        return messageViews;
    }

}
