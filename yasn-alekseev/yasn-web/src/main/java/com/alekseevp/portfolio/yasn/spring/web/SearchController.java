package com.alekseevp.portfolio.yasn.spring.web;

<<<<<<< HEAD
import com.alekseevp.portfolio.yasn.LocationService;
import com.alekseevp.portfolio.yasn.SettingsService;
import com.alekseevp.portfolio.yasn.UserService;
import com.alekseevp.portfolio.yasn.Utility;
=======
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.alekseevp.portfolio.yasn.LocationService;
import com.alekseevp.portfolio.yasn.UserService;
import com.alekseevp.portfolio.yasn.SettingsService;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import com.alekseevp.portfolio.yasn.entities.City;
import com.alekseevp.portfolio.yasn.entities.RequestInfo;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.User.UserView;
<<<<<<< HEAD
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
<<<<<<< HEAD
import org.springframework.web.bind.annotation.*;
=======
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.alekseevp.portfolio.yasn.Utility.isBlank;


@Controller
public class SearchController {
    private static final Logger logger = LogManager.getLogger(SearchController.class);
    private final UserService userService;
    private final LocationService locationService;
    private final SettingsService settingsService;

    @Autowired
    private LoggedUser loggedUser;

    @Autowired
    SearchController(UserService userService, LocationService locationService, SettingsService settingsService) {
        this.userService = userService;
        this.locationService = locationService;
        this.settingsService = settingsService;
    }

<<<<<<< HEAD
    private static User.UserSearchQueryObject prepareSearchQueryObject(Map<String, String> searchParams, int pageNumber) {
        User.UserSearchQueryObject query = new User.UserSearchQueryObject();
        String bdSign = searchParams.get("bdSign");
        query.setBirthDateGreaterThan(!isBlank(bdSign) && bdSign.trim().equals("1"));
        String email = searchParams.get("email");
        query.setEmail(!isBlank(bdSign) ? email.trim().toLowerCase() : null);
        String name = searchParams.get("name");
        query.setName(!isBlank(name) ? name.trim().toLowerCase() : null);
        String surname = searchParams.get("surname");
        query.setSurname(!isBlank(surname) ? surname.trim().toLowerCase() : null);
        String patronymic = searchParams.get("patronymic");
        query.setPatronymic(!isBlank(patronymic) ? patronymic.trim().toLowerCase() : null);
        String sexString = searchParams.get("sex");
        String searchSex = !isBlank(sexString) ? sexString.trim() : "0";
        String birthDate = searchParams.get("birthDate");
        String birthDateString = !(isBlank(birthDate)) ? birthDate.trim() : "";
        query.setBirthDate(Utility.parseDateFromString(birthDateString));
        Boolean sex;
        switch (Integer.valueOf(searchSex)) {
            case 2:
                sex = true;
                break;
            case 1:
                sex = false;
                break;
            default:
                sex = null;
        }
        query.setSex(sex);
        String birthCountry = searchParams.get("birthCountry");
        query.setBirthCountry(birthCountry == null ? 0 : Integer.valueOf(birthCountry));
        String birthCity = searchParams.get("birthCity");
        query.setBirthCity(birthCity == null ? 0 : Integer.valueOf(birthCity));
        String currentCountry = searchParams.get("currentCountry");
        query.setCurrentCountry(currentCountry == null ? 0 : Integer.valueOf(currentCountry));
        String currentCity = searchParams.get("birthCity");
        query.setCurrentCity(currentCity == null ? 0 : Integer.valueOf(currentCity));
        query.setPageNumber(pageNumber);
        return query;


    }

    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public ModelAndView getSearchPage() {
        ModelAndView modelAndView = new ModelAndView("search");
        modelAndView.addObject("requestInfo", new RequestInfo(locationService.getCountries(), new ArrayList<City>(), new ArrayList<City>()));
        return modelAndView;
    }

    @RequestMapping(path = "/search/{pageNumber}", method = RequestMethod.POST, produces = "text/plain; charset=utf-8",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public
    @ResponseBody
    String search(@RequestParam Map<String, String> searchParams, @PathVariable Integer pageNumber) {
        User.UserSearchQueryObject queryObject = prepareSearchQueryObject(searchParams, pageNumber);
        return getSearchResultsJSON(userService.search(queryObject), queryObject);
    }


    @RequestMapping(path = "/quicksearch/{pageNumber}", method = RequestMethod.GET, produces = "text/plain; charset=utf-8")
    public
    @ResponseBody
    String quicksearch(@RequestParam String searchString, @PathVariable Integer pageNumber) {
        User.UserSearchQueryObject queryObject = new User.UserSearchQueryObject();
        queryObject.setQuickSearch(true);
        queryObject.setQuickSearchString(searchString);
        queryObject.setPageNumber(pageNumber);
        return getSearchResultsJSON(userService.search(queryObject), queryObject);
    }

    @RequestMapping(path = "/quicksearch", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView quicksearchInitPage(@RequestParam String searchString) {
        ModelAndView modelAndView = new ModelAndView("search");
        modelAndView.addObject("requestInfo", new RequestInfo(locationService.getCountries(), new ArrayList<City>(), new ArrayList<City>()));
        if (!isBlank(searchString)) {
            modelAndView.addObject("quickSearchString", searchString);
            modelAndView.addObject("isCurrentQuickSearch", true);
        }
        return modelAndView;
    }

    private String getSearchResultsJSON(List<User> searchResults, User.UserSearchQueryObject queryObject) {
        List<UserView> searchResultsView = new ArrayList<>();
        filterForbiddenUserPages(searchResults);
        for (User searchResult : searchResults) {
            searchResultsView.add(new UserView(searchResult));
        }
        String JSONString;
        ObjectMapper mapper = new ObjectMapper();
        JSONString = "{\"searchResults\":";
        try {
            JSONString += mapper.writeValueAsString(searchResultsView);
            JSONString += ",\"pageCount\":{\"count\":" + queryObject.getResultsPageCount() + ",\"current\":" + queryObject.getPageNumber() + "}}";
        } catch (JsonProcessingException e) {
            logger.error("Error converting to JSON", e);
        }
        return JSONString;
    }


    private void filterForbiddenUserPages(List<User> searchResults) {
        Iterator<User> searchIterator = searchResults.iterator();
        while (searchIterator.hasNext()) {
            if (!settingsService.isAllowedToViewPage(loggedUser.getAuthorizedUser(), searchIterator.next())) {
                searchIterator.remove();
            }
        }
=======

    @RequestMapping(path = "/search", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public
    @ResponseBody
    List<UserView> search(@RequestParam Map<String, String> searchParams) {
        try {
            List<User> searchResults = userService.search(searchParams);
            filterForbiddenUserPages(searchResults);
            List<UserView> searchResultsView=new ArrayList<>();
            for (User searchResult : searchResults) {
                searchResultsView.add(new UserView(searchResult));
            }
            return searchResultsView;
        } catch (Exception e) {
            logger.error("Error while searching", e);
        }
        return new ArrayList<>();
    }

    private void filterForbiddenUserPages(List<User> searchResults) {
        Iterator<User> searchIterator = searchResults.iterator();
        while (searchIterator.hasNext()) {
            if (!settingsService.isAllowedToViewPage(loggedUser.getAuthorizedUser(), searchIterator.next())) {
                searchIterator.remove();
            }
        }
    }

    @RequestMapping(path = "/quicksearch", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView quicksearch(String searchParams) {
        RequestInfo requestInfo = new RequestInfo(locationService.getCountries(), new ArrayList<City>(), new ArrayList<City>());
        ModelAndView modelAndView = new ModelAndView("search");
        modelAndView.addObject("requestInfo", requestInfo);
        if (isBlank(searchParams)) {
            return modelAndView;
        }
        List<User> searchResults = userService.quickSearch(searchParams);
        filterForbiddenUserPages(searchResults);
        List<UserView> searchResultsView=new ArrayList<>();
        for (User user:searchResults){
            searchResultsView.add(new UserView(user));
        }
        ObjectMapper mapper=new ObjectMapper();
        String JSONresults="";
        try {
            JSONresults=mapper.writeValueAsString(searchResultsView);
        } catch (JsonProcessingException e) {
            logger.error("Error converting to JSON",e);
        }
        modelAndView.addObject("searchResults", JSONresults);
        return modelAndView;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }


}
