package com.alekseevp.portfolio.yasn.spring.web;

import com.alekseevp.portfolio.yasn.SettingsService;
import com.alekseevp.portfolio.yasn.entities.Setting;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingsValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
public class SettingsController {
    private static final Logger logger = LogManager.getLogger(SettingsController.class);
    private final SettingsService settingsService;
    @Autowired
    private LoggedUser loggedUser;

    @Autowired
    public SettingsController(SettingsService settingsService) {
        this.settingsService = settingsService;
    }


    @RequestMapping(path = "/settings", method = RequestMethod.GET)
    public ModelAndView getSettings() {
        ModelAndView modelAndView = new ModelAndView("/settings");
        List<Setting> userSettings = settingsService.getSettings(loggedUser.getAuthorizedUser());
        modelAndView.addObject("user_settings", userSettings);
        return modelAndView;
    }

    @RequestMapping(path = "/settings", method = RequestMethod.POST)
    public ModelAndView saveSettings(@RequestParam Map<String, String> allRequestParam) {
        for (Setting setting : settingsService.getSettings(loggedUser.getAuthorizedUser())) {
            String settingValue = allRequestParam.get(setting.getSettingType().name());
            if (settingValue != null) {
                setting.setValue(PrivacySettingsValue.valueOf(settingValue));
                settingsService.saveSetting(setting);
            }
        }
        return new ModelAndView("redirect:/profile");
    }
}
