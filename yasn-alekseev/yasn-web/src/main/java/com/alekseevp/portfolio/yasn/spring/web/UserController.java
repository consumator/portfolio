package com.alekseevp.portfolio.yasn.spring.web;

import com.alekseevp.portfolio.yasn.LocationService;
import com.alekseevp.portfolio.yasn.UserService;
import com.alekseevp.portfolio.yasn.SettingsService;
import com.alekseevp.portfolio.yasn.entities.Country;
import com.alekseevp.portfolio.yasn.entities.RequestInfo;
import com.alekseevp.portfolio.yasn.entities.User;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
<<<<<<< HEAD

=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.alekseevp.portfolio.yasn.Utility.isBlank;


@Controller
public class UserController {
    private static final Logger logger = LogManager.getLogger(UserController.class);


    private final UserService userService;
    private final LocationService locationService;
    private final SettingsService settingsService;

    @Autowired
    private LoggedUser loggedUser;

    @Autowired
    UserController(UserService userService, LocationService locationService, SettingsService settingsService) {
        this.userService = userService;
        this.locationService = locationService;
        this.settingsService = settingsService;
    }

    private User modifyLoggedUser(Map<String, String> allRequestParams) {
        String birthCountryInput = allRequestParams.get("birthCountry");
        Integer birthCountryId = birthCountryInput == null ? 0 : Integer.valueOf(birthCountryInput);
        String birthCityInput = allRequestParams.get("birthCity");
        Integer birthCityId = birthCityInput == null ? 0 : Integer.valueOf(birthCityInput);
        String currentCountryInput = allRequestParams.get("currentCountry");
        Integer currentCountryId = currentCountryInput == null ? 0 : Integer.valueOf(currentCountryInput);
        String currentCityInput = allRequestParams.get("currentCity");
        Integer currentCityId = currentCityInput == null ? 0 : Integer.valueOf(currentCityInput);
<<<<<<< HEAD
        String name = allRequestParams.get("name");
        String surname = allRequestParams.get("surname");
=======
        String name=allRequestParams.get("name");
        String surname=allRequestParams.get("surname");
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        String patronymic = allRequestParams.get("patronymic");
        String birthDate = allRequestParams.get("birthDate");
        Boolean sex;
        switch (Integer.valueOf(allRequestParams.get("sex"))) {
            case 2:
                sex = true;
                break;
            case 1:
                sex = false;
                break;
            default:
                sex = null;
                break;
        }
        User modifiedUser = loggedUser.getAuthorizedUser();
        if (!isBlank(name)) {
            modifiedUser.setName(name);
        }
        if (!isBlank(surname)) {
            modifiedUser.setSurname(surname);
        }
        modifiedUser.setPatronymic(isBlank(patronymic) ? null : patronymic);
        modifiedUser.setSex(sex);
        modifiedUser.setBirthCountry(birthCountryId.equals(0) ? null : locationService.getCountry(birthCountryId));
        modifiedUser.setBirthCity(birthCountryId.equals(0) ? null : locationService.getCity(birthCityId));
        modifiedUser.setCurrentCountry(currentCountryId.equals(0) ? null : locationService.getCountry(currentCountryId));
        modifiedUser.setCurrentCity(currentCountryId.equals(0) ? null : locationService.getCity(currentCityId));
        modifiedUser.setBirthDate(isBlank(birthDate) ? null : Date.valueOf(birthDate));
<<<<<<< HEAD
=======


>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        return userService.saveUser(modifiedUser);
    }

    @RequestMapping(path = "/logout", method = RequestMethod.GET)
    public String logOut(HttpSession session) {
        loggedUser.setAuthorizedUser(null);
        session.invalidate();
        return "redirect:/";
    }

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public ModelAndView getLoginPage(ModelAndView modelAndView) {
        if (loggedUser.getAuthorizedUser() != null) {
            modelAndView.setViewName("redirect:/profile");
            return modelAndView;
        }
        modelAndView.setViewName("static/login");
        return modelAndView;
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ModelAndView login(String email, String password) {
        User authorizedUser = userService.getProfile(email, password);
        ModelAndView modelAndView = new ModelAndView();
        if (authorizedUser != null) {
            loginUser(authorizedUser);
            modelAndView.setViewName("redirect:/profile");
            return modelAndView;
        }
        modelAndView.addObject("wrongInput", "true");
        modelAndView.setViewName("static/login");
        return modelAndView;
    }

    private void loginUser(User authorizedUser) {
        loggedUser.setAuthorizedUser(authorizedUser);
    }

    @RequestMapping(path = "/register", method = RequestMethod.GET)
    public String getRegister() {
        if (loggedUser.getAuthorizedUser() != null) {
            return "redirect:/profile";
        }
        return "static/registration";
    }


    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ModelAndView registerUser(@RequestParam Map<String, String> allRequestParams) {
        String email = allRequestParams.get("email");
        String password = allRequestParams.get("password");
        String name = allRequestParams.get("name");
        String surname = allRequestParams.get("surname");
        ModelAndView modelAndView = new ModelAndView();
        if (userService.getProfile(email) != null) {
            modelAndView.addObject("emailIsUsed", true);
            modelAndView.setViewName("static/registration");
            return modelAndView;
        }
        if (isFormUnfilled(email, password, name, surname)) {
            modelAndView.addObject("formUnfilled", true);
            modelAndView.setViewName("static/registration");
            return modelAndView;
        }
        User newUser = new User(email, password, name, surname);
        newUser = userService.saveUser(newUser);
        if (newUser.getId() != 0) {
            settingsService.getSettings(newUser);
            loginUser(newUser);
            modelAndView.setViewName("redirect:/profile");
            return modelAndView;
        }
        modelAndView.setViewName("static/registration");
        return modelAndView;
    }

    private boolean isFormUnfilled(String email, String password, String name, String surname) {
        return isBlank(email) || isBlank(password) || isBlank(name) || isBlank(surname);
    }


    @RequestMapping(path = "/profile", method = RequestMethod.GET)
    public String getProfile() {
        return "profile";
    }

    @RequestMapping(path = "/profile/{profileId}", method = RequestMethod.GET)
    public ModelAndView getForeignProfile(@PathVariable Integer profileId, HttpServletResponse response) {
        User owner = userService.getProfile(profileId);
        if (owner == null) {
            return new ModelAndView("static/404");
        }
        if (owner.equals(loggedUser.getAuthorizedUser())) {
            return new ModelAndView("redirect:/profile");
        }
        ModelAndView modelAndView = new ModelAndView();
        if (settingsService.isAllowedToViewPage(loggedUser.getAuthorizedUser(), owner)) {
            modelAndView = new ModelAndView("profile");
            if (loggedUser.getAuthorizedUser().getFriends().contains(owner)) {
                modelAndView.addObject("isFriend", true);
            }
            modelAndView.addObject("foreignProfile", owner);
            modelAndView.addObject("isForeign", true);
            modelAndView.addObject("isMessageWriteAllowed", settingsService.isAllowedToWriteMessages(loggedUser.getAuthorizedUser(), owner));
            modelAndView.addObject("isWallPostAllowed", settingsService.isAllowedToWallPost(loggedUser.getAuthorizedUser(), owner));
        } else {
            response.setStatus(403);
            modelAndView.setViewName("static/unauthorized");
        }
        return modelAndView;
    }

    @RequestMapping(path = "/modify", method = RequestMethod.GET)
    public ModelAndView getProfileModificationPage() {
        ModelAndView modelAndView = new ModelAndView("profileModification");
        prepareLocationRequestInfo(modelAndView);
        return modelAndView;
    }

    @RequestMapping(path = "/modify", method = RequestMethod.POST)
    public ModelAndView modifyProfile(@RequestParam Map<String, String> allRequestParams) {
        modifyLoggedUser(allRequestParams);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/profile");
        return modelAndView;
    }

    private void prepareLocationRequestInfo(ModelAndView modelAndView) {
        if (loggedUser.getAuthorizedUser() == null) {
            return;
        }
        Country birthCountry = loggedUser.getAuthorizedUser().getBirthCountry();
        Country currentCountry = loggedUser.getAuthorizedUser().getCurrentCountry();
        RequestInfo requestInfo = new RequestInfo(locationService.getCountries(),
                birthCountry == null ? null : locationService.getAllCitiesInCountry(birthCountry.getName()),
                currentCountry == null ? null : locationService.getAllCitiesInCountry(currentCountry.getName()));
        modelAndView.addObject("requestInfo", requestInfo);
    }

    @RequestMapping(path = "/addFriend/{profileId}", method = RequestMethod.GET)
    public String addFriend(@PathVariable Integer profileId) {
        userService.addFriend(loggedUser.getAuthorizedUser(), profileId);
        return "redirect:/friends";
    }

    @RequestMapping(path = "/removeFriend/{profileId}", method = RequestMethod.GET)
    public String removeFriend(@PathVariable Integer profileId) {
        userService.removeFriend(loggedUser.getAuthorizedUser(), profileId);
        return "redirect:/friends";
    }

<<<<<<< HEAD
    @RequestMapping(path = "/friends/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    List<User.UserView> getAllFriends() {
        List<User.UserView> friendsList = new ArrayList<>();
=======
    @RequestMapping(path = "/friends/all",method = RequestMethod.GET,produces= MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<User.UserView> getAllFriends(){
        List<User.UserView> friendsList=new ArrayList<>();
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        for (User user : loggedUser.getAuthorizedUser().getFriends()) {
            friendsList.add(new User.UserView(user));
        }
        return friendsList;
    }

    @RequestMapping(path = "/friends", method = RequestMethod.GET)
    public ModelAndView getFriends() {
        return new ModelAndView("/friends");
    }


}

