package com.alekseevp.portfolio.yasn.spring.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.alekseevp.portfolio.yasn.FavouritesService;
import com.alekseevp.portfolio.yasn.UserService;
import com.alekseevp.portfolio.yasn.WallService;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.WallPost;
import com.alekseevp.portfolio.yasn.entities.WallPost.WallPostView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class WallController {
    private static final Logger logger = LogManager.getLogger(SettingsController.class);
    private final WallService wallService;
    private final FavouritesService favouritesService;
    private final UserService userService;
    @Autowired
    private LoggedUser loggedUser;

    @Autowired
    public WallController(WallService wallService, FavouritesService favouritesService, UserService userService) {
        this.wallService = wallService;
        this.favouritesService = favouritesService;
        this.userService = userService;
    }

    @RequestMapping(path = "/wall/{profileId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public
    @ResponseBody
    List<WallPostView> getWall(@PathVariable Integer profileId) {
        List<WallPost> wallPosts = wallService.getUserWall(profileId);
        Collections.sort(wallPosts, new Comparator<WallPost>() {
            @Override
            public int compare(WallPost o1, WallPost o2) {
                return o1.getTimestamp().compareTo(o2.getTimestamp());
            }
        });
        List<WallPostView> wallPostViews = new ArrayList<>();
<<<<<<< HEAD
        for (WallPost wallPost : wallPosts) {
=======
        for (WallPost wallPost : wallService.getUserWall(profileId)) {
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
            wallPostViews.add(new WallPostView(wallPost,wallPost.getFavouriteOwners().contains(loggedUser.getAuthorizedUser())));
        }
        return wallPostViews;
    }

    @RequestMapping(path = "/wall/post/{profileId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public
    @ResponseBody
    String wallPost(@RequestBody String message, @PathVariable Integer profileId) {
        ObjectMapper objectMapper = new ObjectMapper();
        User owner = userService.getProfile(profileId);
        try {
            if (owner == null) {
                return objectMapper.writeValueAsString("null");
            }

            return objectMapper.writeValueAsString(new WallPostView(
                    wallService.saveWallPost(message, owner, loggedUser.getAuthorizedUser()),false));
        } catch (JsonProcessingException e) {
            logger.error(e);
            return "error";
        }
    }

    @RequestMapping(path = "/wall/remove/{postId}", method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public int removeWallPost(@PathVariable Integer postId) {
        WallPost postToRemove = wallService.getWallPost(postId);
        if (postToRemove.getOwner().equals(loggedUser.getAuthorizedUser())) {
<<<<<<< HEAD
            wallService.removeWallPost(postId);
            return 1;
=======
            return wallService.removeWallPost(postId);
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
        }
        return -1;
    }

    @RequestMapping(path = "/favourite/add/{wallPostId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    boolean addFavourite(@PathVariable Integer wallPostId) {
        boolean result=favouritesService.addFavourite(loggedUser.getAuthorizedUser(), wallPostId);
        loggedUser.setAuthorizedUser(userService.getProfile(loggedUser.getAuthorizedUser().getId()));
        return result;

    }

    @RequestMapping(path = "/favourite/remove/{wallPostId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    boolean removeFavourite(@PathVariable Integer wallPostId) {
        boolean result=favouritesService.removeFavourite(loggedUser.getAuthorizedUser(), wallPostId);
        loggedUser.setAuthorizedUser(userService.getProfile(loggedUser.getAuthorizedUser().getId()));
        return result;
    }




}
