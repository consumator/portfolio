package com.alekseevp.portfolio.yasn.spring.web;

import com.alekseevp.portfolio.yasn.FileService;
import com.alekseevp.portfolio.yasn.entities.Picture;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.ws.rs.core.MediaType;


import java.util.Arrays;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:/yasn-controller-test.xml")
public class FileControllerTest {
    @Autowired
    private LoggedUser loggedUser;
    @Autowired
    private UserService userService;
    @Autowired
    private FileService fileService;

    @Autowired
    private WebApplicationContext webAppContext;
    private MockMvc mockMvc;


    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();
    }

    @Test
    public void getAvatar_returns_avatar_via_model() throws Exception {
        Picture picture = makeRndPicture();
        User user = new User("1@1", "1", "A", "A");
        user.setAvatar(picture);
        when(userService.getProfile(100)).thenReturn(user);

        MvcResult mvcResult = this.mockMvc.perform(
                get("/100/avatar.jpg"))
                .andExpect(status().isOk())
                .andReturn();
        byte[] avatar = mvcResult.getResponse().getContentAsByteArray();
        assertTrue("Image is transferred incorrectly", Arrays.equals(picture.getImage(), avatar));
    }

    @Test
    public void getAvatar_by_picture_id_returns_avatar_via_model() throws Exception {
        Picture picture = makeRndPicture();
        when(fileService.getPicture(100)).thenReturn(picture);

        MvcResult mvcResult = this.mockMvc.perform(
                get("/avatar/100"))
                .andExpect(status().isOk())
                .andReturn();
        byte[] avatar = mvcResult.getResponse().getContentAsByteArray();
        assertTrue("Image is transferred incorrectly", Arrays.equals(picture.getImage(), avatar));
    }


    @Test
    public void getUserAvatar_returns_avatar_via_model() throws Exception {
        Picture picture = makeRndPicture();
        when(fileService.getUserAvatar(100)).thenReturn(picture);

        MvcResult mvcResult = this.mockMvc.perform(
                get("/profile/100/avatar")
                        .contentType("image/jpeg;image/png"))
                .andExpect(status().isOk())
                .andReturn();
        byte[] avatar = mvcResult.getResponse().getContentAsByteArray();
        assertTrue("Image is transferred incorrectly", Arrays.equals(picture.getImage(), avatar));
    }


    @Test
    public void uploadAvatar_must_pass_file_to_dao() throws Exception {
        byte[] contentsOfFile = new byte[128];
        for (int i = 0; i < 128; i++) {
            contentsOfFile[i] = (byte) (255 * Math.random());
        }
        MockMultipartFile testFile = new MockMultipartFile("fileUpload", "image.jpg", "multipart/form-data", contentsOfFile);
        User user = new User("test@test", "12345", "John", "Doe");
        user.setId(5000);
        Picture pictur = new Picture();
        pictur.setId(100);
        pictur.setName("testAvatar");
        pictur.setImage(contentsOfFile);
        loggedUser.setAuthorizedUser(user);
        when(fileService.uploadFile(Matchers.any(CommonsMultipartFile.class))).thenReturn(pictur);
        when(userService.saveUser(Matchers.any(User.class))).thenReturn(user);

        MvcResult mvcResult = this.mockMvc.perform(
                fileUpload("/avatarUpload")
                        .file(testFile)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().is(302))
                .andExpect(view().name("redirect:/modify"))
                .andReturn();
    }

    private Picture makeRndPicture() {
        Picture picture = new Picture();
        picture.setId(5000);
        picture.setName("testPicture");
        byte[] image = new byte[256];
        for (int i = 0; i < 256; i++) {
            image[i] = (byte) (Math.random() * 255);
        }
        picture.setImage(image);
        return picture;
    }
}
