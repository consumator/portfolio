package com.alekseevp.portfolio.yasn.spring.web;

import com.alekseevp.portfolio.yasn.HealthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
<<<<<<< HEAD
=======
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:yasn-controller-test.xml")
public class HealthControllerTest {
    @Autowired
    private HealthService healthService;

    @Autowired
    private WebApplicationContext webAppContext;

    private MockMvc mockMvc;


    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();
    }

    @Test
    public void getHealth_returns_correct_user_count() throws Exception {
        Long testUserCount=99L;
        String userCountObjectName="userCount";
        when(healthService.getUserCount()).thenReturn(testUserCount);

        MvcResult mvcResult = this.mockMvc.perform(
                get("/health"))
                .andExpect(status().isOk())
                .andExpect(model().attribute(userCountObjectName, instanceOf(Long.class)))
                .andExpect(view().name("health"))
                .andReturn();

        Long healthCount=(Long)mvcResult.getModelAndView().getModel().get(userCountObjectName);
        assertEquals("Controller puts wrong number to model",healthCount,testUserCount);

    }


}
