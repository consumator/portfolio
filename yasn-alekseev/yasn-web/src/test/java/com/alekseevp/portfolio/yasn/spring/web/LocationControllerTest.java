package com.alekseevp.portfolio.yasn.spring.web;


import com.alekseevp.portfolio.yasn.LocationService;
import com.alekseevp.portfolio.yasn.entities.City;
import com.alekseevp.portfolio.yasn.entities.Country;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

<<<<<<< HEAD
import static com.alekseevp.portfolio.yasn.TestUtility.createCityWithIdAndCountry;
import static com.alekseevp.portfolio.yasn.TestUtility.createCountryWithId;
=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:/yasn-controller-test.xml")
public class LocationControllerTest {
    @Autowired
    private LocationService locationService;

    @Autowired
    private WebApplicationContext webAppContext;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();
    }



    @Test
    public void getCountries_returns_correct_json() throws Exception {
        Country russia = createCountryWithId(1, "Russia");
        Country usa = createCountryWithId(2, "USA");
        Country germany = createCountryWithId(3, "Germany");
        List<Country> countryList = Arrays.asList(russia, usa, germany);
        when(locationService.getCountries()).thenReturn(countryList);


        MvcResult mvcResult = this.mockMvc.perform(
                get("/countries")
                        .accept(MediaType.APPLICATION_JSON_VALUE))

                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[0].id").value(russia.getId()))
                .andExpect(jsonPath("$[0].name").value(russia.getName()))
                .andExpect(jsonPath("$[1].id").value(usa.getId()))
                .andExpect(jsonPath("$[1].name").value(usa.getName()))
                .andExpect(jsonPath("$[2].id").value(germany.getId()))
                .andExpect(jsonPath("$[2].name").value(germany.getName()))
                .andReturn();
    }


    @Test
    public void getCities_returns_correct_json_list() throws Exception {
        Country russia = createCountryWithId(1, "Russia");
        City moscow = createCityWithIdAndCountry(1, "Moscow", russia);
        City petersburg = createCityWithIdAndCountry(2, "Saint-Petersburg", russia);
        City novosibirsk = createCityWithIdAndCountry(3, "Novosibirsk", russia);
        List<City> cityList = Collections.unmodifiableList(Arrays.asList(moscow, petersburg, novosibirsk));
        when(locationService.getAllCitiesInCountry(russia.getId())).thenReturn(cityList);

        MvcResult mvcResult = this.mockMvc.perform(
                get("/cities/1")
                        .accept(MediaType.APPLICATION_JSON_VALUE))

                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[0].id").value(moscow.getId()))
                .andExpect(jsonPath("$[0].name").value(moscow.getName()))
                .andExpect(jsonPath("$[0].country.name").value(russia.getName()))
                .andExpect(jsonPath("$[1].id").value(petersburg.getId()))
                .andExpect(jsonPath("$[1].name").value(petersburg.getName()))
                .andExpect(jsonPath("$[1].country.name").value(russia.getName()))
                .andExpect(jsonPath("$[2].id").value(novosibirsk.getId()))
                .andExpect(jsonPath("$[2].name").value(novosibirsk.getName()))
                .andExpect(jsonPath("$[2].country.name").value(russia.getName()))
                .andReturn();
    }

    @Test
    public void addCountry_passes_map_and_returns_true() throws Exception {
        String newCountryName = "Egypt";
        when(locationService.saveCountry(newCountryName)).thenReturn(Boolean.TRUE);

        MvcResult mvcResult = this.mockMvc.perform(
                post("/new-loc/country/")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("new-country", newCountryName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(Boolean.TRUE))
                .andReturn();
    }

    @Test
    public void addCity_passes_data_to_service_and_returns_boolean_as_json() throws Exception {
        String countryId = "200";
        String cityName = "Caracas";
        when(locationService.addNewCity(Integer.valueOf(countryId), cityName)).thenReturn(Boolean.TRUE);

        MvcResult mvcResult = this.mockMvc.perform(
                post("/new-loc/city/")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("new-city-country", countryId)
                        .param("new-city",cityName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(Boolean.TRUE))
                .andReturn();


    }


<<<<<<< HEAD

=======
    private City createCityWithIdAndCountry(int id, String name, Country country) {
        City city = new City(name, country);
        city.setId(id);
        return city;
    }

    private Country createCountryWithId(int id, String name) {
        Country country = new Country(name);
        country.setId(id);
        return country;
    }
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b


}
