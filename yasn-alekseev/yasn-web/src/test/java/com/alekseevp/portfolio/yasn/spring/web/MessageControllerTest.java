package com.alekseevp.portfolio.yasn.spring.web;

import com.alekseevp.portfolio.yasn.entities.Message;
import com.alekseevp.portfolio.yasn.MessageService;
import com.alekseevp.portfolio.yasn.entities.Picture;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.UserService;
import com.alekseevp.portfolio.yasn.SettingsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static com.alekseevp.portfolio.yasn.Utility.convertCalendarToString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:/yasn-controller-test.xml")
public class MessageControllerTest {
    @Autowired
    private MessageService messageService;
    @Autowired
    private SettingsService settingsService;
    @Autowired
    private UserService userService;

    @Autowired
    private WebApplicationContext webAppContext;

    private MockMvc mockMvc;

    @Autowired
    private LoggedUser loggedUser;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();
    }

    @Test
    public void getMessages_returns_correct_model() throws Exception {
        MockHttpSession mocksession = new MockHttpSession();
        User fakeUser = new User("A@A", "12345", "A", "A");
        User fakeUser2 = new User("B@B", "12345", "B", "B");
        loggedUser.setAuthorizedUser(fakeUser);
        List<Message> fakeMessages = MessageControllerUnitTest.generateMessageList(fakeUser, fakeUser2);
        when(messageService.getMessages((loggedUser.getAuthorizedUser()))).thenReturn(fakeMessages);

        MvcResult mvcResult = this.mockMvc.perform(
                get("/messages")
                        .session(mocksession))
                .andExpect(status().isOk())
                .andExpect(model().attribute("messages", instanceOf(List.class)))
                .andExpect(view().name("messages"))
                .andReturn();

        List<MessageController.MessageView> messageViews =
                (List<MessageController.MessageView>) mvcResult.getModelAndView().getModel().get("messages");
        assertEquals("Outward list has wrong size", fakeMessages.size(), messageViews.size());
        assertEquals(fakeMessages.get(0).getReceiver().getEmail(), messageViews.get(0).getCorrespondentEmail());
        assertTrue(!messageViews.get(0).inbox);
        assertEquals(fakeMessages.get(1).getAuthor().getEmail(), messageViews.get(0).getCorrespondentEmail());
        assertTrue(messageViews.get(1).inbox);
    }

    @Test
    public void getMessages_with_wrong_id_redirects_to_all_messages() throws Exception {
        when(userService.getProfile(0)).thenReturn(null);
        MvcResult mvcResult = this.mockMvc.perform(
                get("/messages/0"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/messages"))
                .andReturn();
    }

    @Test
    public void getMessages_by_correspondent_id_returns_correct_model() throws Exception {
        User fakeUser = new User("A@A", "12345", "A", "A");
        User fakeUser2 = new User("B@B", "12345", "B", "B");
        fakeUser.setId(1);
        fakeUser2.setId(5);
        loggedUser.setAuthorizedUser(fakeUser2);
        List<Message> fakeMessages = MessageControllerUnitTest.generateMessageList(fakeUser, fakeUser2);
        when(userService.getProfile(1)).thenReturn(fakeUser);
        when(messageService.getMessages(
                loggedUser.getAuthorizedUser(), 1)).thenReturn(fakeMessages);
        when(settingsService.isAllowedToWriteMessages(loggedUser.getAuthorizedUser(), fakeUser)).thenReturn(Boolean.TRUE);


        MvcResult mvcResult = this.mockMvc.perform(
                get("/messages/1"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("correspondent", fakeUser))
                .andExpect(model().attribute("isMessageWriteAllowed", Boolean.TRUE))
                .andReturn();
        List<MessageController.MessageView> messageViews =
                (List<MessageController.MessageView>) mvcResult.getModelAndView().getModel().get("messages");
        assertEquals("Outward list has wrong size", fakeMessages.size(), messageViews.size());
        assertEquals(fakeMessages.get(0).getAuthor().getEmail(), messageViews.get(0).getCorrespondentEmail());
        assertTrue(messageViews.get(0).inbox);
        assertEquals(fakeMessages.get(1).getReceiver().getEmail(), messageViews.get(0).getCorrespondentEmail());
        assertTrue(!messageViews.get(1).inbox);
    }

    @Test
    public void postMessage_returns_correct_json_value() throws Exception {
        User fakeUser = new User("A@A", "12345", "A", "A");
        User fakeUser2 = new User("B@B", "12345", "B", "B");
        String messageText = "Hello!";
        fakeUser.setId(1);
        fakeUser2.setId(5);
        Picture avatar = new Picture();
        avatar.setId(1000);
        fakeUser.setAvatar(avatar);
        loggedUser.setAuthorizedUser(fakeUser2);
        Message message = new Message(fakeUser, fakeUser2, messageText);
        when(messageService.postMessage(Matchers.any(Message.class))).thenReturn(message);
        MvcResult mvcResult = this.mockMvc.perform(
                post("/messages/post/5")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content("post text")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.inbox").value(true))
                .andExpect(jsonPath("$.correspondentEmail").value(fakeUser.getEmail()))
                .andExpect(jsonPath("$.correspondentId").value(fakeUser.getId()))
                .andExpect(jsonPath("$.text").value(messageText))
                .andExpect(jsonPath("$.avatarId").value(fakeUser.getAvatar().getId()))
                .andExpect(jsonPath("$.timeStamp").value(convertCalendarToString(message.getTimestamp())))
                .andReturn();
    }
}
