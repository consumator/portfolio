package com.alekseevp.portfolio.yasn.spring.web;


import com.alekseevp.portfolio.yasn.entities.Message;
import com.alekseevp.portfolio.yasn.entities.User;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MessageControllerUnitTest {
    @Test
    public void convertMessagesForView_returns_expected_List(){
        User user1=generateUser("first",100);
        User user2=generateUser("second", 200);

        List<Message> messages=generateMessageList(user1,user2);
        List<MessageController.MessageView> messageViews=MessageController.convertMessagesForView(messages,user1);

        assertEquals("Converted list has wrong size",messages.size(),messageViews.size());
         assertEquals(messages.get(0).getReceiver().getEmail(),messageViews.get(0).getCorrespondentEmail());
        assertTrue(!messageViews.get(0).inbox);
        assertEquals(messages.get(1).getAuthor().getEmail(),messageViews.get(0).getCorrespondentEmail());
        assertTrue(messageViews.get(1).inbox);
    }

    private User generateUser(String email, int id){
        return new User(email,"","A","B");
    }

    public static List<Message> generateMessageList(User user1,User user2){
        Message message1=new Message(user1,user2,"Hello!");
        Message message2=new Message(user2,user1,"Nihao");
        Message message3=new Message(user1,user2,"Good bye!");
        Message message4=new Message(user2,user1,"C Ya");
        return Collections.unmodifiableList(Arrays.asList(message1,message2,message3,message4));
    }

}
