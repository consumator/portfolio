package com.alekseevp.portfolio.yasn.spring.web;

import com.alekseevp.portfolio.yasn.LocationService;
<<<<<<< HEAD
import com.alekseevp.portfolio.yasn.SettingsService;
import com.alekseevp.portfolio.yasn.UserService;
import com.alekseevp.portfolio.yasn.entities.Country;
import com.alekseevp.portfolio.yasn.entities.RequestInfo;
import com.alekseevp.portfolio.yasn.entities.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
=======
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.SettingsService;
import com.alekseevp.portfolio.yasn.UserService;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

<<<<<<< HEAD
import java.util.Arrays;
import java.util.List;

import static com.alekseevp.portfolio.yasn.TestUtility.*;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.hasSize;
=======
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.alekseevp.portfolio.yasn.TestUtility.*;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Enlighter
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:/yasn-controller-test.xml")
public class SearchControllerTest {
    private final static User TEST_USER_1 = generateTestUser(TEST_EMAIL, 1);
    private final static User TEST_USER_2 = generateTestUser(TEST_EMAIL2, 5);
    private final static User TEST_USER_3 = generateTestUser(TEST_EMAIL3, 10);
    private final static User TEST_USER_4 = generateTestUser(TEST_EMAIL4, 777);

    @Autowired
    private UserService userService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private LoggedUser loggedUser;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

<<<<<<< HEAD
    @Test
    public void getSearch_returns_correct_view() throws Exception {
        Country russia = createCountryWithId(1, "Russia");
        Country usa = createCountryWithId(2, "USA");
        Country germany = createCountryWithId(3, "Germany");
        List<Country> countryList = Arrays.asList(russia, usa, germany);

        when(locationService.getCountries()).thenReturn(countryList);
        this.mockMvc.perform
                (
                        get("/search")
                )
                .andExpect(status().isOk())
                .andExpect(view().name("search"))
                .andReturn();
    }

=======
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

    @Test
    public void search_returns_json_list() throws Exception {
        loggedUser.setAuthorizedUser(TEST_USER_1);
<<<<<<< HEAD
        final List<User> mockedSearchResults = Arrays.asList(TEST_USER_1, TEST_USER_2, TEST_USER_3, TEST_USER_4);
        when(userService.search(isA(User.UserSearchQueryObject.class)))
                .thenAnswer(new Answer() {
                    @Override
                    public List<User> answer(InvocationOnMock invocation) throws Throwable {
                        Object[] args = invocation.getArguments();
                        User.UserSearchQueryObject queryObject = (User.UserSearchQueryObject) args[0];
                        queryObject.setResultsPageCount(1);
                        return mockedSearchResults;
                    }
                });
        when(settingsService.isAllowedToViewPage(isA(User.class), isA(User.class))).thenReturn(true);

        mockMvc.perform(
                post("/search/0")
                        .param("name", "any")
                        .param("surname", "any")
                        .accept("text/plain; charset=utf-8")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.searchResults", hasSize(4)))
                .andExpect(jsonPath("$.searchResults[0].id").value(TEST_USER_1.getId()))
                .andExpect(jsonPath("$.searchResults[0].email").value(TEST_USER_1.getEmail()))
                .andExpect(jsonPath("$.searchResults[1].id").value(TEST_USER_2.getId()))
                .andExpect(jsonPath("$.searchResults[1].email").value(TEST_USER_2.getEmail()))
                .andExpect(jsonPath("$.searchResults[2].id").value(TEST_USER_3.getId()))
                .andExpect(jsonPath("$.searchResults[2].email").value(TEST_USER_3.getEmail()))
                .andExpect(jsonPath("$.searchResults[3].id").value(TEST_USER_4.getId()))
                .andExpect(jsonPath("$.searchResults[3].email").value(TEST_USER_4.getEmail()))
                .andExpect(jsonPath("$.pageCount.count").value(1))
                .andExpect(jsonPath("$.pageCount.current").value(0))
=======
        List<User> fakeSearchResults = Arrays.asList(TEST_USER_1, TEST_USER_2, TEST_USER_3, TEST_USER_4);
        when(userService.search(isA(Map.class))).thenReturn(fakeSearchResults);
        when(settingsService.isAllowedToViewPage(isA(User.class), isA(User.class))).thenReturn(true);

        MvcResult mvcResult = mockMvc.perform(
                post("/search")
                        .param("name", "any")
                        .param("surname", "any")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].id").value(TEST_USER_1.getId()))
                .andExpect(jsonPath("$[0].email").value(TEST_USER_1.getEmail()))
                .andExpect(jsonPath("$[1].id").value(TEST_USER_2.getId()))
                .andExpect(jsonPath("$[1].email").value(TEST_USER_2.getEmail()))
                .andExpect(jsonPath("$[2].id").value(TEST_USER_3.getId()))
                .andExpect(jsonPath("$[2].email").value(TEST_USER_3.getEmail()))
                .andExpect(jsonPath("$[3].id").value(TEST_USER_4.getId()))
                .andExpect(jsonPath("$[3].email").value(TEST_USER_4.getEmail()))
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
                .andReturn();
    }

    @Test
<<<<<<< HEAD
    public void quicksearch_returns_json_list() throws Exception {
        loggedUser.setAuthorizedUser(TEST_USER_1);
        final List<User> mockedSearchResults = Arrays.asList(TEST_USER_1, TEST_USER_2, TEST_USER_3, TEST_USER_4);
        when(userService.search(isA(User.UserSearchQueryObject.class)))
                .thenAnswer(new Answer() {
                    @Override
                    public List<User> answer(InvocationOnMock invocation) throws Throwable {
                        Object[] args = invocation.getArguments();
                        User.UserSearchQueryObject queryObject = (User.UserSearchQueryObject) args[0];
                        queryObject.setResultsPageCount(1);
                        return mockedSearchResults;
                    }
                });
        when(settingsService.isAllowedToViewPage(isA(User.class), isA(User.class))).thenReturn(true);

        mockMvc.perform
                (
                        get("/quicksearch/0")
                                .param("searchString", "any")
                                .accept("text/plain; charset=utf-8")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.searchResults", hasSize(4)))
                .andExpect(jsonPath("$.searchResults[0].id").value(TEST_USER_1.getId()))
                .andExpect(jsonPath("$.searchResults[0].email").value(TEST_USER_1.getEmail()))
                .andExpect(jsonPath("$.searchResults[1].id").value(TEST_USER_2.getId()))
                .andExpect(jsonPath("$.searchResults[1].email").value(TEST_USER_2.getEmail()))
                .andExpect(jsonPath("$.searchResults[2].id").value(TEST_USER_3.getId()))
                .andExpect(jsonPath("$.searchResults[2].email").value(TEST_USER_3.getEmail()))
                .andExpect(jsonPath("$.searchResults[3].id").value(TEST_USER_4.getId()))
                .andExpect(jsonPath("$.searchResults[3].email").value(TEST_USER_4.getEmail()))
                .andExpect(jsonPath("$.pageCount.count").value(1))
                .andExpect(jsonPath("$.pageCount.current").value(0))
                .andReturn();
    }

    @Test
    public void quicksearch_returns_correct_view() throws Exception {
        Country russia = createCountryWithId(1, "Russia");
        Country usa = createCountryWithId(2, "USA");
        Country germany = createCountryWithId(3, "Germany");
        List<Country> countryList = Arrays.asList(russia, usa, germany);
        String searchString = "string";

        when(locationService.getCountries()).thenReturn(countryList);
        MvcResult result = this.mockMvc.perform(
                get("/quicksearch")
                        .param("searchString", searchString))
                .andExpect(status().isOk())
                .andExpect(view().name("search"))
                .andExpect(model().attribute("quickSearchString", searchString))
                .andExpect(model().attribute("isCurrentQuickSearch", true))
                .andReturn();

        RequestInfo requestInfo = (RequestInfo) result.getModelAndView().getModel().get("requestInfo");
        assertTrue("RequestInfo object formed incorrectly", requestInfo.getCountryList() == countryList);
        assertTrue("RequestInfo object formed incorrectly", requestInfo.getBirthCountryCityList().size() == 0);
        assertTrue("RequestInfo object formed incorrectly", requestInfo.getCurrentCountryCityList().size() == 0);


    }
=======
    public void quicksearch_returns_correct_json_list() throws Exception {
        loggedUser.setAuthorizedUser(TEST_USER_1);
        List<User> fakeSearchResults = new ArrayList<>(Arrays.asList(TEST_USER_1, TEST_USER_2, TEST_USER_3, TEST_USER_4));
        when(userService.quickSearch(isA(String.class))).thenReturn(new ArrayList<>(fakeSearchResults));
        when(settingsService.isAllowedToViewPage(isA(User.class), isA(User.class))).thenReturn(true);
        when(settingsService.isAllowedToViewPage(loggedUser.getAuthorizedUser(), TEST_USER_3)).thenReturn(false);
        MvcResult mvcResult = mockMvc.perform(
                get("/quicksearch")
                        .param("searchParams", "test value")
                        .accept(MediaType.TEXT_HTML_VALUE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("searchResults", instanceOf(String.class)))
                        .andExpect(view().name("search"))
                        .andReturn();

        String searchResults = (String) mvcResult.getModelAndView().getModel().get("searchResults");
        Type listType = new TypeToken<ArrayList<User>>(){}.getType();

        List<User> userList = new Gson().fromJson(searchResults, listType);
        User user1=userList.get(0);
        User user2=userList.get(1);

        User user3=userList.get(2);

        assertNotNull(userList);
        assertTrue("Wrong collection size", userList.size() == 3);
        assertTrue("Wrong user representation",userList.get(0).equalsForJSON(TEST_USER_1));
        assertTrue("Wrong user representation",userList.get(1).equalsForJSON(TEST_USER_2));
        assertTrue("Wrong user representation",userList.get(2).equalsForJSON(TEST_USER_4));


    }

>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
}
