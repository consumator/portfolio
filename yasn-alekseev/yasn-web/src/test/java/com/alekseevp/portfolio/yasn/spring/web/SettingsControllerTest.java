package com.alekseevp.portfolio.yasn.spring.web;


import com.alekseevp.portfolio.yasn.entities.Setting;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.SettingsService;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingType;
import com.alekseevp.portfolio.yasn.entities.settingEnums.PrivacySettingsValue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static com.alekseevp.portfolio.yasn.TestUtility.generateTestUser;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:yasn-controller-test.xml")
public class SettingsControllerTest {
    @Autowired
    private SettingsService settingsService;
    @Autowired
    private LoggedUser loggedUser;
    @Autowired
    WebApplicationContext wac;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void getSettings_returns_correct_view_with_settings_object() throws Exception {
        User testUser = generateTestUser("test@email", 999);
        loggedUser.setAuthorizedUser(testUser);
        List<Setting> settings = new ArrayList<>();
        for (PrivacySettingType privacySettingType : PrivacySettingType.values()) {
            Setting setting = new Setting();
            setting.setSettingType(privacySettingType);
            setting.setValue(PrivacySettingsValue.getDefaultValue(privacySettingType));
            setting.setUser(testUser);
            settings.add(setting);
        }
        when(settingsService.getSettings(loggedUser.getAuthorizedUser())).thenReturn(settings);

        MvcResult mvcResult = mockMvc.perform(
                get("/settings"))
                .andExpect(status().isOk())
                .andExpect(view().name("/settings"))
                .andExpect(model().attribute("user_settings", settings))
                .andReturn();
    }

    @Test
    public void  saveSettings_redirects_to_profile() throws Exception {
        when(settingsService.getSettings(loggedUser.getAuthorizedUser())).thenReturn(new ArrayList<Setting>());


        MvcResult mvcResult=mockMvc.perform(
                post("/settings"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/profile"))
                .andReturn();
    }


}
