package com.alekseevp.portfolio.yasn.spring.web;


<<<<<<< HEAD
import com.alekseevp.portfolio.yasn.LocationService;
import com.alekseevp.portfolio.yasn.SettingsService;
import com.alekseevp.portfolio.yasn.UserService;
import com.alekseevp.portfolio.yasn.entities.Country;
import com.alekseevp.portfolio.yasn.entities.Setting;
import com.alekseevp.portfolio.yasn.entities.User;
=======
import com.alekseevp.portfolio.yasn.entities.Country;
import com.alekseevp.portfolio.yasn.LocationService;
import com.alekseevp.portfolio.yasn.entities.Setting;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.SettingsService;
import com.alekseevp.portfolio.yasn.UserService;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;

import static com.alekseevp.portfolio.yasn.TestUtility.*;
<<<<<<< HEAD
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.mockito.Matchers.isA;
=======
import static com.alekseevp.portfolio.yasn.TestUtility.TEST_PASSWORD;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.*;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
<<<<<<< HEAD
=======
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:/yasn-controller-test.xml")
public class UserControllerTest {

    @Autowired
    WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    private UserService userService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private SettingsService settingsService;

    @Autowired
    private LoggedUser loggedUser;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        loggedUser.setAuthorizedUser(null);
    }


    @Test
    public void logOut_invalidates_session_and_redirects() throws Exception {
        MockHttpSession mockSession = new MockHttpSession();
        User testUser = generateTestUser(TEST_EMAIL);
        loggedUser.setAuthorizedUser(testUser);
        assertNotNull(loggedUser.getAuthorizedUser());

        MvcResult mvcResult = this.mockMvc.perform(
                get("/logout")
                        .session(mockSession))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"))
                .andReturn();

        assertNull("Session was not invalidated", loggedUser.getAuthorizedUser());
    }

    @Test
    public void getLoginPage_returns_login_page() throws Exception {
        MockHttpSession mockSession = new MockHttpSession();

        MvcResult mvcResult = this.mockMvc.perform(
                get("/login")
                        .session(mockSession))
                .andExpect(status().isOk())
                .andExpect(view().name("static/login"))
                .andReturn();

        assertNull(loggedUser.getAuthorizedUser());

    }

    @Test
    public void getLoginPage_redirects_to_profile() throws Exception {
        MockHttpSession mockSession = new MockHttpSession();
        User testUser = generateTestUser(TEST_EMAIL);
        loggedUser.setAuthorizedUser(testUser);

        MvcResult mvcResult = this.mockMvc.perform(
                get("/login")
                        .session(mockSession))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/profile"))
                .andReturn();

        assertNotNull(loggedUser.getAuthorizedUser());
    }


    @Test
    public void login_posts_and_redirects_to_profile() throws Exception {
        MockHttpSession mockHttpSession = new MockHttpSession();
        final User testUser = generateTestUser(TEST_EMAIL);
        when(userService.getProfile(TEST_EMAIL, TEST_PASSWORD)).thenReturn(testUser);

        MvcResult mvcResult = this.mockMvc.perform(
                post("/login")
                        .param("email", TEST_EMAIL)
                        .param("password", TEST_PASSWORD)
                        .session(mockHttpSession))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/profile"))
                .andReturn();

        assertEquals("User logged unsuccessfully", testUser, loggedUser.getAuthorizedUser());
    }

    @Test
    public void login_returns_correct_view_in_wrong_input_case() throws Exception {
        MockHttpSession mockHttpSession = new MockHttpSession();
        when(userService.getProfile(TEST_EMAIL, TEST_PASSWORD)).thenReturn(null);

        MvcResult mvcResult = this.mockMvc.perform(
                post("/login")
                        .param("email", TEST_EMAIL)
                        .param("password", TEST_PASSWORD)
                        .session(mockHttpSession))
                .andExpect(status().isOk())
                .andExpect(model().attribute("wrongInput", "true"))
                .andExpect(view().name("static/login"))
                .andReturn();

        assertNull("Logged user is not null", loggedUser.getAuthorizedUser());
    }

    @Test
    public void getRegister_returns_registration_page_no_logged_user() throws Exception {
        MockHttpSession mockHttpSession = new MockHttpSession();
        this.mockMvc.perform(
                get("/register")
                        .session(mockHttpSession))
                .andExpect(status().isOk())
                .andExpect(view().name("static/registration"))
                .andReturn();
    }

    @Test
    public void getRegister_redirects_to_profile_with_logged_user() throws Exception {
        MockHttpSession mockHttpSession = new MockHttpSession();
        final User testUser = generateTestUser(TEST_EMAIL);
        loggedUser.setAuthorizedUser(testUser);
        this.mockMvc.perform(
                get("/register")
                        .session(mockHttpSession))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/profile"))
                .andReturn();
    }

    @Test
    public void registerUser_with_used_email_returns_correct_model() throws Exception {
        User testUser = generateTestUser(TEST_EMAIL);
        when(userService.getProfile(TEST_EMAIL)).thenReturn(testUser);

        this.mockMvc.perform(
                post("/register")
                        .param("email", TEST_EMAIL))
                .andExpect(status().isOk())
                .andExpect(model().attribute("emailIsUsed", true))
                .andExpect(view().name("static/registration"))
                .andReturn();
    }

    @Test
    public void registerUser_with_unfilled_form_returns_correct_model() throws Exception {
        User testUser = generateTestUser(TEST_EMAIL);
        when(userService.getProfile(TEST_EMAIL)).thenReturn(null);

        this.mockMvc.perform(
                post("/register")
                        .param("email", TEST_EMAIL)
                        .param("password", "12345"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("formUnfilled", true))
                .andExpect(view().name("static/registration"))
                .andReturn();
    }


    @Test
    public void registerUser_with_filled_form_returns_redirects_to_profile() throws Exception {
        final int id = 500;
        User testUser = generateTestUser(TEST_EMAIL);
        testUser.setId(id);
        when(userService.getProfile(TEST_EMAIL)).thenReturn(null);
        when(userService.saveUser(isA(User.class))).thenReturn(testUser);
        when(settingsService.getSettings(testUser)).thenReturn(new ArrayList<Setting>());

        this.mockMvc.perform(
                post("/register")
                        .param("email", TEST_EMAIL)
                        .param("password", TEST_PASSWORD)
                        .param("name", TEST_NAME)
                        .param("surname", TEST_SURNAME)
        )
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/profile"))
                .andReturn();

        assertEquals("Logged user is not equal to test user", testUser, loggedUser.getAuthorizedUser());
    }

    @Test
    public void getProfile_returns_correct_view() throws Exception {
        this.mockMvc.perform(
                get("/profile"))
                .andExpect(status().isOk())
                .andExpect(view().name("profile"))
                .andReturn();
    }


    @Test
    public void getForeignProfile_returns_404_in_wrong_id_case() throws Exception {
        when(userService.getProfile(404)).thenReturn(null);

        mockMvc.perform(
                get("/profile/404"))
                .andExpect(status().isOk())
                .andExpect(view().name("static/404"))
                .andReturn();
    }

    @Test
    public void getForeignProfile_redirects_to_own_page_in_own_id_case() throws Exception {
        User testUser = generateTestUser(TEST_EMAIL);
        testUser.setId(777);
        loggedUser.setAuthorizedUser(testUser);
        when(userService.getProfile(777)).thenReturn(testUser);

        mockMvc.perform(
                get("/profile/777"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/profile"))
                .andReturn();
    }

    @Test
    public void getForeignProfile_returns_unauthorized_when_not_allowed_to_view() throws Exception {
        User user1 = generateTestUser(TEST_EMAIL);
        user1.setId(777);
        User user2 = generateTestUser(TEST_EMAIL2);
        user2.setId(555);
        loggedUser.setAuthorizedUser(user1);
        when(userService.getProfile(555)).thenReturn(user2);
        when(settingsService.isAllowedToViewPage(user1, user2)).thenReturn(false);


        mockMvc.perform(
                get("/profile/555"))
                .andExpect(status().is(403))
                .andExpect(view().name("static/unauthorized"))
                .andReturn();
    }

    @Test
    public void getForeignProfile_returns_profile_when_everything_is_ok() throws Exception {
        User testUser1 = generateTestUser(TEST_EMAIL);
        testUser1.setId(777);
        User testUser2 = generateTestUser(TEST_EMAIL2);
        testUser2.setId(555);
        testUser1.addFriend(testUser2);
        loggedUser.setAuthorizedUser(testUser1);
        when(userService.getProfile(555)).thenReturn(testUser2);
        when(settingsService.isAllowedToViewPage(testUser1, testUser2)).thenReturn(true);
        when(settingsService.isAllowedToWriteMessages(testUser1, testUser2)).thenReturn(true);
        when(settingsService.isAllowedToWallPost(testUser1, testUser2)).thenReturn(false);


        mockMvc.perform(
                get("/profile/555"))
                .andExpect(status().isOk())
                .andExpect(view().name("profile"))
                .andExpect(model().attribute("isFriend", true))
                .andExpect(model().attribute("foreignProfile", testUser2))
                .andExpect(model().attribute("isMessageWriteAllowed", true))
                .andExpect(model().attribute("isWallPostAllowed", false))
                .andReturn();
    }

    @Test
    public void getProfileModificationPage_returns_correct_view() throws Exception {
        this.mockMvc.perform(
                get("/modify"))
                .andExpect(status().isOk())
                .andExpect(view().name("profileModification"))
                .andReturn();
    }

    @Test
    public void modifyProfile_modifies_and_redirects() throws Exception {
        User testUser = generateTestUser(TEST_EMAIL);
        testUser.setId(50);
        String newPatronymic = "Petrovich";
        Country russia = new Country("Russia");
        russia.setId(1);
        loggedUser.setAuthorizedUser(testUser);
        when(locationService.getCountry(1)).thenReturn(russia);

        MvcResult mvcResult = mockMvc.perform(
                post("/modify")
                        .param("patronymic", newPatronymic)
                        .param("birthCountry", "1")
                        .param("sex", "1"))

                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/profile"))
                .andReturn();

        assertEquals("Patronymic was not changed", newPatronymic, loggedUser.getAuthorizedUser().getPatronymic());
        assertEquals("Birth country was not changed", russia, loggedUser.getAuthorizedUser().getBirthCountry());
<<<<<<< HEAD
        assertEquals("Sex was not changed", false, loggedUser.getAuthorizedUser().getSex());
=======
        assertEquals("Sex is unchanged", false, loggedUser.getAuthorizedUser().getSex());
>>>>>>> fa389a9136375816c11a659f798e116666a89c4b
    }

    @Test
    public void addFriend_redirects_to_friend() throws Exception {
        User testUser = generateTestUser(TEST_EMAIL);
        loggedUser.setAuthorizedUser(testUser);
        when(userService.addFriend(loggedUser.getAuthorizedUser(), 5)).thenReturn(Boolean.TRUE);

        mockMvc.perform(get("/addFriend/5"))

                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/friends"))
                .andReturn();
    }

    @Test
    public void removeFriend_redirects_to_friend() throws Exception {
        User testUser = generateTestUser(TEST_EMAIL);
        loggedUser.setAuthorizedUser(testUser);

        mockMvc.perform(get("/removeFriend/5"))

                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/friends"))
                .andReturn();
    }


    @Test
    public void getFriends_returns_view_with_friends() throws Exception {
        User testUser1 = generateTestUser(TEST_EMAIL);
        testUser1.setId(3);
        User testUser2 = generateTestUser(TEST_EMAIL2);
        testUser2.setId(5);
        User testUser3 = generateTestUser(TEST_EMAIL3);
        testUser3.setId(10);
        User testUser4 = generateTestUser(TEST_EMAIL4);
        testUser4.setId(11);
        testUser1.addFriend(testUser2);
        testUser1.addFriend(testUser3);
        testUser1.addFriend(testUser4);
        loggedUser.setAuthorizedUser(testUser1);

        MvcResult mvcResult = mockMvc.perform(get("/friends/all")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(3)))
                        .andExpect(jsonPath("$[0].email").value(testUser2.getEmail()))
                        .andExpect(jsonPath("$[1].email").value(testUser3.getEmail()))
                        .andExpect(jsonPath("$[2].email").value(testUser4.getEmail()))
                        .andReturn();
    }
}



