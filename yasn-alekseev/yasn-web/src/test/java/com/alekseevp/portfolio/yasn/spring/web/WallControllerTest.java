package com.alekseevp.portfolio.yasn.spring.web;

import com.alekseevp.portfolio.yasn.*;
import com.alekseevp.portfolio.yasn.entities.User;
import com.alekseevp.portfolio.yasn.entities.WallPost;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:yasn-controller-test.xml")
public class WallControllerTest {
    public static final String TEST_STRING_0 = "Hello! How are you?";
    public static final String TEST_STRING_1 = "Hello! I`m fine!";
    public static final String TEST_STRING_2 = "Let`s move out please!";
    public static final String TEST_STRING_3 = "C u there!";

    @Autowired
    private LoggedUser loggedUser;
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    @Autowired
    private WallService wallService;

    @Autowired
    private FavouritesService favouritesService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void getWall_returns_proper_wall_post_views_as_json() throws Exception {
        User testUser1 = TestUtility.generateTestUser("email1", 500);
        User testUser2 = TestUtility.generateTestUser("email2", 100);
        loggedUser.setAuthorizedUser(testUser1);
        WallPost post0 = new WallPost(TEST_STRING_0, testUser1, testUser2);
        post0.setId(5);
        post0.getFavouriteOwners().add(testUser1);
        WallPost post1 = new WallPost(TEST_STRING_1, testUser1, testUser1);
        post1.setId(10);
        WallPost post2 = new WallPost(TEST_STRING_2, testUser1, testUser2);
        post2.setId(15);
        WallPost post3 = new WallPost(TEST_STRING_3, testUser1, testUser1);
        post3.setId(20);
        List<WallPost> wallPosts = new ArrayList<>(Arrays.asList(post0, post1, post2, post3));
        when(wallService.getUserWall(500)).thenReturn(wallPosts);

        mockMvc.perform(
                get("/wall/500")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(post0.getId()))
                .andExpect(jsonPath("$[0].from").value(post0.getFrom().getEmail()))
                .andExpect(jsonPath("$[0].from_id").value(post0.getFrom().getId()))
                .andExpect(jsonPath("$[0].favourite").value(true))
                .andExpect(jsonPath("$[0].text").value(TEST_STRING_0))
                .andExpect(jsonPath("$[1].id").value(post1.getId()))
                .andExpect(jsonPath("$[1].from").value(post1.getFrom().getEmail()))
                .andExpect(jsonPath("$[1].from_id").value(post1.getFrom().getId()))
                .andExpect(jsonPath("$[1].favourite").value(false))
                .andExpect(jsonPath("$[1].text").value(TEST_STRING_1))
                .andExpect(jsonPath("$[2].id").value(post2.getId()))
                .andExpect(jsonPath("$[2].from").value(post2.getFrom().getEmail()))
                .andExpect(jsonPath("$[2].from_id").value(post2.getFrom().getId()))
                .andExpect(jsonPath("$[2].favourite").value(false))
                .andExpect(jsonPath("$[2].text").value(TEST_STRING_2))
                .andExpect(jsonPath("$[3].id").value(post3.getId()))
                .andExpect(jsonPath("$[3].from").value(post3.getFrom().getEmail()))
                .andExpect(jsonPath("$[3].from_id").value(post3.getFrom().getId()))
                .andExpect(jsonPath("$[3].favourite").value(false))
                .andExpect(jsonPath("$[3].text").value(TEST_STRING_3))
                .andReturn();
    }

    @Test
    public void wallPost_returns_null_json() throws Exception {
        mockMvc.perform(
                post("/wall/post/-5")
                        .content("Bebe")
                        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$").value("null"))
                .andReturn();
    }

    @Test
    public void wallPost_returns_correct_json() throws Exception {
        int testUserId1 = 500;
        int testUserId2 = 1000;
        int testWallPostId = 700;
        User testUser1 = TestUtility.generateTestUser("email1", testUserId1);
        User testUser2 = TestUtility.generateTestUser("email2", testUserId2);
        loggedUser.setAuthorizedUser(testUser1);
        String messageText = "Blahblah";
        WallPost wallPost = new WallPost(messageText, testUser1, testUser2);
        wallPost.setId(testWallPostId);
        when(userService.getProfile(testUserId1)).thenReturn(testUser1);
        when(userService.getProfile(testUserId2)).thenReturn(testUser2);
        when(wallService.saveWallPost(messageText, testUser2, testUser1)).thenReturn(wallPost);
        mockMvc.perform(
                post("/wall/post/" + testUserId2)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(messageText)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(wallPost.getId()))
                .andExpect(jsonPath("$.from_id").value(wallPost.getFrom().getId()))
                .andExpect(jsonPath("$.from").value(wallPost.getFrom().getEmail()))
                .andExpect(jsonPath("$.text").value(wallPost.getText()))
                .andExpect(jsonPath("$.avatarId").value(0))
                .andExpect(jsonPath("$.time_stamp").value(Utility.convertCalendarToString(wallPost.getTimestamp())))
                .andExpect(jsonPath("$.favourite").value(false))
                .andReturn();
    }

    @Test
    public void removeWallPost_returns_json() throws Exception {
        int testWallPostId = 1000;
        User testUser = TestUtility.generateTestUser("email1", 500);
        User testUser1 = TestUtility.generateTestUser("email2", 1000);

        loggedUser.setAuthorizedUser(testUser);
        WallPost wallPost = new WallPost("Blabla", testUser, testUser1);
        when(wallService.removeWallPost(testWallPostId)).thenReturn(1);
        when(wallService.getWallPost(testWallPostId)).thenReturn(wallPost);
        mockMvc.perform(
                delete("/wall/remove/" + testWallPostId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").value(1))
                .andReturn();
    }

    @Test
    public void addFavourite_returns_json() throws Exception {
        User testUser1 = TestUtility.generateTestUser("email1", 500);
        loggedUser.setAuthorizedUser(testUser1);
        int testWallPostId = 1000;
        when(favouritesService.addFavourite(testUser1, testWallPostId)).thenReturn(true);
        mockMvc.perform(
                post("/favourite/add/" + testWallPostId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").value(true))
                .andReturn();
    }

    @Test
    public void removeFavourite_returns_json() throws Exception {
        User testUser1 = TestUtility.generateTestUser("email1", 500);
        loggedUser.setAuthorizedUser(testUser1);
        int testWallPostId = 1000;
        when(favouritesService.removeFavourite(testUser1, testWallPostId)).thenReturn(true);
        mockMvc.perform(
                post("/favourite/remove/" + testWallPostId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").value(true))
                .andReturn();
    }

}
